#include "pch.hpp"
#include "stringer.hpp"


//const Length StringerT::plyThickness = 0.125_mm;

Area StringerJ::GetArea() const
{
    auto bw = botFlWidth.GetValue();
    auto tw = topFlWidth.GetValue();
    auto wh = height.GetValue();
    auto t = element.material.GetThickness().GetValue();
    auto botFlA = bw * t;
    auto topFlA = tw * t * 2;
    auto webA = wh * t * 2;
    return botFlA + topFlA + webA;
}

Area StringerJ::GetEffArea(Length skinThickness) const
{
    return GetArea() + skinThickness * botFlWidth;
}

Vec3d StringerJ::GetCentroidLocal() const
{
    auto bw = botFlWidth.GetValue();
    auto tw = topFlWidth.GetValue();
    auto wh = height.GetValue();
    auto t = element.material.GetThickness().GetValue();
    auto botFlA = bw * t;
    auto topFlA = tw * t * 2;
    auto webA = wh * t * 2;
    auto a = botFlA + topFlA + webA;
    auto ay = botFlA * t / 2.
            + topFlA * (t + wh + t / 2.)
            + webA * (t + wh / 2.);
    return Vec3d(0., ay / a, 0.);
}

SecondAreaMoment StringerJ::I() const
{
    auto bw = botFlWidth.GetValue();
    auto tw = topFlWidth.GetValue();
    auto wh = height.GetValue();
    auto t = element.material.GetThickness().GetValue();
    auto y = GetCentroidLocal().y(); // ybar
    auto botFlA = bw * t;
    auto topFlA = tw * t * 2;
    auto webA = wh * t * 2;
    return 2. * t * pow(wh, 3.) / 12.
           + bw * pow(t, 3.) / 12. + botFlA * pow(y - t / 2., 2)
           + tw * pow(2 * t, 3.) / 12. + topFlA * pow(t + wh + t / 2., 2);
}

Area StringerT::GetArea() const
{
    return {
        GetFlangeArea() + GetWebArea()
    };
}

Area StringerT::GetEffArea(Length skinThickness) const
{
    return GetArea() + skinThickness * width;
}

Vec3d StringerT::GetCentroidLocal() const
{
    Vec3d centroid = { 0., 0., 0. };
    
    // z along flange 
    centroid.z() = GetFlangeLength().GetValue() / 2.0;

    const auto yFlange = GetFlangeThickness() / 2.0;
    const auto yWeb = GetWebHeight() / 2.0 - GetFlangeThickness();
    centroid.y() = ((yFlange * GetFlangeArea() + yWeb * GetWebArea()) / GetArea()).GetValue();

    return centroid;
}

Vec3d StringerT::GetCentroidGlobal(Line3d surfaceNormal) const
{
    return position + surfaceNormal.direction() * GetCentroidLocal().y();
}

Length StringerT::GetFlangeLength() const
{
    return width;
}

Length StringerT::GetFlangeThickness() const
{
    return element.material.GetThickness();
}

Area StringerT::GetFlangeArea() const
{
    return GetFlangeLength() * GetFlangeThickness();
}

Length StringerT::GetWebHeight() const
{
    return height - GetFlangeThickness();
}

Length StringerT::GetWebThickness() const
{
    return element.material.GetThickness() * 2.0;
}

Area StringerT::GetWebArea() const
{
    return GetWebHeight() * GetWebThickness();
}

SecondAreaMoment StringerT::I() const
{
    auto wt = GetWebThickness();
    auto wh = GetWebHeight();
    auto wi = wt * wh * wh * wh / 12.0;
    auto fh = GetFlangeThickness();
    auto fb = GetFlangeLength();
    auto fi = fb * fh * fh * fh / 12.0;
    auto tc = (wh + fh) / 2.0;
    auto wc = wh / 2.0;
    auto dw = abs(tc - wc);
    auto wa = GetWebArea();
    auto fc = fh / 2.0;
    auto df = abs(tc - fc);
    auto fa = GetFlangeArea();

    return wi + wa * dw * dw + fi + fa * df * df;
}

Area StringerHat::GetArea() const
{
    auto w = width.GetValue();
    auto h = height.GetValue();
    auto t = element.material.GetThickness().GetValue();
    return 3 * w * t;
}

Area StringerHat::GetEffArea(Length skinThickness) const
{
    return GetArea() + skinThickness * width; // just skin to flanges??
}

Vec3d StringerHat::GetCentroidLocal() const
{
    auto w = width.GetValue();
    auto h = height.GetValue();
    auto t = element.material.GetThickness().GetValue();
    auto b = h - 2. * t;
    auto bt = b       * t;
    auto wt = w       * t;
    auto ay = t       * wt / 2. 
            + wt * 2. * (h - t / 2.)
            + bt * 2. * (t + b / 2.); 
    auto a = 3. * wt 
           + 2. * bt;
    return Vec3d(0., ay / a, 0.);
}

SecondAreaMoment StringerHat::I() const
{
    auto w = width.GetValue();
    auto h = height.GetValue();
    auto t = element.material.GetThickness().GetValue();
    auto b = h - 2. * t;
    auto y = GetCentroidLocal().y(); // y_bar
    auto wt = w * t;
    auto bt = b * t;
    return w       * pow(t, 3.)              / 12.
         + t  * 2. * pow(b, 3.)              / 12.
         + w  * 2. * pow(t, 3.)              / 12.

         + wt      * pow((y - t / 2.), 2.)
         + bt * 2. * pow((y - (b / 2. + t)), 2.)
         + wt * 2. * pow((h - y), 2.);
}
