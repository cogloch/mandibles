#pragma once
#include <string>
#include "units.hpp"


class ConfigState
{
public:
    std::string rootAirfoilPath = "naca632615.dat";
    std::string tipAirfoilPath = "naca632615.dat";

    Length rootChord = 3.5_m;
    Percentage taperRatio = 37_percent;
    Length wingspan = 47.9_m;
    Percentage frontSparOffset = 15_percent, rearSparOffset = 60_percent;
    double quarterChordSweep; // Length tipDisplacementBack;

    Percentage allowableDeflection = 10_percent; // Of semispan

    Length strutConvTol = 0.1_mm; 

    Percentage junctionToCentreline = 50_percent; // Of semispan
    double junctionAngle = 16.0;
    Length strutChord = 400_mm;
    Length strutLength = 11464_mm;
    Percentage strutTC = 0.15;
    Length strutWallThickness = 1_mm;
};