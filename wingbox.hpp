#pragma once
#include "units.hpp"
#include "composites.hpp"


struct Wingbox;

//Eigen::Vector3d GetEigenVec(const Point3d& what);

struct WingboxSection
{
    // Same treatment as airfoils (refactor??)
    //enum Surface { TOP, BOT };
    //std::array<PiecewiseCurve3d, 2> surfaces;

    // Connectivity implied by order of verts
    // Loop: ..vertN--vert0--vert1--..--vertN--vert0..
    std::vector<Point3d> verts;
    //std::vector<Line3d> edges;

    // Reflection
    size_t sectionIndex;

    Length GetSpanwisePos() const
    {
        return verts.front().x();
    }

    Length GetCurveLength(size_t vert0Index, size_t vert1Index) const;
    Area GetSectionalArea() const;

    Length GetUprCurveLength() const;
    Length GetLwrCurveLength() const;
    Length GetFrontSparHeight() const;
    Length GetRearSparHeight() const;

    Eigen::ParametrizedLine<double, 3> GetFrontSpar() const;

    // Neutral axis should roughly go through 

    // TODO: refactor this massive pile of trash
    Stiffness effExUprSkin, effExLwrSkin;
    Stiffness effExUprStringer, effExLwrStringer;
    Stiffness effExFrontSparWeb, effExRearSparWeb;
    Stiffness effExFrontSparLwrFlange, effExFrontSparUprFlange,
              effExRearSparLwrFlange, effExRearSparUprFlange;
    
    enum ElementIndex
    {
        UPR_SKIN, LWR_SKIN,
        UPR_STRINGER, LWR_STRINGER,
        FRONT_SPAR_WEB, REAR_SPAR_WEB,
        FRONT_SPAR_LWR_FLANGE, FRONT_SPAR_UPR_FLANGE,
        REAR_SPAR_LWR_FLANGE, REAR_SPAR_UPR_FLANGE,
        
        NUM_ELEMENTS
    };
    //std::array<Stress, NUM_ELEMENTS> dirStress;
    Stress dirStressUprSkin, dirStressLwrSkin,
        dirStressUprStringer, dirStressLwrStringer,
        dirStressFrontSparWeb, dirStressRearSparWeb,
        dirStressFrontSparLwrFlange, dirStressFrontSparUprFlange,
        dirStressRearSparLwrFlange, dirStressRearSparUprFlange;

    Stress allowDirStressUprSkin, allowDirStressLwrSkin,
        allowDirStressUprStringer, allowDirStressLwrStringer,
        allowDirStressFrontSparWeb, allowDirStressRearSparWeb,
        allowDirStressFrontSparLwrFlange, allowDirStressFrontSparUprFlange,
        allowDirStressRearSparLwrFlange, allowDirStressRearSparUprFlange;

    double rfDirStressUprSkin, rfDirStressLwrSkin,
        rfDirStressUprStringer, rfDirStressLwrStringer,
        rfDirStressFrontSparWeb, rfDirStressRearSparWeb,
        rfDirStressFrontSparLwrFlange, rfDirStressFrontSparUprFlange,
        rfDirStressRearSparLwrFlange, rfDirStressRearSparUprFlange;

    // Equivalent isotropic modulus
    Stiffness ExBar;
    SecondAreaMoment IzBar;

    AxialStiffness AExBar;
    BendingStiffness ExIzBar;

    Area effAreaUprSkin, effAreaLwrSkin;
    Area effAreaUprStringers, effAreaLwrStringers;
    Area effAreaFrontSparWeb, effAreaRearSparWeb;
    Area effAreaFrontSparLwrFlange, effAreaFrontSparUprFlange,
         effAreaRearSparLwrFlange, effAreaRearSparUprFlange;
    Area effArea;

    Point3d centroid;

    // See Wingbox decl for note on spanwise changes (tip: right now sections don't do that)
    void ComputeEffectiveAreas(Wingbox& wingbox);
    void ComputeEffectiveStiffnesses(Wingbox& wingbox);
    void Test(Wingbox& wingbox);

    Force netTransverse;

    Length GetChord()
    {
        //auto le = GetEigenVec(verts.front());
        //auto te = GetEigenVec(verts[verts.size() / 2 + 1]);
        const auto le = verts.front();
        const auto te = verts[verts.size() / 2 + 1];
        return (te - le).norm();
    }

    Eigen::Vector3d GetAC()
    {
        // TEMP
        // Quarter chord from LE
        //auto le = GetEigenVec(verts.front());
        //auto te = GetEigenVec(verts[verts.size() / 2 + 1]);
        const auto le = verts.front();
        const auto te = verts[verts.size() / 2 + 1];
        return le + (te - le) * 0.25;
    }

    Eigen::Vector3d GetCG()
    {
        // TEMP
        return GetSC();
    }

    Eigen::Vector3d GetSC()
    {
        // TEMP
        // 40% chord from LE 
        //auto le = GetEigenVec(verts.front());
        //auto te = GetEigenVec(verts[verts.size() / 2 + 1]);
        const auto le = verts.front();
        const auto te = verts[verts.size() / 2 + 1];
        return le + (te - le) * 0.4;
    }

    Length topStringerToNA;
    Length botStringerToNA;
    Length topSkinToNA;
    Length botSkinToNA;

    Length frontSparUprFlangeToNA;
    Length frontSparLwrFlangeToNA;

    // TODO!!!!
    Length rearSparUprFlangeToNA;
    Length rearSparLwrFlangeToNA;

private:
    size_t m_numUprStringers;
    size_t m_numLwrStringers;
};

// Defined by coordinates in world space (Spanwise : x, Thicknesswise : y, Streamwise : z), at a discrete set
// of sections
struct SectionalEnvelope
{
    std::vector<WingboxSection> sections;
    
    Length GetDistanceBetweenSections(size_t sec0Index, size_t sec1Index);
    Volume GetVolumeBetweenSections(size_t sec0Index, size_t sec1Index);
};

struct ShellElement
{
    QILayup layup;
    Length thickness;
};

struct BeamElement
{
    QILayup layup;
    Area area;       // Cross-sectional area for "skin/boom" idealization in analysis section by section
                     // TODO: consider the actual topology 
};

struct Wingbox
{
    // Each section: 
    // TOP surface:        0     -> floor(numVerts / 2) 
    // BOT surface: numVerts - 1 -> floor(numVerts / 2) + 1  
    SectionalEnvelope envelope;
    // TODO: make not stupid
    SectionalEnvelope airfoilEnvelope;

    // Assume no spanwise material changes (upper skin thickness towards root == lower skin thickness towards tip, etc.)
    // Also the same "basic" material properties for all elements (for metal construction - same Al alloy; for 
    // CFRP - same UD or woven (TODO) properties for one 0deg ply)
    // TODO: allow discrete spanwise changes 

    // UD "basic" ply for all elements  
    Ply plyProperties;
    
    // !! Only stiffned skin construction !!
    ShellElement upperSkin;
    ShellElement lowerSkin;

    // For now, only 2 spars (TODO: extend to multispar/multicell configs; maybe)
    ShellElement frontSparWeb;
    BeamElement frontSparUprFlange;
    BeamElement frontSparLwrFlange;
    ShellElement rearSparWeb;
    BeamElement rearSparUprFlange;
    BeamElement rearSparLwrFlange;

    // Stiffener number and spacing is set at root 
    // Layup/material the same along span (FOR NOW)
    // For each subsequent section, set how many stringers "end" at the section plane 
    // E.g. if at root there are 5 stringers and at section 1, 2 stringers "end" -> there are 4 stringers
    // starting from section 1 and ending at the tip
    BeamElement uprStringer;
    BeamElement lwrStringer;

    // Distance from last stringer (or from start/spar flange)
    // Number of stringers is implied (== rootStringerSpacings.size()) 
    std::vector<Length> rootUprStringerSpacings;  
    // Size is == number of sections - 0
    // [0] must be 0 
    std::vector<size_t> numUprStringersEndingHere;

    std::vector<Length> rootLwrStringerSpacings; 
    std::vector<size_t> numLwrStringersEndingHere;

    void ComputeSectionalProperties();
    void TempComputeLoadState();

    template<typename T>
    T Mean(T a, T b)
    {
        return 0.5 * (a + b);
    }

    std::vector<Length> GetDeflection(bool clean)
    {
        const auto numSec = envelope.sections.size();
        std::vector<double> deflectionAngle(numSec);
        std::vector<Length> deflection(numSec);
        
        // BCs
        deflectionAngle[0] = 0.;
        deflection[0] = 0_m;

        for (size_t i = 1; i < envelope.sections.size(); ++i)
        {
            const auto& cur = envelope.sections[i];
            const auto& prev = envelope.sections[i - 1];
            const auto dx = envelope.GetDistanceBetweenSections(i - 1, i);

            // Integrate M/EI(x)dx
            if (clean)
            {
                const auto prevFrac = cleanBM[i - 1] / prev.ExIzBar;
                const auto curFrac = cleanBM[i] / cur.ExIzBar;
                deflectionAngle[i] = deflectionAngle[i - 1] + (Mean(prevFrac, curFrac) * dx).GetValue();
            }
            else
            {
                const auto prevFrac = BM[i - 1] / prev.ExIzBar;
                const auto curFrac = BM[i] / cur.ExIzBar;
                deflectionAngle[i] = deflectionAngle[i - 1] + (Mean(prevFrac, curFrac) * dx).GetValue();
            }
            
            // Integrate deflAngle(x)dx
            deflection[i] = deflection[i - 1] + Mean(deflectionAngle[i - 1], deflectionAngle[i]) * dx;
        }

        return deflection;
    }

    // TEMP
    std::vector<ForcePerUnitLength> netTransverseLoads;
    std::vector<Length> momentArms;
    std::vector<ShearForce> SF;
    std::vector<BendingMoment> BM;
    std::vector<ShearForce> cleanSF;
    std::vector<BendingMoment> cleanBM;
    std::vector<TorsionMoment> T;
};
