#include "pch.hpp"
#include "spar.hpp"


Area SparFlange::GetArea() const
{
    const auto t = element.material.GetThickness();
    return base * t + (height - t) * t;
}

SecondAreaMoment SparFlange::I() const
{
    auto wt = element.material.GetThickness();
    auto wh = height - wt;
    auto wi = wt * wh * wh * wh / 12.0;
    auto fh = element.material.GetThickness();
    auto fb = base;
    auto fi = fb * fh * fh * fh / 12.0;
    auto tc = (wh + fh) / 2.0;
    auto wc = wh / 2.0;
    auto dw = abs(tc - wc);
    auto wa = wt * wh;
    auto fc = fh / 2.0;
    auto df = abs(tc - fc);
    auto fa = fb * fh;

    return wi + wa * dw * dw + fi + fa * df * df;
}

Length Spar::GetWebHeight() const
{
    return (uprFlange.element.centroid - lwrFlange.element.centroid).norm();
}

Area Spar::GetWebArea() const
{
    return web.element.material.GetThickness() * GetWebHeight();
}

Area Spar::GetEffArea() const
{
    return uprFlange.GetArea() + lwrFlange.GetArea() + GetWebArea();
}

