#pragma once


class Airfoil;
class SectionalEnvelope;

class Wing
{
public:
    Wing();
    Wing(const Wing&);
    ~Wing();// = default;

    // Equal spacing every (semispan / (numSections - 1))
    // TODO: take a list of sections instead 
    SectionalEnvelope GetWingboxEnvelope(size_t numSections);
    // Num sections == sectionSpacings + 1 (== num bays + 1)
    SectionalEnvelope GetWingboxEnvelope(const std::vector<Length>& sectionSpacings);

    const SectionalEnvelope& GetAirfoilEnvelope() const;

    Length GetSemispan() const;
    Length GetSpan() const;
    
    Wing& SetFoils(Airfoil root, Airfoil tip);
    Wing& SetSparOffsets(Percentage front, Percentage rear);
    Wing& SetTipDisplacement(Length dispBack);
    Wing& SetChords(Length rootChord, Length tipChord = 0_m);
    Wing& SetTaperRatio(Percentage taper);
    Wing& SetSemispan(Length semiSpan);

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};
