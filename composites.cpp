#include "pch.hpp"
#include "composites.hpp"
#include "utils.hpp"


const Eigen::Vector3d QILayup::s_exContrib = {
        1.0, 0.1, 0.1
};

const Eigen::Vector3d QILayup::s_eyContrib = {
        0.1, 0.1, 1.0 
};

QILayup::QILayup(double percZero, double percFortyFive, double percNinety)
    : QILayup(Eigen::Vector3d{percZero, percFortyFive, percNinety})
{
}

//QILayup::QILayup(const std::array<double, 3>& percentages)
//    : layupPercentages(percentages)
//{
//    double total = 0.0;
//    for (auto perc : layupPercentages)
//    {
//        if (perc < 0.0 || perc > 100.0)
//            throw "QILayup ctor:: invalid fraction: " + std::to_string(perc) + "!";
//        total += perc;
//    }
//
//    if (!EqualDouble(total, 100.0))
//        throw "QILayup ctor:: fractions do not add up to 100%: " + std::to_string(total) + "!";
//}

QILayup::QILayup(const Eigen::Vector3d& percentages)
{
    SetLayup(percentages);
}

double& QILayup::operator[](size_t idx)
{
    return m_layupPercentages[idx];
}

void QILayup::SetLayup(const Eigen::Vector3d& percentages)
{
    CheckLayupSetValid(percentages);
    m_layupPercentages = percentages;

    m_exFactor = s_exContrib.dot(m_layupPercentages / 100.0);
    m_eyFactor = s_eyContrib.dot(m_layupPercentages / 100.0);
}

double QILayup::GetExFactor() const
{
    return m_exFactor;
}

double QILayup::GetEyFactor() const
{
    return m_eyFactor;
}

Stiffness QILayup::GetExFor(Stiffness fibreE) const
{
    return m_exFactor * fibreE;
}

Stiffness QILayup::GetEyFor(Stiffness fibreE) const
{
    return m_eyFactor * fibreE;
}

void QILayup::CheckLayupSetValid(const Eigen::Vector3d& percentages)
{
    double total = 0.0;
    for (size_t dim = 0; dim < 3; ++dim)
    {
        auto perc = percentages[dim];
        if (perc < 0.0 || perc > 100.0)
            throw "QILayup ctor:: invalid fraction: " + std::to_string(perc) + "!";
        total += perc;
    }

    if (!EqualDouble(total, 100.0))
        throw "QILayup ctor:: fractions do not add up to 100%: " + std::to_string(total) + "!";
}
