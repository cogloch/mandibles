#pragma once
#include "geometry.hpp"

//I. Form airfoils
// Chordwise: x
// Thicknesswise: y
//
// 1. Load Lednicer format in two arrays - one for top surface, one for bottom surface
// 1.5 If any x < 0.000 fuck off, don't even bother
// 2. Shift all coords chordiwse by -0.5 (half chord); origin becomes mid-chord 
// 3. For each point on top surface, if doesn't exist, find corresponding point on bottom (find neighboring points, then LERP)
// 4. - "- for bottom surface
// ~The number of coordinates for top and bottom surfaces should be the same now
// 5. struct ChordwisePair { double chordLoc; double top; double bot; };
//      For each x coord, make a ChordWisePair{ xcoord, top[at xcoord], bot[at xcoord] }
//      Shove into an array
//      Badabingbadaboom, airfoil
class Airfoil
{
public:
    struct ChordwiseSection
    {
        double frac;
        double top, bot;
    };

    Airfoil() = default;
    Airfoil(const std::string& fromRaw);

    enum Surface
    {
        TOP, BOT
    };
    std::array<PiecewiseCurve2d, 2> surfaces;
};

struct AirfoilManager
{
public:
    AirfoilManager() { m_airfoils.reserve(2); }
    Airfoil& GetAirfoil(size_t index);

    // Return index of added airfoil
    size_t LoadAirfoilByLocalPath(const std::string& path);

private:
    std::vector<Airfoil> m_airfoils;
};