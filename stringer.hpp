#pragma once
#include "qi_laminate.hpp"


struct Stringer
{
    virtual ~Stringer() = default;
    virtual Area GetArea() const = 0;
    virtual Area GetEffArea(Length skinThickness) const = 0;      // Add area of skin attached to stringer 
    virtual Vec3d GetCentroidLocal() const = 0;

    // NOT centroid
    // Reference point specific to each topology
    // E.g. the peel corner for a Z-section; midpoint for T-section, symmetrical hat, Y etc.
    Vec3d position;

    enum class Topology
    {
        T, J, Hat
    } topology;

protected:
    Stringer(Topology topo = Topology::T) : topology(topo) {}
};

struct StringerHat : Stringer
{
    StringerHat() : Stringer(Topology::Hat) {}

    Area GetArea() const override;
    Area GetEffArea(Length skinThickness) const override;
    Vec3d GetCentroidLocal() const override;

    SecondAreaMoment I() const;

    // Thicknesses in multiples of 0.125mm (thickness of one base ply -> thickness_detail = num_plies X 0.125mm)
    //static const Length plyThickness;
    // Also sets a constraint on ply fractions out of the overall laminate
    // (must be divisible; cannot have fractions of plies)
    StructuralElement element;
    Length width;
    Length height;
};

struct StringerJ : Stringer
{
    StringerJ() : Stringer(Topology::J) {}

    Area GetArea() const override;
    Area GetEffArea(Length skinThickness) const override;
    Vec3d GetCentroidLocal() const override;

    SecondAreaMoment I() const;

    // Thicknesses in multiples of 0.125mm (thickness of one base ply -> thickness_detail = num_plies X 0.125mm)
    //static const Length plyThickness;
    // Also sets a constraint on ply fractions out of the overall laminate
    // (must be divisible; cannot have fractions of plies)
    StructuralElement element;
    Length botFlWidth;
    Length topFlWidth;
    Length height; // Between flanges, exclusive!
};

struct StringerT : Stringer
{
    StringerT() : Stringer(Topology::T) {}

    Area GetArea() const override;
    Area GetEffArea(Length skinThickness) const override;
    // As offset from <position> (sitting on skin)
    Vec3d GetCentroidLocal() const override;
    Vec3d GetCentroidGlobal(Line3d surfaceNormal) const;           // TODO: clean; global centroid set in <element> by mesh builder 

    Length GetFlangeLength() const;
    Length GetFlangeThickness() const;
    Area GetFlangeArea() const;
    Length GetWebHeight() const;
    Length GetWebThickness() const;
    Area GetWebArea() const;

    SecondAreaMoment I() const;

    // Thicknesses in multiples of 0.125mm (thickness of one base ply -> thickness_detail = num_plies X 0.125mm)
    //static const Length plyThickness;
    // Also sets a constraint on ply fractions out of the overall laminate
    // (must be divisible; cannot have fractions of plies)
    StructuralElement element;
    Length width;
    Length height;
};
