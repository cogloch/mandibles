#include "pch.hpp"
#include "wingbox.hpp"


//Eigen::Vector3d GetEigenVec(const Point3d& what)
//{
//    return { what.x().GetValue(), what.y().GetValue(), what.z().GetValue() };
//}

Vec3d GetEigenVec(const Vec3d& what) { return what; }

Length WingboxSection::GetCurveLength(size_t vert0Index, size_t vert1Index) const
{
    // Order vert0Index < vert1Index
    if (vert0Index > vert1Index)
        std::swap(vert0Index, vert1Index);

    Length result(0_m);
    for (; vert0Index < vert1Index; ++vert0Index)
    {
        auto segStart = GetEigenVec(verts[vert0Index]);
        auto segEnd = GetEigenVec(verts[vert0Index + 1]);
        result += (segEnd - segStart).norm();
    }

    return result;
}

Area WingboxSection::GetSectionalArea() const
{
    // Gauss area
    Area area = 0_m2;
    
    for (size_t i = 0; i < verts.size() - 1; ++i)
    {
        const auto xcur = verts[i].z();
        const auto ynext = verts[i + 1].y();
        const auto darea = xcur * ynext;
        area += darea;
    }
    
    area += verts[verts.size() - 1].z() * verts[0].y();

    for (size_t i = 0; i < verts.size() - 1; ++i)
        area -= verts[i + 1].z() * verts[i].y();

    area -= verts[0].z() * verts[verts.size() - 1].y();

    return 0.5 * abs(area);
}

Length WingboxSection::GetUprCurveLength() const
{
    return GetCurveLength(0, verts.size() / 2);
}

Length WingboxSection::GetLwrCurveLength() const
{
    return GetCurveLength(verts.size() - 1, verts.size() / 2 + 1);
}

Length WingboxSection::GetFrontSparHeight() const
{
    const Eigen::Vector3d upr = GetEigenVec(verts.front());
    const Eigen::Vector3d lwr = GetEigenVec(verts.back());
    return ((upr - lwr).norm());
}

Length WingboxSection::GetRearSparHeight() const
{
    const Eigen::Vector3d upr = GetEigenVec(verts[verts.size() / 2]);
    const Eigen::Vector3d lwr = GetEigenVec(verts[verts.size() / 2 + 1]);
    return ((upr - lwr).norm());
}

Eigen::ParametrizedLine<double, 3> WingboxSection::GetFrontSpar() const
{
    return Eigen::ParametrizedLine<double, 3>::Through(GetEigenVec(verts.front()), GetEigenVec(verts.back()));
}

Length SectionalEnvelope::GetDistanceBetweenSections(size_t sec0Index, size_t sec1Index)
{
    // Sections are streamwise (not chordwise), hence any point is valid on either section
    return Length(abs(sections[sec0Index].verts.front().x() - sections[sec1Index].verts.front().x()));
}

Volume SectionalEnvelope::GetVolumeBetweenSections(size_t sec0Index, size_t sec1Index)
{
    if (sec0Index == sec1Index)
        return 0_m3;

    if (sec0Index > sec1Index)
        std::swap(sec0Index, sec1Index);

    Volume result = 0_m3;
    for (size_t i = sec0Index + 1; i < sec1Index; ++i)
    {
        const auto dx = GetDistanceBetweenSections(i - 1, i);
        const auto prevArea = sections[i - 1].GetSectionalArea();
        const auto curArea = sections[i].GetSectionalArea();
        const auto meanArea = 0.5 * (prevArea + curArea);
        result += dx * meanArea;
    }
    return result;
}

void WingboxSection::ComputeEffectiveAreas(Wingbox& wingbox)
{
    // Skins 
    effAreaUprSkin = GetUprCurveLength() * wingbox.upperSkin.thickness;
    effAreaLwrSkin = GetLwrCurveLength() * wingbox.lowerSkin.thickness;

    // Stringers
    m_numUprStringers = wingbox.rootUprStringerSpacings.size();
    m_numLwrStringers = wingbox.rootLwrStringerSpacings.size();
    for (size_t i = 0; i <= sectionIndex; ++i)
    {
        m_numUprStringers -= wingbox.numUprStringersEndingHere[i];
        m_numLwrStringers -= wingbox.numLwrStringersEndingHere[i];
    }

    effAreaUprStringers = wingbox.uprStringer.area * m_numUprStringers;
    effAreaLwrStringers = wingbox.lwrStringer.area * m_numLwrStringers;

    // Spar webs 
    effAreaFrontSparWeb = GetFrontSparHeight() * wingbox.frontSparWeb.thickness;
    effAreaRearSparWeb = GetRearSparHeight() * wingbox.rearSparWeb.thickness;

    // Spar flanges 
    // Comes out-of-the-box (tm)
    effAreaFrontSparUprFlange = wingbox.frontSparUprFlange.area;
    effAreaFrontSparLwrFlange = wingbox.frontSparLwrFlange.area;
    effAreaRearSparUprFlange = wingbox.rearSparUprFlange.area;
    effAreaRearSparLwrFlange = wingbox.rearSparLwrFlange.area;

    // Total 
    effArea = effAreaUprSkin + effAreaLwrSkin +
        effAreaFrontSparWeb + effAreaRearSparWeb +
        effAreaUprStringers + effAreaLwrStringers +
        effAreaFrontSparUprFlange + effAreaFrontSparLwrFlange + effAreaRearSparUprFlange + effAreaRearSparLwrFlange;
}

void WingboxSection::ComputeEffectiveStiffnesses(Wingbox& wingbox)
{
    std::pair<Stiffness&, QILayup&> exLayupPairs[] = {
        { effExUprSkin, wingbox.upperSkin.layup},
        { effExLwrSkin, wingbox.lowerSkin.layup},
        { effExUprStringer, wingbox.uprStringer.layup},
        { effExLwrStringer, wingbox.lwrStringer.layup},
        { effExFrontSparWeb, wingbox.frontSparWeb.layup},
        { effExRearSparWeb, wingbox.rearSparWeb.layup},
        { effExFrontSparUprFlange, wingbox.frontSparUprFlange.layup},
        { effExFrontSparLwrFlange, wingbox.frontSparLwrFlange.layup},
        { effExRearSparUprFlange, wingbox.rearSparUprFlange.layup},
        { effExRearSparLwrFlange, wingbox.rearSparLwrFlange.layup}
    };

    for (auto& pair : exLayupPairs)
        pair.first = pair.second.GetExFor(wingbox.plyProperties.E1);
}

void WingboxSection::Test(Wingbox& wingbox)
{
    ComputeEffectiveAreas(wingbox);
    ComputeEffectiveStiffnesses(wingbox);

    /*Eigen::Matrix<Area, 10, 1> effArea;
    effArea << effAreaUprSkin, effAreaLwrSkin, 
               effAreaUprStringers, effAreaLwrStringers,
               effAreaFrontSparWeb, effAreaRearSparWeb, 
               effAreaFrontSparUprFlange, effAreaFrontSparLwrFlange, effAreaRearSparUprFlange, effAreaRearSparLwrFlange;

    Eigen::Matrix<Stiffness, 10, 1> effEx;
    effEx << effExUprSkin, effExLwrSkin,
        effExUprStringer, effExLwrStringer,
        effExFrontSparWeb, effExRearSparWeb,
        effExFrontSparUprFlange, effExFrontSparLwrFlange, effExRearSparUprFlange, effExRearSparLwrFlange;

    auto effAE = effArea.dot(effEx);*/

    AExBar = effAreaUprSkin * effExUprSkin + effAreaLwrSkin * effExLwrSkin +
        effAreaUprStringers * effExUprStringer + effAreaLwrStringers * effExLwrStringer +
        effAreaFrontSparWeb * effExFrontSparWeb + effAreaRearSparWeb * effExRearSparWeb +
        effAreaFrontSparUprFlange * effExFrontSparUprFlange + effAreaFrontSparLwrFlange * effExFrontSparLwrFlange +
        effAreaRearSparUprFlange * effExRearSparUprFlange + effAreaRearSparLwrFlange * effExRearSparLwrFlange;

    ExBar = AExBar / effArea;

    const Eigen::Vector3d upr = GetEigenVec(verts.front());
    const Eigen::Vector3d lwr = GetEigenVec(verts.back());

    // !TODO: Properly compute centroid and NA!
    // Assume BM about the z(bar)-axis overlapping the NA, which then runs from the front spar mid, parallel to the global z 
    const auto frontSparTop = GetEigenVec(verts.front());
    const auto frontSparBot = GetEigenVec(verts.back());
    const auto frontSparMid = frontSparBot + (frontSparTop - frontSparBot) * 0.5;
    double NAy = frontSparMid.y();

    // PAT
    // TODO (ybar = sum(Ay)/sum(A))
    double centroidYTopSkin = (frontSparTop + (GetEigenVec(verts[verts.size() / 4]) - frontSparTop) * 0.5).y();
    double centroidYBotSkin = (frontSparBot + (GetEigenVec(verts[verts.size() - verts.size() / 4]) - frontSparBot) * 0.5).y();

    topSkinToNA = centroidYTopSkin - NAy;
    //                               A                       y^2
    SecondAreaMoment IzUprSkin = effAreaUprSkin * (topSkinToNA * topSkinToNA);
    
    botSkinToNA = centroidYBotSkin - NAy;
    SecondAreaMoment IzLwrSkin = effAreaLwrSkin * (botSkinToNA * botSkinToNA);

    // TODO URGENT
    // Make parallel stringer planes and actually cut the top/bot skins, then get here the actual distances 
    std::vector<Eigen::Vector3d> topStringers;
    // TEMP!!!!
    topStringerToNA = frontSparTop.y() - NAy;
    botStringerToNA = frontSparBot.y() - NAy;
    SecondAreaMoment IzUprStringers = m_numUprStringers * wingbox.uprStringer.area * (topStringerToNA * topStringerToNA);
    SecondAreaMoment IzLwrStringers = m_numLwrStringers * wingbox.lwrStringer.area * (botStringerToNA * botStringerToNA);

    frontSparUprFlangeToNA = frontSparTop.y() - NAy;
    frontSparLwrFlangeToNA = frontSparBot.y() - NAy;

    // TODO!!!!
    rearSparUprFlangeToNA = frontSparTop.y() - NAy;
    rearSparLwrFlangeToNA = frontSparBot.y() - NAy;

    SecondAreaMoment IzFrontSparUprFlange = wingbox.frontSparUprFlange.area * (frontSparUprFlangeToNA * frontSparUprFlangeToNA);
    SecondAreaMoment IzFrontSparLwrFlange = wingbox.frontSparLwrFlange.area * (frontSparLwrFlangeToNA * frontSparLwrFlangeToNA);
    SecondAreaMoment IzRearSparUprFlange = wingbox.rearSparUprFlange.area * (rearSparUprFlangeToNA * rearSparUprFlangeToNA);
    SecondAreaMoment IzRearSparLwrFlange = wingbox.rearSparLwrFlange.area * (rearSparLwrFlangeToNA * rearSparLwrFlangeToNA);

    const auto frontSparHeight = GetFrontSparHeight();
    const auto rearSparHeight = GetRearSparHeight();
    SecondAreaMoment IzFrontSparWeb = wingbox.frontSparWeb.thickness * (frontSparHeight * frontSparHeight * frontSparHeight) / 12.0;
    SecondAreaMoment IzRearSparWeb = wingbox.rearSparWeb.thickness * (rearSparHeight * rearSparHeight * rearSparHeight) / 12.0;

    // E dot I 
    BendingStiffness EIzUprSkin = effExUprSkin * IzUprSkin;
    BendingStiffness EIzLwrSkin = effExLwrSkin * IzLwrSkin;
    BendingStiffness EIzUprStringers = effExUprStringer * IzUprStringers;
    BendingStiffness EIzLwrStringers = effExLwrStringer * IzLwrStringers;
    BendingStiffness EIzFrontSparWeb = effExFrontSparWeb * IzFrontSparWeb;
    BendingStiffness EIzRearSparWeb = effExRearSparWeb * IzRearSparWeb;
    BendingStiffness EIzFrontSparUprFlange = effExFrontSparUprFlange * IzFrontSparUprFlange;
    BendingStiffness EIzFrontSparLwrFlange = effExFrontSparLwrFlange * IzFrontSparLwrFlange;
    BendingStiffness EIzRearSparUprFlange = effExRearSparUprFlange * IzRearSparUprFlange;
    BendingStiffness EIzRearSparLwrFlange = effExRearSparLwrFlange * IzRearSparLwrFlange;

    ExIzBar = EIzUprSkin + EIzLwrSkin + EIzUprStringers + EIzLwrStringers
        + EIzFrontSparWeb + EIzRearSparWeb + EIzFrontSparUprFlange + EIzFrontSparLwrFlange + EIzRearSparUprFlange + EIzRearSparLwrFlange;

    IzBar = ExIzBar / ExBar;
}

void Wingbox::ComputeSectionalProperties()
{
    for (auto& section : envelope.sections)
    {
        section.Test(*this);
    }
}

void Wingbox::TempComputeLoadState()
{
    // TODO: TEMP
    // Do everything at ULT for now (TODO: stiffness checks at LIM)
    double n = 2.5 * 1.5;
    Acceleration g = 9.80665_mPerS2;
    Area wingArea = 118_m2;
    Mass wingMass = 5782_kg;
    Mass mtow = 62668_kg;
    Mass fuselageMass = mtow - wingMass;
    Weight fuselageWeight = fuselageMass * g;   

    size_t numSec = envelope.sections.size();
    netTransverseLoads.clear();
    netTransverseLoads.resize(numSec);
    momentArms.clear();
    momentArms.resize(numSec);
    SF.clear();
    SF.resize(numSec);
    BM.clear();
    BM.resize(numSec);
    T.clear();
    T.resize(numSec);

    /*auto semispan = envelope.GetDistanceBetweenSections(
        0, envelope.sections.back().sectionIndex);*/
    
    // TEMP! 
    auto k = (n * fuselageWeight) / wingArea;
    for (size_t i = 0; i < numSec; ++i)
    {
        auto& section = envelope.sections[i];

        netTransverseLoads[i] = k * envelope.sections[i].GetChord();
        momentArms[i] = (section.GetAC() - section.GetSC()).norm();
    }

    SF.back() = 0_N;
    BM.back() = 0_Nm;
    T.back() = 0_Nm;
    for (int i = numSec - 2; i >= 0; --i)
    {
        auto dx = envelope.GetDistanceBetweenSections(i, i + 1);
        
        auto avgLoad = 0.5 * (netTransverseLoads[i] + netTransverseLoads[i + 1]);
        SF[i] = SF[i + 1] + avgLoad * dx;

        auto avgShear = 0.5 * (SF[i] + SF[i + 1]);
        BM[i] = BM[i + 1] + avgShear * dx;

        // TODO: actually read cooper flexular/elastic axis (does it matter for
        // no quarter chord sweep??)
        T[i] = T[i + 1] + avgShear * momentArms[i];
    }
}
