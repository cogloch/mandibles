#include "pch.hpp"
#include "structural_section.hpp"


Area StructuralSection::GetInteriorArea() const
{
    std::vector<Vec3d> verts;
    for (auto& pt : uprSkin.verts)
        verts.push_back(pt);
    for (auto it = lwrSkin.verts.rbegin(); it != lwrSkin.verts.rend(); ++it)
        verts.push_back(*it);

    // Gauss area
    Area area = 0_m2;

    for (size_t i = 0; i < verts.size() - 1; ++i)
    {
        const auto xcur = verts[i].z();
        const auto ynext = verts[i + 1].y();
        const auto darea = xcur * ynext;
        area += darea;
    }

    area += verts[verts.size() - 1].z() * verts[0].y();

    for (size_t i = 0; i < verts.size() - 1; ++i)
        area -= verts[i + 1].z() * verts[i].y();

    area -= verts[0].z() * verts[verts.size() - 1].y();

    return 0.5 * abs(area);
}

Vec3d StructuralSection::GetCentroid() const
{
    // TODO
    const auto frontSparC = spars[0].web.element.centroid;
    const auto rearSparC = spars[1].web.element.centroid;
    return frontSparC + (rearSparC - frontSparC) * 0.5;
}

Area StructuralSection::GetEffArea() const
{
    Area eff(0_m2);
    for (auto& spar : spars)
        eff += spar.GetEffArea();
    for (auto& stringer : uprStringers)
        eff += stringer.GetEffArea(uprSkin.element.material.GetThickness());
    for (auto& stringer : lwrStringers)
        eff += stringer.GetEffArea(lwrSkin.element.material.GetThickness());
    eff += uprSkin.GetEffArea();
    eff += lwrSkin.GetEffArea();
    return eff;
}

// jesus fucking christ this is horrible
AxialStiffness StructuralSection::GetEffAE()
{
    AxialStiffness eff(0);
    for (auto& spar : spars)
    {
        eff += spar.GetWebArea() * spar.web.element.material.GetEffEx();
        eff += spar.uprFlange.GetArea() * spar.uprFlange.element.material.GetEffEx();
        eff += spar.lwrFlange.GetArea() * spar.lwrFlange.element.material.GetEffEx();
    }
    for (auto& stringer : uprStringers)
        eff += stringer.GetEffArea(uprSkin.element.material.GetThickness()) * stringer.element.material.GetEffEx();
    for (auto& stringer : lwrStringers)
        eff += stringer.GetEffArea(lwrSkin.element.material.GetThickness()) * stringer.element.material.GetEffEx();
    eff += uprSkin.GetEffArea() * uprSkin.element.material.GetEffEx();
    eff += lwrSkin.GetEffArea() * lwrSkin.element.material.GetEffEx();
    return eff;
}

Stiffness StructuralSection::GetExBar()
{
    return GetEffAE() / GetEffArea();
}

// Not really
Line3d StructuralSection::GetNA()
{
    return Line3d::Through(spars[0].web.element.centroid, spars[1].web.element.centroid);
}

BendingStiffness StructuralSection::GetExIzBar()
{
    auto NA = GetNA();

    BendingStiffness result;

    auto yToNA = (Length)NA.distance(uprSkin.verts.front());
    SecondAreaMoment IzUprSkin = uprSkin.GetEffArea() * (yToNA * yToNA);
    BendingStiffness ExIzUprSkin = uprSkin.GetEffEx() * IzUprSkin;
    result += ExIzUprSkin;

    yToNA = (Length)NA.distance(lwrSkin.verts.front());
    SecondAreaMoment IzLwrSkin = lwrSkin.GetEffArea() * (yToNA * yToNA);
    BendingStiffness ExIzLwrSkin = lwrSkin.GetEffEx() * IzLwrSkin;
    result += ExIzLwrSkin;

    std::vector<BendingStiffness> ExIzUprStringers;
    for (auto& uprStringer : uprStringers)
    {
        yToNA = (Length)NA.distance(uprStringer.element.centroid);
        SecondAreaMoment Iz = uprStringer.GetEffArea(uprSkin.element.material.GetThickness()) * (yToNA * yToNA);
        ExIzUprStringers.push_back(uprStringer.element.material.GetEffEx() * Iz);
        result += ExIzUprStringers.back();
    }

    std::vector<BendingStiffness> ExIzLwrStringers;
    for (auto& stringer : lwrStringers)
    {
        yToNA = (Length)NA.distance(stringer.element.centroid);
        SecondAreaMoment Iz = stringer.GetEffArea(lwrSkin.element.material.GetThickness()) * (yToNA * yToNA);
        ExIzLwrStringers.push_back(stringer.element.material.GetEffEx() * Iz);
        result += ExIzLwrStringers.back();
    }

    BendingStiffness eifrontupr, eifrontlwr, eirearupr, eirearlwr;
    yToNA = (Length)NA.distance(spars[0].uprFlange.element.centroid);
    auto iz = spars[0].uprFlange.GetArea() * (yToNA * yToNA);
    eifrontupr = spars[0].uprFlange.element.material.GetEffEx() * iz;
    result += eifrontupr;

    yToNA = (Length)NA.distance(spars[0].lwrFlange.element.centroid);
    iz = spars[0].lwrFlange.GetArea() * (yToNA * yToNA);
    eifrontlwr = spars[0].lwrFlange.element.material.GetEffEx() * iz;
    result += eifrontlwr;

    yToNA = (Length)NA.distance(spars[1].uprFlange.element.centroid);
    iz = spars[1].uprFlange.GetArea() * (yToNA * yToNA);
    eirearupr = spars[1].uprFlange.element.material.GetEffEx() * iz;
    result += eirearupr;

    yToNA = (Length)NA.distance(spars[1].lwrFlange.element.centroid);
    iz = spars[1].lwrFlange.GetArea() * (yToNA * yToNA);
    eirearlwr = spars[1].lwrFlange.element.material.GetEffEx() * iz;
    result += eirearlwr;

    auto h = spars[0].GetWebHeight();
    iz = spars[0].web.element.material.GetThickness() * h * h * h / 12.;
    auto eifrontweb = spars[0].web.element.material.GetEffEx() * iz;
    result += eifrontweb;

    h = spars[1].GetWebHeight();
    iz = spars[1].web.element.material.GetThickness() * h * h * h / 12.;
    auto eirearweb = spars[1].web.element.material.GetEffEx() * iz;
    result += eirearweb;

    return result;
}

SecondAreaMoment StructuralSection::GetIzBar()
{
    exbar = GetExBar();
    izbar = GetExIzBar() / exbar;
    return izbar;
}

double StructuralSection::GetGJBar()
{
    if (gjbar < 0.)
    {
        // Smear upr stringers
        Area uprStrArea;
        for (auto& str : uprStringers)
            uprStrArea += str.GetArea();
        auto uprStrSmearThick = uprStrArea / uprSkin.GetLength();
        /*auto uprStrContr = uprSkin.GetLength() /
            (uprStrSmearThick * uprStringers.front().element.material.GetEffGxy());*/

        /*auto uprSkinContr = uprSkin.GetLength() /
            (uprSkin.element.material.GetThickness() * uprSkin.element.material.GetEffGxy());
        */
        auto uprSkinContr = uprSkin.GetLength() /
            ((uprSkin.element.material.GetThickness() + uprStrSmearThick) * 2 * uprSkin.element.material.GetEffGxy());

        Area lwrStrArea;
        for (auto& str : lwrStringers)
            lwrStrArea += str.GetArea();
        auto lwrStrSmearThick = lwrStrArea / lwrSkin.GetLength();
        /*auto lwrStrContr =  lwrSkin.GetLength() /
            (lwrStrSmearThick * lwrStringers.front().element.material.GetEffGxy());*/
        //bigt += lwrStrContr;

        //auto lwrSkinContr = lwrSkin.GetLength() /
        //    (lwrSkin.element.material.GetThickness() * lwrSkin.element.material.GetEffGxy());

        auto lwrSkinContr = lwrSkin.GetLength() /
            ((lwrSkin.element.material.GetThickness() + lwrStrSmearThick) * 2 * lwrSkin.element.material.GetEffGxy());

        auto frontSparCapSmear = (spars[0].uprFlange.GetArea() + spars[0].lwrFlange.GetArea()) / spars[0].GetWebHeight();

        auto frontSparContr = spars[0].GetWebHeight() /
            ((spars[0].web.element.material.GetThickness() + frontSparCapSmear) * spars[0].web.element.material.GetEffGxy());
        
        auto rearSparCapSmear = (spars[1].uprFlange.GetArea() + spars[1].lwrFlange.GetArea()) / spars[1].GetWebHeight();
        
        auto rearSparContr = spars[1].GetWebHeight() /
            ((spars[1].web.element.material.GetThickness() + rearSparCapSmear) * spars[1].web.element.material.GetEffGxy());

        auto bigt = uprSkinContr + lwrSkinContr + frontSparContr + rearSparContr;
        //bigt += uprStrContr + lwrStrContr;

        /*Area shearArea(0);
        shearArea += uprSkin.GetLength() * uprSkin.element.material.GetThickness();
        shearArea += lwrSkin.GetLength() * lwrSkin.element.material.GetThickness();
        shearArea += spars[0].GetWebHeight() * spars[0].web.element.material.GetThickness();
        shearArea += spars[1].GetWebHeight() * spars[1].web.element.material.GetThickness();
        */
        Area twistyArea = uprSkin.GetLength() * spars[0].GetWebHeight();
        Area intArea = GetInteriorArea();

        gjbar = (4.0 * twistyArea * twistyArea / bigt).GetValue();
    }

    return gjbar;
}

