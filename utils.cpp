#include "pch.hpp"
#include "utils.hpp"


bool EqualDouble(double a, double b)
{
    // wtf why no abs??
    return { std::abs(a - b) < std::numeric_limits<double>::epsilon() };
}

bool LessOrEqualDouble(double a, double b)
{
    return { a < b || EqualDouble(a, b) };
}

bool HigherOrEqualDouble(double a, double b)
{
    return { a > b || EqualDouble(a, b) };
}

Eigen::Vector2d Lerp2(const Eigen::Vector2d& from, const Eigen::Vector2d& to, double scale)
{
    Eigen::Vector2d nupt = from + (to - from) * scale;
    return nupt;
}