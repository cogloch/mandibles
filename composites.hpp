#pragma once
#include "units.hpp"


struct MaterialProperty
{
    Stiffness E;
    Stiffness G;
    Strength St;
    Strength Sc;
    Strength Sg;

    MaterialProperty(Stiffness E_ = 0.0_Pa, Stiffness G_ = 0.0_Pa,
        Strength St_ = 0.0_Pa, Strength Sc_ = 0.0_Pa, Strength Sg_ = 0.0_Pa)
        : E(E_), G(G_), St(St_), Sc(Sc_), Sg(Sg_) {}

    MaterialProperty& SetModulus(Stiffness E_) { E = E_; return *this; }
    MaterialProperty& SetShearModulus(Stiffness G_) { G = G_; return *this; }
    MaterialProperty& SetTensileStrength(Strength St_) { St = St_; return *this; }
    MaterialProperty& SetCompressiveStrength(Strength Sc_) { Sc = Sc_; return *this; }
    MaterialProperty& SetShearStrength(Strength Sg_) { Sg = Sg_; return *this; }
};

struct Ply
{      
    Eigen::Matrix3d GetQMatrix() const
    {
        auto E11 = E1.GetValue();
        auto E22 = E2.GetValue();
        auto v12 = nu;
        auto v21 = E22 / E11 * v12;

        auto Q11 = E11 / (1 - v12 * v21);
        auto Q22 = E22 / (1 - v12 * v21);
        auto Q12 = (v12 * E22) / (1 - v12 * v21);
        auto Q66 = G12.GetValue();

        Eigen::Matrix3d Q;
        Q(0, 0) = Q11;
        Q(0, 1) = Q12;
        Q(1, 0) = Q12;
        Q(1, 1) = Q22;
        Q(2, 2) = Q66;
        return Q;
    }

    Eigen::Matrix3d GetQTrans(double angleDeg) const
    {
        auto th = angleDeg * M_PI / 180.0;
        auto s1 = sin(th);
        auto s2 = pow(s1, 2);
        auto s3 = pow(s1, 3);
        auto s4 = pow(s1, 4);
        auto c1 = cos(th);
        auto c2 = pow(c1, 2);
        auto c3 = pow(c1, 3);
        auto c4 = pow(c1, 4);

        auto q = GetQMatrix();
        auto q11 = q(0, 0);
        auto q12 = q(0, 1);
        auto q22 = q(1, 1);
        auto q66 = q(2, 2);

        auto qt11 = q11 * c4 + 2 * (q12 + 2 * q66) * s2 * c2 + q22 * s4;
        auto qt22 = q11 * s4 + 2 * (q12 + 2 * q66) * s2 * c2 + q22 * c4;
        auto qt12 = (q11 + q22 - 4 * q66) * s2 * c2 + q12 * (s4 + c4);
        auto qt66 = (q11 + q22 - 2 * q12 - 2 * q66) * s2 * c2 + q66 * (s4 + c4);;
        
        // Please be small 
        // Please let me ignore bend-twist coupling for the sake of buckling checks
        // Please...
        auto qt16 = (q11 - q12 - 2 * q66) * s1 * c3 - (q22 - q12 - 2 * q66) * c1 * s3;
        auto qt26 = (q11 - q12 - 2 * q66) * c1 * s3 - (q22 - q12 - 2 * q66) * s1 * c3;

        Eigen::Matrix3d qt;
        qt(0, 0) = qt11;
        qt(0, 1) = qt12;
        qt(1, 0) = qt12;
        qt(1, 1) = qt22;
        qt(2, 2) = qt66;

        // Please be small
        qt(0, 2) = qt16;
        qt(2, 0) = qt16;
        qt(1, 2) = qt26;
        qt(2, 1) = qt26;
        return qt;
    }

    Eigen::Matrix3d GetQ0() const
    {
        return GetQTrans(0.);
    }

    Eigen::Matrix3d GetQ90() const
    {
        return GetQTrans(90.);
    }

    Eigen::Matrix3d GetQ45Pos() const
    {
        return GetQTrans(45);
    }

    Eigen::Matrix3d GetQ45Neg() const
    {
        return GetQTrans(-45);
    }

    Density rho;

    // Stiffness 
    Stiffness E1;      // Fibre direction 
    Stiffness E2;      // Transverse to fibres 
    Stiffness G12;     // Shear modulus
    double nu;         // Poisson ratio

    // Strength 
    Strength S1t;      // Tensile stength, fibre direction
    Strength S1c;      // Compressive strength, fibre direction
    Strength S2t;      // Tensile strength, transverse to fibres
    Strength S2c;      // Compressive strength, transverse to fibres 
    Strength S12;      // Shear strength 

    // Strain to failure [%]
    double eps1t;
    double eps1c;
    double eps2t;
    double eps2c;
    double eps12;

    Ply(Density rho_ = 0_gPerCm3, Stiffness E1_ = 0_Pa, Stiffness E2_ = 0_Pa, Stiffness G12_ = 0_Pa, double nu_ = 0.0,
        Strength S1t_ = 0_Pa, Strength S1c_ = 0_Pa, Strength S2t_ = 0_Pa, Strength S2c_ = 0_Pa, Strength S12_ = 0_Pa,
        double eps1t_ = 0.0, double eps1c_ = 0.0, double eps2t_ = 0.0, double eps2c_ = 0.0, double eps12_ = 0.0)
        : rho(rho_), E1(E1_), E2(E2_), G12(G12_), nu(nu_), S1t(S1t_), S1c(S1c_), S2t(S2t_), S2c(S2c_), S12(S12_)
        , eps1t(eps1t_), eps1c(eps1c_), eps2t(eps2t_), eps2c(eps2c_), eps12(eps12_)
    {
    }

    Ply& SetRho(const Density rho) { this->rho = rho; return *this; }
    Ply& SetE1(const Stiffness E1) { this->E1 = E1; return *this; }
    Ply& SetE2(const Stiffness E2) { this->E2 = E2; return *this; }
    Ply& SetG12(const Stiffness G12) { this->G12 = G12; return *this; }
    Ply& SetNu(const double nu) { this->nu = nu; return *this; }
    Ply& SetS1t(const Strength S1t) { this->S1t = S1t; return *this; }
    Ply& SetS1c(const Strength S1c) { this->S1c = S1c; return *this; }
    Ply& SetS2t(const Strength S2t) { this->S2t = S2t; return *this; }
    Ply& SetS2c(const Strength S2c) { this->S2c = S2c; return *this; }
    Ply& SetS12(const Strength S12) { this->S12 = S12; return *this; }
};

// Unidirectional ply
struct UDProperties
{
    MaterialProperty fibre;     // In fibre direction [1]
    MaterialProperty trans;     // Transverse to fibres [2]
};

// Standard quasi-isotropic layout:
// [0, +/-45, 90]
struct QILayup
{
    enum DirIdx
    {
        Zero, FortyFive, Ninety
    };

    QILayup() = default;
    QILayup(double percZero, double percFortyFive, double percNinety);
    //QILayup(const std::array<double, 3>& percentages);
    QILayup(const Eigen::Vector3d& percentages);
    double& operator[](size_t idx);

    void SetLayup(const Eigen::Vector3d& percentages);
    double GetExFactor() const;
    double GetEyFactor() const;

    Stiffness GetExFor(Stiffness fibreE) const;
    Stiffness GetEyFor(Stiffness fibreE) const;

private:
    //std::array<double, 3> layupPercentages;
    Eigen::Vector3d m_layupPercentages = { 0., 0., 0. };

    double m_exFactor = 0.;
    double m_eyFactor = 0.;

    static const Eigen::Vector3d s_exContrib;
    static const Eigen::Vector3d s_eyContrib;
    
    void CheckLayupSetValid(const Eigen::Vector3d& percentages);
};