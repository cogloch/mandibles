#pragma once
#include "units.hpp"
#include "utils.hpp"


const double g_factorULT = 1.5;
const std::array<double, 2> g_manoeuvreFactors = { 2.5, -1.0 };
const Acceleration g_gravitAcc = 9.80665_mPerS2;

class EllipticalLiftSampler
{
public:
    EllipticalLiftSampler(Weight mtow, Length wingspan)
        : m_wingspan(wingspan)
    {
        if (LessOrEqualDouble(mtow.GetValue(), 0.) ||
            LessOrEqualDouble(wingspan.GetValue(), 0.))
            throw "angriness";

        m_rootLift = (4.0 * mtow) / (M_PI * wingspan);
    }

    ForcePerUnitLength SampleAt(Length spanwiseDistFromRoot)
    {
        const auto twiceFrac = 2.0 * spanwiseDistFromRoot / m_wingspan;
        const double twiceFracSq = std::pow(twiceFrac.GetValue(), 2);
        return m_rootLift * sqrt(1.0 - twiceFracSq);
    }

private:
    ForcePerUnitLength m_rootLift;
    Length m_wingspan;
};

struct LoadingState
{
    double loadFactor;
    Mass engineMass;
    Eigen::Vector3d enginePosition;

    // Jet A @ 15degC
    Density fuelDensity = 0.804_kgPerL;

    std::vector<ShearForce> SF;
    std::vector<BendingMoment> BM;
};