#pragma once


class PiecewiseCurve2d
{
public:
    PiecewiseCurve2d(size_t numPoints = 0);

    void ClearDuplicates();
    void SortPoints();
    void Refine();

    void AddPointAtX(double x);
    bool HasPointAtX(double x);

    size_t GetSize() const { return points.size(); }

    Eigen::Vector2d& operator[](size_t index) { return points[index]; }

    std::vector<Eigen::Vector2d>::iterator begin() { return points.begin(); }
    std::vector<Eigen::Vector2d>::iterator end() { return points.end(); }
    std::vector<Eigen::Vector2d>::reverse_iterator rbegin() { return points.rbegin(); }
    std::vector<Eigen::Vector2d>::reverse_iterator rend() { return points.rend(); }

private:
    std::vector<Eigen::Vector2d> points;
};

class PiecewiseCurve3d
{
public:
    PiecewiseCurve3d(size_t numPoints = 0);

    void ClearDuplicates();
    void SortPoints();
    void Refine();

    void AddPointAtX(double x);
    bool HasPointAtX(double x);

    size_t GetSize() { return points.size(); }

    Eigen::Vector3d& operator[](size_t index) { return points[index]; }

    std::vector<Eigen::Vector3d>::iterator begin() { return points.begin(); }
    std::vector<Eigen::Vector3d>::iterator end() { return points.end(); }

private:
    std::vector<Eigen::Vector3d> points;
};