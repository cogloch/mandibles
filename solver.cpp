#include "pch.hpp"
#include "solver.hpp"
#include "airfoil.hpp"
#include "output_set.hpp"
#include "config_state.hpp"
#include "wing_geometry.hpp"
#include "loading_state.hpp"

#include "wingbox.hpp"
#include "structural_mesh.hpp"
#include "loading_mesh.hpp"

#include "chromosome.hpp"


struct Strut
{
    Length distanceFromRoot = 13297_mm;
    double junctionAngle = 16.0; //* (M_PI / 180.0); // rads 
    Length length = 11464_mm; // between junction and landing gear housing edge
    // Rectangular section
    Length chord = 400_mm;
    Length thickness = 0.15 * 400_mm;
    Length wallThickness = 1_mm;

    struct Metal : Material
    {
        Stiffness m_modulus = 73.1_GPa;          
        Stress m_tensYield = 324_MPa;
        Stress m_tensUlt = 469_MPa;
        Density m_rho = 2.78_gPerCm3;
        
        Density rho() override
        {
            return m_rho;
        }

        Stress tensUlt() override
        {
            return m_tensUlt;
        }

        Stiffness modulus() override
        {
            return m_modulus;
        }
    };

    Strut()
    {
        // HM-CF, EP matrix, 60% Vf
        auto plyProp = Ply()
            .SetRho(1.6_gPerCm3)
            .SetE1(180_GPa)
            .SetE2(8_GPa)
            .SetG12(5_GPa)
            .SetNu(0.3)
            .SetS12(60_MPa)
            // For T-H/max stress failure (ignore)
            .SetS1t(1000_MPa)
            .SetS1c(-850_MPa)
            .SetS2t(40_MPa)
            .SetS2c(-200_MPa);
        cfrp.mechProp = std::make_shared<Ply>(std::move(plyProp));
        cfrp.baseLam.num0 = 8;
        cfrp.baseLam.num45 = 1;
        cfrp.baseLam.num90 = 1;

        al.m_modulus = 73.1_GPa;
        al.m_tensYield = 324_MPa;
        al.m_tensUlt = 469_MPa;
        al.m_rho = 2.78_gPerCm3;
    };

    QiLaminate cfrp;  
    Metal al;
    Material* mat = nullptr;

    Mass GetMass()
    {
        const Volume strutMaterialVolume = GetCrossSection() * length;
        return mat->rho() * strutMaterialVolume;
    }

    bool Ok(Force axialComp, Force axialTens)
    {
        /*auto dirstr = DirStress(abs(axialTens) * 1.5);
        auto buckl = abs(axialComp) * 1.5;
        auto bucklcrit = GlobalBucklingCritLoad();

        return (dirstr < mat->tensUlt()) &&
            (buckl < bucklcrit);*/

        //auto tbuck = ceil((1.5 * abs(axialComp) * length * length 
        //    / (M_PI * M_PI * mat->modulus() * (thickness * thickness * thickness / 12.0 + 2 * chord * (thickness / 2) * (thickness / 2)))).GetValue() * 1000.);
        //tbuck /= 1000.; // ceil to mm and now back to m
        auto tbuck = (1.5 * abs(axialComp) * length * length
            / (M_PI * M_PI * mat->modulus() * (thickness * thickness * thickness / 12.0 + 2 * chord * (thickness / 2) * (thickness / 2)))).GetValue();
        auto it = ceil((tbuck / 0.125_mm).GetValue());
        auto tbuckReal = it * 0.125_mm;
        
        // Solve for min t for buckling (negligible effect on the junction BC)
        wallThickness = 1.5 * tbuckReal; // ULT 
        it = ceil((wallThickness / 0.125_mm).GetValue());
        wallThickness = it * 0.125_mm;

        auto dirstr = DirStress(abs(axialTens) * 1.5);
        auto buckl = abs(axialComp) * 1.5;
        auto bucklcrit = GlobalBucklingCritLoad();

        auto RFstr = (mat->tensUlt() / dirstr).GetValue();
        auto RFbuck = (bucklcrit / buckl).GetValue();

        return (RFstr > 1. && RFbuck > 1.);
    }

    Stress DirStress(Force axial)
    {
        return axial / GetCrossSection();
    }

    SecondAreaMoment I()
    {
        return 2 * wallThickness * thickness * thickness * thickness / 12.0 +
            2 * (thickness / 2) * (thickness / 2) * (chord * wallThickness);
    }

    Area GetCrossSection()
    {
        const auto hole = (chord - 2 * wallThickness) * (thickness - 2 * wallThickness);
        return (chord * thickness) - hole;
    }

    Length GetExtension(Force axialLoad)
    {
        return (axialLoad * length) / (mat->modulus() * GetCrossSection());
    }

    Length GetVerticalExtension(Force axialLoad)
    {
        return GetExtension(axialLoad) * sin(junctionAngle * (M_PI / 180.0));
    }

    Force GlobalBucklingCritLoad()
    {
        return M_PI * M_PI * mat->modulus() * I() / (1.5 * length * length);
    }
};

// OLD!!!
// TODO: CLEANUP
std::vector<Length> Deflect(Wing& wing, Wingbox& box, double n, Strut* strut = nullptr, Force* strutLoad = nullptr)
{
    Point3d strutPos = { strut->distanceFromRoot.GetValue(), 0., 0. };

    auto vol = box.envelope.GetVolumeBetweenSections(0, box.envelope.sections.size() - 1);

    LoadingState loading;
    auto fuelMass = vol * loading.fuelDensity;

    const Mass mtom = 62'268_kg;
    EllipticalLiftSampler liftSampler(mtom * g_gravitAcc, wing.GetSpan());

    Point3d enginePos(Length(5431_mm).GetValue(), 0., 0.);
    Weight engineWeight(-2079.76_kg * g_gravitAcc);

    std::vector<std::pair<Point3d, Force>> pointLoads = {
        {enginePos, engineWeight * n}
    };

    if (strutLoad)
        pointLoads.push_back({ strutPos, *strutLoad * n });

    size_t numSec = box.envelope.sections.size();
    loading.SF.clear();
    loading.SF.resize(numSec);
    loading.BM.clear();
    loading.BM.resize(numSec);

    loading.SF.back() = 0_N;
    loading.BM.back() = 0_Nm;
    for (int i = numSec - 2; i >= 0; --i)
    {
        // Integrate distributed loads 
        auto dx = box.envelope.GetDistanceBetweenSections(i, i + 1);

        const auto curX = box.envelope.sections[i].verts.front().x();
        const auto nextX = box.envelope.sections[i + 1].verts.front().x();
        auto avgLoad = 0.5 * (liftSampler.SampleAt(curX) + liftSampler.SampleAt(nextX)) * n;
        loading.SF[i] = loading.SF[i + 1] + avgLoad * dx;

        auto avgShear = 0.5 * (loading.SF[i] + loading.SF[i + 1]);
        loading.BM[i] = loading.BM[i + 1] + avgShear * dx;

        // Add point loads 
        for (auto it = pointLoads.begin(); it != pointLoads.end(); ++it)
        {
            if (it->first.x() > curX)
            {
                dx = it->first.x() - curX;
                loading.SF[i] += it->second;
                loading.BM[i] += it->second * dx;
                //it = pointLoads.erase(it);
                //if (it == pointLoads.end()) break;
            }
        }
    }

    //box.TempComputeLoadState();
    if (!strutLoad)
    {
        box.cleanSF = loading.SF;
        box.cleanBM = loading.BM;
    }
    else
    {
        box.SF = loading.SF;
        box.BM = loading.BM;
    }
    box.ComputeSectionalProperties();
    return box.GetDeflection(strutLoad == nullptr);
}

template<typename T>
Percentage PercentDifference(T a, T b)
{
    return (abs(a - b) / (0.5 * (a + b))) * 100_percent;
}

Solver::Solver(std::shared_ptr<ConfigState> configState,
    std::shared_ptr<OutputSet> outputSet)
    : m_configState(configState)
    , m_outputSet(outputSet)
{
}

#include <optional>

enum MassIdx
{
    STRUT, SKIN, STRINGER, SPAR, RIBS
};
std::optional<std::array<Mass, 5>> TestTrial(StructuralMesh/*&*/ mesh,
    Chromosome& trial,
    /*LoadingMesh& cleanLoad*/
    const SectionalEnvelope& airMesh,
    Mass totalWingStructuralMass,

    bool killIfFail,
    bool useStrut,
    bool output = false)
{
    //Mass totalMass = 0_kg;
    std::array<Mass, 5> masses;

    mesh.runSec = trial.rootSection;
    mesh.taper = trial.taper;
    if (!mesh.BuildLattice())
        return {};

    Mass skinMass(0_kg);
    Mass stringerMass(0_kg);
    Mass sparMass(0_kg);
    for (size_t i = 1; i < mesh.sections.size(); ++i)
    {
        auto& prev = mesh.sections[i - 1];
        auto& cur = mesh.sections[i];
        const Length dx = abs(cur.GetCentroid().x() - prev.GetCentroid().x());
        const auto rho = cur.uprSkin.element.material.mechProp->rho;

        // this is shit
        //Area meanUprSkin = 0.5 * (prev.uprSkin.GetEffArea() + cur.uprSkin.GetEffArea());
        //Area meanLwrSkin = 0.5 * (prev.lwrSkin.GetEffArea() + cur.lwrSkin.GetEffArea());
        //skinMass = (meanUprSkin * dx + meanLwrSkin * dx) * rho;
        skinMass += (cur.uprSkin.GetEffArea() * dx + cur.lwrSkin.GetEffArea() * dx) * rho;

        // Use runout not means
        for (auto& stringer : cur.uprStringers)
            stringerMass += stringer.GetArea() * dx * rho;

        for (auto& stringer : cur.lwrStringers)
            stringerMass += stringer.GetArea() * dx * rho;

        //Area meanFrontSpar = 0.5 * (prev.spars[0].GetEffArea() + cur.spars[0].GetEffArea());
        //Area meanRearSpar = 0.5 * (prev.spars[1].GetEffArea() + cur.spars[1].GetEffArea());
        //sparMass += (meanFrontSpar * dx + meanRearSpar * dx) * rho;
        sparMass += (cur.spars[0].GetEffArea() * dx + cur.spars[1].GetEffArea() * dx) * rho;
    }

    masses[SKIN] = skinMass;
    masses[STRINGER] = stringerMass;
    masses[SPAR] = sparMass;
    //totalMass += skinMass + stringerMass + stringerMass;

    LoadingMesh loadingMesh;

    const Mass mtom = 69'254_kg;
    const Mass engineMass = 3671.81_kg;
    Vec3d enginePos(Length(5431_mm).GetValue(), 0., 0.374);
    const Length wingspan = 47.9_m;

    loadingMesh.pointLoads.push_back(PointLoad(-engineMass * g_gravitAcc, enginePos));

    Mass fuelPerWing = 5748.0037_kg / 2.0;

    //const Mass wingmass = 530.8180053_kg; // ????
    //const Mass wingmass = 3000_kg;

    std::vector<double> spanwiseSamples;
    for (auto& sec : mesh.sections)
        spanwiseSamples.push_back(sec.GetCentroid().x());
    loadingMesh.distribLoads.push_back(
        GenerateWingMassDistrib(spanwiseSamples, wingspan, totalWingStructuralMass, airMesh));
    loadingMesh.distribLoads.push_back(
        GenerateFuelLoadDistrib(spanwiseSamples, wingspan, fuelPerWing, mesh.sections));
    loadingMesh.distribLoads.push_back(
        GenerateLiftDistribution(spanwiseSamples, wingspan, mtom, airMesh));

    ReferenceAxis refAxis;
    for (auto& sec : mesh.sections)
        refAxis.pts.push_back(sec.GetCentroid());

    //auto l = loadingMesh.SolveAt(refAxis, 2.5);
    //auto defl = structMesh.Deflect(l);
    {
        /*auto loadingPos25 = loadingMesh.SolveAt(refAxis, 2.5);

        auto rfs = mesh.GetDirStressRF(loadingPos25);
        for (auto& rf : rfs)
            if (rf < 1.0 && killIfFail)
                return {};*/
    }

    std::vector<Length> deflection;
    Force strutLoadComp, strutLoadTens;
    Strut strut;
    if (useStrut)
    {
        static const Length strutConvTol = 10_mm;

        strut.distanceFromRoot = trial.distFromRoot;
        auto projectedRoot = trial.distFromRoot - (trial.fuselageHeight / 2.); // Remove one fuselage radius
        strut.chord = trial.strutChord;
        strut.length = sqrt((projectedRoot * projectedRoot
            + trial.fuselageHeight * trial.fuselageHeight).GetValue());
        strut.junctionAngle =
            (atan((trial.fuselageHeight / projectedRoot).GetValue())) * (180.0 / M_PI);
        strut.thickness = trial.strutTC.GetValue() / 100.0 * strut.chord;
        strut.wallThickness = trial.strutWallThickness;

        //strut.mat = &strut.al;
        //auto buc = strut.GlobalBucklingCritLoad();
        strut.mat = &strut.cfrp;
        //buc = strut.GlobalBucklingCritLoad();
        //auto cstress = 100'000_N / strut.GetCrossSection();

        bool bisect = true;

        if (bisect)
        {
            bool converged = false;
            size_t maxIterations = 100;
            size_t iteration = 0;
            size_t secBeforeStrutIndex = 0;
            for (size_t i = 0; i < mesh.sections.size(); ++i)
            {
                auto& sec = mesh.sections[i];
                if (sec.GetCentroid().x() > strut.distanceFromRoot.GetValue())
                {
                    secBeforeStrutIndex = i - 1;
                    break;
                }
            }
            Vec3d strutPos = {
                Length(strut.distanceFromRoot).GetValue(),
                0.,
                0.5 * (mesh.sections[secBeforeStrutIndex].GetCentroid().z()
                            + mesh.sections[secBeforeStrutIndex + 1].GetCentroid().z()) * 0.65
            };
            //auto maxTens = strut.tensUlt * strut.GetCrossSection() / 1.5;
            //Force strutLoadMin = -1'000'000_N;
            //Force strutLoadMax = 0_N;
            auto loadMin = 0_N;
            auto loadMax = 1'000'000_N;
            //strutLoadTens = 0.5 * (strutLoadMin + strutLoadMax);
            strutLoadTens = 0.5 * (loadMin + loadMax);
            loadingMesh.pointLoads.push_back(PointLoad(0_N, strutPos));
            Length tipDeflTens;
            while (!converged && ++iteration < maxIterations)
            {
                // All loads (including the tens trial) at +2.5g!
                const double n = 2.5;
                // Reactions!
                auto horiz = -abs(strutLoadTens * cos(strut.junctionAngle * (M_PI / 180.0)));
                auto verti = -abs(strutLoadTens * sin(strut.junctionAngle * (M_PI / 180.0)));
                loadingMesh.pointLoads.back().force = verti;
                auto l = loadingMesh.SolveAt(refAxis, n);
                deflection = mesh.Deflect(l);
                tipDeflTens = deflection.back();

                auto meanDeflAtJunction = 0.5 * (deflection[secBeforeStrutIndex] + deflection[secBeforeStrutIndex + 1]);
                //auto deflStrut = strut.GetVerticalExtension(-strutLoadTens * n); // Tension reaction 
                auto deflStrut = strut.GetVerticalExtension(strutLoadTens); 

                if (abs(deflStrut - meanDeflAtJunction) < strutConvTol)
                {
                    converged = true;
                    break;
                }
                else
                {
                    if (deflStrut > meanDeflAtJunction)
                        loadMax = strutLoadTens;
                    else
                        loadMin = strutLoadTens;
                    // High leads to hogging!!
                    /*if (abs(deflStrut) > abs(meanDeflAtJunction))
                        loadMin = strutLoadTens;
                    else
                        loadMax = strutLoadTens;*/
                    strutLoadTens = 0.5 * (loadMin + loadMax);
                }
            }

            if (!converged && killIfFail)
                return{};

            //strutLoadMin = 0_N;
            //strutLoadMax = 1'000'000_N;
            loadMin = 0_N;
            loadMax = 1'000'000_N;
            strutLoadComp = 0.5 * (loadMin + loadMax);
            iteration = 0;
            converged = false;
            while (!converged && ++iteration < maxIterations)
            {
                const double n = -1;
                // Reactions!
                auto horiz = abs(strutLoadComp * cos(strut.junctionAngle * (M_PI / 180.0)));
                auto verti = abs(strutLoadComp * sin(strut.junctionAngle * (M_PI / 180.0)));
                //loadingMesh.pointLoads.back().force = -strutLoadComp;
                loadingMesh.pointLoads.back().force = -verti;    // Sign flipped in SolveAt()!!!
                
                //auto l = loadingMesh.SolveAt(refAxis, n);
                auto lPos = loadingMesh.SolveAt(refAxis, 2.5);
                auto l = lPos;
                for (size_t i = 0; i < lPos.size(); ++i)
                {
                    auto& neg1 = l[i];
                    auto& pos25 = lPos[i];
                    // TODO: wtf is happening in SolveAt() at -1g??
                    neg1.shear = -pos25.shear / 2.5;
                    neg1.streamMoment = -pos25.streamMoment / 2.5;
                    neg1.spanMoment = -pos25.spanMoment / 2.5;
                }

                deflection = mesh.Deflect(l);

                auto meanDeflAtJunction = 0.5 * (deflection[secBeforeStrutIndex] + deflection[secBeforeStrutIndex + 1]);
                //auto deflStrut = strut.GetVerticalExtension(-strutLoadComp * n); // Com reaction 
                auto deflStrut = strut.GetVerticalExtension(-strutLoadComp); // Com reaction 

                if (abs(deflStrut - meanDeflAtJunction) < strutConvTol)
                {
                    converged = true;
                    break;
                }
                else
                {
                    if (deflStrut > meanDeflAtJunction)
                        loadMin = strutLoadComp;
                    else
                        loadMax = strutLoadComp;
                    /*if (deflStrut > meanDeflAtJunction)
                        loadMax = strutLoadComp;
                    else
                        loadMin = strutLoadComp;*/
                    strutLoadComp = 0.5 * (loadMin + loadMax);
                }
            }

            if (!converged && killIfFail)
                return{};
        }
        else
        {
            size_t secBeforeStrutIndex = 0;
            //strutConvTol = 
            for (size_t i = 0; i < mesh.sections.size(); ++i)
            {
                auto& sec = mesh.sections[i];
                if (sec.GetCentroid().x() > strut.distanceFromRoot.GetValue())
                {
                    secBeforeStrutIndex = i - 1;
                    break;
                }
            }
            Vec3d strutPos = {
                Length(strut.distanceFromRoot).GetValue(),
                0.,
                0.5 * (mesh.sections[secBeforeStrutIndex].GetCentroid().z()
                            + mesh.sections[secBeforeStrutIndex + 1].GetCentroid().z())
            };
            loadingMesh.pointLoads.push_back(PointLoad(0_N, strutPos));

            strutLoadTens = 0;
            bool converged = false;
            while (!converged)
            {
                const double n = 2.5;
                auto horizontal = -abs(strutLoadTens * cos(strut.junctionAngle * (M_PI / 180.0)));
                auto vertical = -abs(strutLoadTens * sin(strut.junctionAngle * (M_PI / 180.0)));
                loadingMesh.pointLoads.back().force = vertical;
                auto l = loadingMesh.SolveAt(refAxis, n);
                deflection = mesh.Deflect(l);
                
                auto meanDeflAtJunction = 0.5 * (deflection[secBeforeStrutIndex] + deflection[secBeforeStrutIndex + 1]);
                auto deflStrut = strut.GetVerticalExtension(strutLoadTens * n); // Tension reaction 

                if (abs(deflStrut - meanDeflAtJunction) < strutConvTol)
                {
                    converged = true;
                    break;
                }

                strutLoadTens += 100;
            }
        }

        //strutLoadTens = -strutLoadTens;
        //strutLoadComp = -strutLoadComp;
        strutLoadComp = strutLoadTens / (-2.5);
        /*if (!strut.Ok(strutLoadTens, strutLoadComp) && killIfFail)
            return {};*/
        if (!strut.Ok(strutLoadComp, strutLoadTens) && killIfFail)
            return {};
        //totalMass += strut.GetMass();
        masses[STRUT] = strut.GetMass();
    }

    // Check the rest at +2.5g
    const double n = 2.5;
    auto compLoading = loadingMesh;
    auto cleanLoading = loadingMesh;
    if (useStrut)
    {
        cleanLoading.pointLoads.erase(cleanLoading.pointLoads.begin() + 1);
        
        //compLoading.pointLoads.back().force = -strutLoadComp;
        //loadingMesh.pointLoads.back().force = -strutLoadTens;

        // At -ve g, put in a flipped reaction (flipped again to the correct direction in SolveAt()) 
        compLoading.pointLoads.back().force = -abs(strutLoadComp * sin(strut.junctionAngle * (M_PI / 180.0)));
        // Actual sign at +ve g (no flipping anywhere)
        loadingMesh.pointLoads.back().force = -abs(strutLoadTens * sin(strut.junctionAngle * (M_PI / 180.0)));
    }
    auto loadingPos25 = loadingMesh.SolveAt(refAxis, 2.5);
    auto cleanLoading25 = cleanLoading.SolveAt(refAxis, 2.5);
    auto loadingPos1 = loadingMesh.SolveAt(refAxis, 1.0);
    auto loadingNeg1 = compLoading.SolveAt(refAxis, -1.0);

    for (size_t i = 0; i < loadingNeg1.size(); ++i)
    {
        auto& neg1 = loadingNeg1[i];
        auto& pos25 = loadingPos25[i];
        // TODO: wtf is happening in SolveAt() at -1g??
        neg1.shear = -pos25.shear / 2.5;
        neg1.streamMoment = -pos25.streamMoment / 2.5;
        neg1.spanMoment = -pos25.spanMoment / 2.5;
    }

    if (output)
    {
        std::ofstream fout25sf("loading_pos25_sf");
        std::ofstream fout25bm("loading_pos25_bm");
        std::ofstream fout25t("loading_pos25_t");
        std::ofstream fout1sf("loading_neg1_sf");
        std::ofstream fout1bm("loading_neg1_bm");
        std::ofstream fout1t("loading_neg1_t");

        for (auto& sec : loadingPos25)
        {
            fout25sf << sec.shear.GetValue() << "\n";
            fout25bm << sec.spanMoment.GetValue() << "\n";
            fout25t << sec.streamMoment.GetValue() << "\n";
        }

        for (auto& sec : loadingPos1)
        {
            fout1sf << sec.shear.GetValue() << "\n";
            fout1bm << sec.spanMoment.GetValue() << "\n";
            fout1t << sec.streamMoment.GetValue() << "\n";
        }

        fout25sf.close();
        fout25bm.close();
        fout25t.close();
        fout1sf.close();
        fout1bm.close();
        fout1t.close();
    }

    // Stiffness checks
    // Deflection
    deflection = mesh.Deflect(loadingPos25);
    
    if (output)
    {
        std::ofstream foutDefl25("deflect25");
        for (auto& defl : deflection)
        {
            foutDefl25 << defl.GetValue() << "\n";
        }

        foutDefl25.close();
    }

    auto vol = mesh.boxEnvelope.GetVolumeBetweenSections(0, mesh.boxEnvelope.sections.size() - 1);
    LoadingState loading;
    auto fuelMass = vol * loading.fuelDensity;

    static Length maxTipDefl = 0.1 * wingspan / 2.0;
    if (deflection.back() > maxTipDefl && killIfFail)
        return {};
    // Torsion
    auto twist = mesh.Twist(loadingPos25);
    //auto twist = mesh.Twist(cleanLoading25);
    auto tipTwistDeg = twist.back() * (180.0 / M_PI);
    static const double maxTipTwistDeg = 5.0;
    if (tipTwistDeg > maxTipTwistDeg && killIfFail)
        return {};

    if (output)
    {
        std::ofstream foutTw25("twist25");
        for (auto& tw : twist)
        {
            foutTw25 << tw * (180.0 / M_PI) << "\n";
        }

        foutTw25.close();
    }

    // Dir stress checks 
    const double ULT = 1.5;
    auto rfs = mesh.GetDirStressRF(loadingPos25);
    for (auto& rf : rfs)
        if (rf < 1.0 && killIfFail)
            return {};

    // Shear stress checks
    auto tau = mesh.GetShearStresses(loadingPos25);
    bool ok = true;
    for (int i = 0; i < mesh.sections.size(); ++i)
    {
        auto& sec = mesh.sections[i];
        auto& secTau = tau[i];

        auto str = (Stress)secTau.front() * ULT;
        auto all = sec.uprSkin.element.material.GetAllTau();
        if (str > all)
        {
            ok = false;
            break;
        }

        str = (Stress)secTau.back() * ULT;
        all = sec.spars[0].web.element.material.GetAllTau();
        if (str > all)
        {
            ok = false;
            break;
        }
    }

    if (!ok && killIfFail)
        return {};

    mesh.GetDirStressRF(loadingNeg1, StructuralMesh::Case::Neg1);
    //auto ribPitch = (wingspan / 2.0) / (mesh.sections.size() - 1);
    //auto ribFac = 1;
    //ribPitch = ribPitch / ribFac;

    double minRibPitch = std::numeric_limits<double>::max();
    std::vector<double> sectionRibPitch(mesh.sections.size());
    for (auto& pitch : sectionRibPitch)
        pitch = std::numeric_limits<double>::max();

    // Stringer/spar flange column buckling 
    bool stabOk = true;
    for (size_t secI = 0; secI < mesh.sections.size(); ++secI)
    {
        auto& sec = mesh.sections[secI];
        for (auto& spar : sec.spars)
        {
            //auto crit = ppCoeff * M_PI * spar.uprFlange.element.material.GetEffEx() * spar.uprFlange.I()
            //    / (ribPitch * ribPitch * spar.uprFlange.GetArea());
            // Kassapoglou pp 200
            auto c = 1.88; // pinned-pinned
            double ex = spar.uprFlange.element.material.GetEffEx().GetValue();
            double i = spar.uprFlange.I().GetValue();
            double a = spar.uprFlange.GetArea().GetValue();
            double str = spar.uprFlange.element.dirStress[StructuralElement::Case::Pos25].GetValue();
            str = abs(str);
            // TODO ceil
            auto curPitch = sqrt((c * M_PI * ex * i) / (a * str));
            sectionRibPitch[secI] = std::min(sectionRibPitch[secI], curPitch);
            minRibPitch = std::min(minRibPitch, curPitch);
            /*if (spar.uprFlange.element.dirStress[StructuralElement::Case::Pos25] > crit)
            {
                stabOk = false;
                break;
            }*/

            /*crit = ppCoeff * M_PI * spar.lwrFlange.element.material.GetEffEx() * spar.lwrFlange.I()
                / (ribPitch * ribPitch * spar.lwrFlange.GetArea());
            if (abs(spar.lwrFlange.element.dirStress[StructuralElement::Case::Neg1]) > crit)
            {
                stabOk = false;
                break;
            }*/
            ex = spar.lwrFlange.element.material.GetEffEx().GetValue();
            i = spar.lwrFlange.I().GetValue();
            a = spar.lwrFlange.GetArea().GetValue();
            str = spar.lwrFlange.element.dirStress[StructuralElement::Case::Neg1].GetValue();
            str = abs(str);
            curPitch = sqrt((c * M_PI * ex * i) / (a * str));
            sectionRibPitch[secI] = std::min(sectionRibPitch[secI], curPitch);
            minRibPitch = std::min(minRibPitch, curPitch);
        }

        for (auto& stringer : sec.uprStringers)
        {
            auto c = 1.88; // pinned-pinned
            double ex = stringer.element.material.GetEffEx().GetValue();
            double i = stringer.I().GetValue();
            double a = stringer.GetArea().GetValue();
            double str = stringer.element.dirStress[StructuralElement::Case::Pos25].GetValue();
            str = abs(str);
            if (str < std::numeric_limits<double>::min())
                continue;
            auto curPitch = sqrt((c * M_PI * ex * i) / (a * str));
            sectionRibPitch[secI] = std::min(sectionRibPitch[secI], curPitch);
            minRibPitch = std::min(minRibPitch, curPitch);

            //auto ppCoeff = 1.88; // pinned-pinned
            //auto crit = ppCoeff * M_PI * stringer.element.material.GetEffEx() * stringer.I()
            //    / (ribPitch * ribPitch * stringer.GetArea());
            //if (stringer.element.dirStress[StructuralElement::Case::Pos25] > crit)
            //{
            //    stabOk = false;
            //    break;
            //}
        }

        for (auto& stringer : sec.lwrStringers)
        {
            //auto ppCoeff = 1.88; // pinned-pinned
            //auto crit = ppCoeff * M_PI * stringer.element.material.GetEffEx() * stringer.I()
            //    / (ribPitch * ribPitch * stringer.GetArea());
            //if (abs(stringer.element.dirStress[StructuralElement::Case::Neg1]) > crit)
            //{
            //    stabOk = false;
            //    break;
            //}
            auto c = 1.88; // pinned-pinned
            double ex = stringer.element.material.GetEffEx().GetValue();
            double i = stringer.I().GetValue();
            double a = stringer.GetArea().GetValue();
            double str = stringer.element.dirStress[StructuralElement::Case::Neg1].GetValue();
            str = abs(str);
            if (str < std::numeric_limits<double>::min())
                continue;
            auto curPitch = sqrt((c * M_PI * ex * i) / (a * str));
            sectionRibPitch[secI] = std::min(sectionRibPitch[secI], curPitch);
            minRibPitch = std::min(minRibPitch, curPitch);
        }

        if (!stabOk)
            break;
    }

    if (!stabOk && killIfFail)
        return{};

    Mass ribMass = 0_kg;
    //int numRibs = std::ceil(wingspan.GetValue() / 2 / minRibPitch) + 2;
    double curRib = 0;
    std::vector<double> ribMasses;
    while (curRib < wingspan.GetValue() / 2.)
    {
        StructuralSection* prev = nullptr;
        StructuralSection* next = nullptr;
        // Follow the actual airfoil, not just box
        const WingboxSection* airPrev = nullptr;  // Not actual "wingbox"section, just shitty naming 
        const WingboxSection* airNext = nullptr;
        for (int i = 1; i < mesh.sections.size() - 1; ++i)
        {
            if (mesh.sections[i].GetCentroid().x() > curRib)
            {
                prev = &mesh.sections[i];
                next = &mesh.sections[i + 1];
                airPrev = &airMesh.sections[i];
                airNext = &airMesh.sections[i + 1];
                break;
            }
        }

        if (!prev || !next)
        {
            curRib += minRibPitch;
            continue;
        }
        auto ribArea = 0.5 * (airPrev->GetSectionalArea() + airNext->GetSectionalArea()).GetValue();
        auto ribSkinThickness = 0.5 * (prev->uprSkin.element.material.GetThickness()
            + next->uprSkin.element.material.GetThickness()).GetValue();

        ribMasses.push_back(ribArea * ribSkinThickness * prev->uprSkin.element.material.mechProp->rho.GetValue());
        ribMass += ribMasses.back();

        curRib += minRibPitch;
    }

    // Last rib 
    ribMasses.push_back((airMesh.sections.back().GetSectionalArea() * mesh.sections.back().uprSkin.element.material.GetThickness()
        * mesh.sections.back().uprSkin.element.material.mechProp->rho).GetValue());
    
    bool hasEngineRib = false, hasStrutRib = false;
    for (size_t i = 0; i < mesh.sections.size(); ++i)
    {
        auto& sec = mesh.sections[i];
        if (sec.GetCentroid().x() > enginePos.x())
        {
            if (!hasEngineRib)
            {
                auto meanArea = 0.5 * (airMesh.sections[i].GetSectionalArea() + airMesh.sections[i - 1].GetSectionalArea());
                ribMasses.push_back((meanArea * sec.uprSkin.element.material.GetThickness()
                    * sec.uprSkin.element.material.mechProp->rho).GetValue());
                hasEngineRib = true;
            }
        }

        if (sec.GetCentroid().x() > strut.distanceFromRoot.GetValue())
        {
            if (!hasStrutRib)
            {
                auto meanArea = 0.5 * (airMesh.sections[i].GetSectionalArea() + airMesh.sections[i - 1].GetSectionalArea());
                ribMasses.push_back((meanArea * sec.uprSkin.element.material.GetThickness()
                    * sec.uprSkin.element.material.mechProp->rho).GetValue());
                hasStrutRib = true;
            }
        }
    }
    
    ribMass += ribMasses.back();

    /*
    for (auto& sec : mesh.sections)
    {
        ribMass += (sec.uprSkin.element.material.GetThickness() * sec.GetInteriorArea())
            * sec.uprSkin.element.material.mechProp->rho * ribFac;
    }*/

    masses[MassIdx::RIBS] = ribMass;
   
    // Panels
    auto mat = mesh.sections.front().uprSkin.element.material.mechProp;
    auto nu_xy = 0.33; // 0/45/90 nu^star
                    // assume nu_yx = E22/E11 nu_xy
    //auto nu_yx = (mat->E2 / mat->E1 * nu_xy).GetValue();
    // Upr
    for (size_t secI = 0; secI < mesh.sections.size(); ++secI)
    {
        auto& sec = mesh.sections[secI];
        auto e1 = sec.uprSkin.element.material.mechProp->E1.GetValue();
        auto ex = sec.uprSkin.element.material.GetEffEx().GetValue();
        auto ey = sec.uprSkin.element.material.GetEffEy().GetValue();
        auto gxy = sec.uprSkin.element.material.GetEffEx().GetValue(); 
        
        auto nu_yx = ey / ex * nu_xy;

        bool cranfield = false;
        if (cranfield)
        {
            auto kp = M_PI * M_PI / (6.0 * e1 * (1 - nu_xy * nu_yx))
                * (sqrt(ex * ey) + 0.5 * (nu_xy * ey) + 0.5 * (nu_yx * ex) + 2.0 * (1 - nu_xy * nu_yx) * gxy);
            auto pw = (sec.uprSkin.element.dirStress[StructuralElement::Pos25] *
                sec.uprSkin.element.material.GetThickness()).GetValue() / sectionRibPitch[secI];
            auto bstr = sec.uprSkin.GetLength().GetValue() / (sec.uprStringers.size() - 1);
            auto askin = sec.uprSkin.GetEffArea().GetValue();
            double astr = 0.;
            for (auto& stringer : sec.uprStringers)
            {
                astr += stringer.GetArea().GetValue();
            }
            auto ksk = askin / (askin + astr);
            double t = 1.1 * std::cbrt(pw / (kp * e1) * std::pow(bstr / ksk, 2));
            if (t > sec.uprSkin.element.material.GetThickness().GetValue())
            {
                stabOk = false;
                break;
            }
        }
        else
        {
            // Based on D matrix | Combined direct & shear
            // Panel length given by stringer pitch 
            //auto skinlen = sec.uprSkin.GetLength();
            Length b = (sec.uprStringers[1].element.centroid - sec.uprStringers[0].element.centroid).norm();
            // Given by rib pitch 
            Length a = minRibPitch; // Const
            auto AR = (a / b).GetValue();
            // Per unit length (section-wise)
            auto nx = (sec.uprSkin.element.dirStress[StructuralElement::Pos25] * sec.uprSkin.element.material.GetThickness()).GetValue();
            if (nx > 0)
            {
                // Tension, don't care
                continue;
            }
            nx = abs(nx);
            auto nxy = sec.uprSkin.element.maxShearFlow;

            // Crit loads 
            auto d = sec.uprSkin.element.material.GetMatrixD();
            auto d11 = d(0, 0);
            auto d12 = d(0, 1);
            auto d22 = d(1, 1);
            auto d66 = d(2, 2);

            // Please be small
            auto d16 = d(0, 2);
            auto d26 = d(1, 2);

            auto pi2 = pow(M_PI, 2.0);
            auto ar2 = pow(AR, 2.);
            auto ar4 = pow(AR, 4.);
            auto a2 = pow(a.GetValue(), 2.);

            // Minimize for number of half-waves m 

            // Direct uniaxial
            int halfWavesBWiseDir = 1;
            double nxcr = std::numeric_limits<double>::max();
            for (int m = 1; m < 20; ++m)
            {
                auto m2 = pow(m, 2.);
                auto m4 = pow(m, 4.);
                auto ncr = pi2 * (d11 * m4 + 2 * (d12 + 2 * d66) * m2 * ar2 + d22 * ar4) / (a2 * m2);
                if (ncr < nxcr)
                {
                    nxcr = ncr;
                    halfWavesBWiseDir = m;
                }
            }

            // Shear
            auto beta = pow((d11 / d22), 1. / 4.);
            auto frac = (d12 + 2 * d66) / sqrt(d11 * d22);
            auto A = -0.27 + 0.185 * frac;
            auto B = 0.82 + 0.46 * frac - 0.2 * pow(frac, 2.);
            auto K = 8.2 + 5 * frac * 1 / (pow(10, A / beta + B * beta));
            auto nxycr = 4 / pow(b.GetValue(), 2.) * pow((d11 * pow(d22, 3.)), 1. / 4.) * K;

            auto rc = nx / nxcr;
            auto rs = nxy / nxycr;
            auto fi = rc + pow(rs, 2.);
            auto rf = 1 / sqrt(fi);
            if (rf < 1.)
            {
                stabOk = false;
                break;
            }
        }
    }

    if (!stabOk && killIfFail)
        return{};

    // Lwr
    for (size_t secI = 0; secI < mesh.sections.size(); ++secI)
    {
        auto& sec = mesh.sections[secI];
        auto e1 = sec.lwrSkin.element.material.mechProp->E1.GetValue();
        auto ex = sec.lwrSkin.element.material.GetEffEx().GetValue();
        auto ey = sec.lwrSkin.element.material.GetEffEy().GetValue();
        auto gxy = sec.lwrSkin.element.material.GetEffEx().GetValue();

        auto nu_yx = ey / ex * nu_xy;

        bool cranfield = false;
        if (cranfield)
        {
            auto kp = M_PI * M_PI / (6.0 * e1 * (1 - nu_xy * nu_yx))
                * (sqrt(ex * ey) + 0.5 * (nu_xy * ey) + 0.5 * (nu_yx * ex) + 2.0 * (1 - nu_xy * nu_yx) * gxy);

            auto pw = (sec.lwrSkin.element.dirStress[StructuralElement::Neg1] *
                sec.lwrSkin.element.material.GetThickness()).GetValue() / sectionRibPitch[secI];
            auto bstr = sec.lwrSkin.GetLength().GetValue() / (sec.uprStringers.size() - 1);
            auto askin = sec.lwrSkin.GetEffArea().GetValue();
            double astr = 0.;
            for (auto& stringer : sec.uprStringers)
            {
                astr += stringer.GetArea().GetValue();
            }
            auto ksk = askin / (askin + astr);
            double t = 1.1 * std::cbrt(pw / (kp * e1) * std::pow(bstr / ksk, 2));
            if (t > sec.lwrSkin.element.material.GetThickness().GetValue())
            {
                stabOk = false;
                break;
            }
        }
        else
        {
            // Based on D matrix | Combined direct & shear
            // Panel length given by stringer pitch 
            //auto skinlen = sec.uprSkin.GetLength();
            Length b = (sec.lwrStringers[1].element.centroid - sec.lwrStringers[0].element.centroid).norm();
            // Given by rib pitch 
            Length a = minRibPitch; // Const
            auto AR = (a / b).GetValue();
            // Per unit length (section-wise)
            auto nx = ((sec.lwrSkin.element.dirStress[StructuralElement::Neg1] * sec.lwrSkin.element.material.GetThickness()).GetValue());
            if (nx > 0)
            {
                // Tension, don't care
                continue;
            }
            nx = abs(nx);
            auto nxy = sec.lwrSkin.element.maxShearFlow;

            // Crit loads 
            auto d = sec.lwrSkin.element.material.GetMatrixD();
            auto d11 = d(0, 0);
            auto d12 = d(0, 1);
            auto d22 = d(1, 1);
            auto d66 = d(2, 2);

            // Please be small
            auto d16 = d(0, 2);
            auto d26 = d(1, 2);

            auto pi2 = pow(M_PI, 2.0);
            auto ar2 = pow(AR, 2.);
            auto ar4 = pow(AR, 4.);
            auto a2 = pow(a.GetValue(), 2.);

            // Minimize for number of half-waves m 

            // Direct uniaxial
            int halfWavesBWiseDir = 1;
            double nxcr = std::numeric_limits<double>::max();
            for (int m = 1; m < 20; ++m)
            {
                auto m2 = pow(m, 2.);
                auto m4 = pow(m, 4.);
                auto ncr = pi2 * (d11 * m4 + 2 * (d12 + 2 * d66) * m2 * ar2 + d22 * ar4) / (a2 * m2);
                if (ncr < nxcr)
                {
                    nxcr = ncr;
                    halfWavesBWiseDir = m;
                }
            }

            // Shear
            auto beta = pow((d11 / d22), 1. / 4.);
            auto frac = (d12 + 2 * d66) / sqrt(d11 * d22);
            auto A = -0.27 + 0.185 * frac;
            auto B = 0.82 + 0.46 * frac - 0.2 * pow(frac, 2.);
            auto K = 8.2 + 5 * frac * 1 / (pow(10, A / beta + B * beta));
            auto nxycr = 4 / pow(b.GetValue(), 2.) * pow((d11 * pow(d22, 3.)), 1. / 4.) * K;

            auto rc = nx / nxcr;
            auto rs = nxy / nxycr;
            auto fi = rc + pow(rs, 2.);
            auto rf = 1 / sqrt(fi);
            if (rf < 1.)
            {
                stabOk = false;
                break;
            }
        }
    }

    if (!stabOk && killIfFail)
        return{};

    //return { totalMass };
    return masses;
}

// Just FUCK Matlab 
#include "openga.hpp"

struct MiddleCost
{
    // TODO: split up elements over multiple sims 
    std::array<Mass, 5> masses;
    Mass totalMass;
};

using GAType = EA::Genetic<Chromosome, MiddleCost>;
using GenType = EA::GenerationType<Chromosome, MiddleCost>;

void InitGenes(Chromosome& guy, const std::function<double(void)>& rnd)
{
    guy.Generate();
}

StructuralMesh* g_structMesh;
SectionalEnvelope g_airfoilEnv;
WingEnvelope g_boxEnvelope;

bool EvalGenes(const Chromosome& guy, MiddleCost& cost)
{
    auto structMesh = StructuralMesh(g_boxEnvelope);
    auto trial = guy;
    auto wingmassGuess = 3000_kg;
    auto trialResult = TestTrial(structMesh, trial, g_airfoilEnv, wingmassGuess, true, false);
    if (!trialResult)
        return false;

    Mass newGuess = 0;
    for (auto& m : *trialResult)
        newGuess += m;

    cost.masses = *trialResult;
    cost.totalMass = newGuess;
    return true;
}

Chromosome Mutate(const Chromosome& base, const std::function<double(void)>& rnd, double shrinkScale)
{
    Chromosome mutant;
    mutant.MutateFrom(base, shrinkScale);
    return mutant;
}

Chromosome Crossover(const Chromosome& dad, const Chromosome& mum, const std::function<double(void)>& rnd)
{
    Chromosome kid;
    kid.Crossover(dad, mum);    
    return kid;
}

double GetTotalFitness(const GAType::thisChromosomeType& guy)
{
    return guy.middle_costs.totalMass.GetValue();
}

void Report(int genNumber, const EA::GenerationType<Chromosome, MiddleCost>& lastGeneration, const Chromosome& best)
{
    auto bestTotalMass = lastGeneration.best_total_cost;
    //auto best = lastGeneration.chromosomes[lastGeneration.best_chromosome_index];
    //enum MassIdx { STRUT, SKIN, STRINGER, SPAR, RIBS };
    auto trial = best;
    auto retest = TestTrial(*g_structMesh, trial, g_airfoilEnv, 3000_kg, true, true);
    // wtf is going on with masses
    auto cur_uprSkinThick = trial.rootSection.uprSkinLam.GetThickness();
    auto cur_lwrSkinThick = trial.rootSection.lwrSkinLam.GetThickness();
    Chromosome fdr;
    auto fdrTest = TestTrial(*g_structMesh, fdr, g_airfoilEnv, 3000_kg, true, true);
    auto fdr_uprSkinThick = fdr.rootSection.uprSkinLam.GetThickness();
    auto fdr_lwrSkinThick = fdr.rootSection.lwrSkinLam.GetThickness();
    __debugbreak();
}

std::pair<WingEnvelope, StructuralMesh> BuildStructMesh(size_t numCuts, const Wingbox& other)
{
    // TODO: remove duplicate code (wingbox and all the other trash)
    
    for (auto& sec : other.envelope.sections)
    {
        SectionEnvelope newsec;
        newsec.verts = sec.verts;
        g_boxEnvelope.sections.push_back(newsec);
    }
    g_structMesh = new StructuralMesh(g_boxEnvelope);
    g_airfoilEnv = other.airfoilEnvelope;

    // Test FDR
    Chromosome single;
    auto singleResult = TestTrial(*g_structMesh, single, g_airfoilEnv, 3000_kg, true, true, true);

    GAType obj;
    obj.problem_mode = EA::GA_MODE::SOGA;
    obj.multi_threading = false;
    obj.idle_delay_us = 1; 
    obj.verbose = false;
    obj.population = 50; // 20
    obj.generation_max = 50;
    obj.calculate_SO_total_fitness = GetTotalFitness;
    obj.init_genes = InitGenes;
    obj.eval_solution = EvalGenes;
    obj.mutate = Mutate;
    obj.crossover = Crossover;
    obj.SO_report_generation = Report;
    obj.best_stall_max = 10;
    obj.elite_count = 10;
    obj.crossover_fraction = 0.7;
    obj.mutation_rate = 0.4;
    obj.solve();

    Mass bestMass(100_t);
    size_t pop = 1000;
    Chromosome elite;
    std::array<Mass, 5> eliteMasses;
    std::vector<Chromosome> trials;
    for (size_t indi = 0; indi < pop; ++indi)
    {
        Chromosome trial;
        trial.Generate();
        trials.push_back(trial);

        // Iterate interia relief
        Mass wingmassGuess = 3000_kg;
        //Mass dm = 100_t;
        //while (dm > 100_kg)
        //{
        //    auto trialResult = TestTrial(structMesh, trial, other.airfoilEnvelope, wingmassGuess);
        //    if (!trialResult)
        //        break;
        //    
        //    Mass newGuess = 0;
        //    for (auto& m : *trialResult)
        //        newGuess += m;
        //    //http://www.engbrasil.eng.br/artigos/art2.pdf
        //    newGuess = newGuess * 1.7372;

        //    dm = abs(newGuess.GetValue() - wingmassGuess.GetValue());
        //    wingmassGuess = newGuess;
        //}

        auto trialResult = TestTrial(*g_structMesh, trial, g_airfoilEnv, wingmassGuess,
            false, false);
        if (!trialResult)
            continue;

        Mass newGuess = 0;
        for (auto& m : *trialResult)
            newGuess += m;
        newGuess = newGuess - (*trialResult)[MassIdx::STRUT];
        newGuess = newGuess * 1.7372; // Systems, high lift devices, pipes, whatever, just for the main wing 
        newGuess = newGuess + (*trialResult)[MassIdx::STRUT] * 1.23; // Ribs, stringers, fasteners, etc. just for the strut

        //http://www.engbrasil.eng.br/artigos/art2.pdf
        //newGuess = newGuess * 1.7372;

        //dm = abs(newGuess.GetValue() - wingmassGuess.GetValue());
        //wingmassGuess = newGuess;

        if (newGuess < bestMass)
        {
            bestMass = newGuess;
            eliteMasses = *trialResult;
            elite = trial;
        }
    }

    return { g_boxEnvelope, *g_structMesh };
}

void Solver::Run()
{
    // TODO: cache 
    AirfoilManager airfoilMgr;
    auto& rootFoil = airfoilMgr.GetAirfoil(
        airfoilMgr.LoadAirfoilByLocalPath(m_configState->rootAirfoilPath));
    auto& tipFoil = airfoilMgr.GetAirfoil(
        airfoilMgr.LoadAirfoilByLocalPath(m_configState->tipAirfoilPath));

    auto wing = Wing()
        .SetFoils(rootFoil, tipFoil)
        .SetChords(m_configState->rootChord /* root */)
        .SetTaperRatio(m_configState->taperRatio)
        .SetSemispan(m_configState->wingspan / 2.0)
        .SetSparOffsets(m_configState->frontSparOffset, m_configState->rearSparOffset)
        .SetTipDisplacement(0.5_m);     // GA
        //.SetTipDisplacement(0_m);     // TODO: temp test

    const size_t numCuts = g_numCuts;
    Wingbox box;
    box.envelope = wing.GetWingboxEnvelope(numCuts);
    box.airfoilEnvelope = wing.GetAirfoilEnvelope();

    ///////////////////////////////////////////////////////////////////////////////
    // Current entry point
    // Everything below this is trash that needs cleaning up 
    ///////////////////////////////////////////////////////////////////////////////
    auto [wingEnvelope, mesh] = BuildStructMesh(numCuts, box);

    // HS-CF, EP matrix, 60% Vf
    box.plyProperties = Ply()
        .SetRho(1.6_gPerCm3)
        .SetE1(140_GPa)
        .SetE2(10_GPa)
        .SetG12(5_GPa)
        .SetNu(0.3)
        .SetS12(70_MPa)
        // For T-H/max stress failure (ignore)
        .SetS1t(1500_MPa)
        .SetS1c(-1200_MPa)
        .SetS2t(50_MPa)
        .SetS2c(-250_MPa);

    box.upperSkin.layup = QILayup(50., 40., 10.);
    box.upperSkin.thickness = 15_mm;

    box.lowerSkin.layup = QILayup(60., 30., 10.);
    box.lowerSkin.thickness = 15_mm;

    box.frontSparWeb.layup = QILayup(20., 60., 20.);
    box.frontSparWeb.thickness = 8_mm;
    box.frontSparUprFlange.layup = QILayup(20., 60., 20.);
    box.frontSparUprFlange.area = 720_mm2;
    box.frontSparLwrFlange.layup = QILayup(20., 60., 20.);
    box.frontSparLwrFlange.area = 720_mm2;

    box.rearSparWeb.layup = QILayup(20., 60., 20.);
    box.rearSparWeb.thickness = 8_mm;
    box.rearSparUprFlange.layup = QILayup(20., 60., 20.);
    box.rearSparUprFlange.area = 720_mm2;
    box.rearSparLwrFlange.layup = QILayup(20., 60., 20.);
    box.rearSparLwrFlange.area = 720_mm2;

    box.uprStringer.layup = QILayup(60., 30., 10.);
    box.uprStringer.area = 1200_mm2;
    box.lwrStringer.layup = QILayup(60., 30., 10.);
    box.lwrStringer.area = 1200_mm2;

    // TODO: create stringer cutting planes (and actually cut the upr/lwr surfaces)
    //auto semispan = boxEnvelope.GetDistanceBetweenSections(0, boxEnvelope.sections.size() - 1);
    const auto rootUprSurfaceLength = box.envelope.sections.front().GetUprCurveLength();
    const size_t numRootUprStringers = 9;
    const auto equalUprStringerSpacing = rootUprSurfaceLength / (numRootUprStringers + 1);
    for (size_t i = 0; i < numRootUprStringers; ++i)
    {
        box.rootUprStringerSpacings.push_back(equalUprStringerSpacing);
        // TODO: add corresponding vertex on BOTH lwr and upr surfaces (for ALL sections)
    }

    for (size_t i = 0; i < box.envelope.sections.size(); ++i)
    {
        // For now, a constant number of stringers along the whole span 
        box.numUprStringersEndingHere.push_back(0);
    }

    const auto rootLwrSurfaceLength = box.envelope.sections.front().GetLwrCurveLength();
    const size_t numRootLwrStringers = 5;
    const auto equalLwrStringerSpacing = rootLwrSurfaceLength / (numRootLwrStringers + 1);
    for (size_t i = 0; i < numRootLwrStringers; ++i)
    {
        box.rootLwrStringerSpacings.push_back(equalLwrStringerSpacing);
        // TODO: add corresponding vertex on BOTH lwr and upr surfaces (for ALL sections)
    }

    for (size_t i = 0; i < box.envelope.sections.size(); ++i)
    {
        // For now, a constant number of stringers along the whole span 
        box.numLwrStringersEndingHere.push_back(0);
    }

    std::ofstream fconv("conv");
    Strut strut;
    strut.distanceFromRoot = m_configState->junctionToCentreline / 100.0 * wing.GetSemispan();
    strut.chord = m_configState->strutChord;
    strut.length = m_configState->strutLength;
    strut.junctionAngle = m_configState->junctionAngle;
    strut.thickness = m_configState->strutTC / 100.0 * strut.chord;
    strut.wallThickness = m_configState->strutWallThickness;

    bool converged = false;
    Percentage convergenceTol = 5_percent;
    size_t maxIterations = 1000;
    size_t iteration = 0;
    size_t secBeforeStrutIndex = 0;
    for (auto& sec : box.envelope.sections)
        if (sec.GetSpanwisePos() > strut.distanceFromRoot)
        {
            secBeforeStrutIndex = sec.sectionIndex - 1;
            break;
        }
    Point3d strutPos = { Length(strut.distanceFromRoot).GetValue(), 0., 0. };
    Force strutLoadMin = -1'000'000_N;
    Force strutLoadMax = 0_N;
    Force strutLoad = 0.5 * (strutLoadMin + strutLoadMax);
    double n = 2.5;
    // Strut extension too small => double dLoad & add
    //                 too big => halve dLoad & substract
    //Force dLoad = 1'000_N;
    while (!converged && ++iteration < maxIterations)
    {
        auto defl = Deflect(wing, box, n, &strut, &strutLoad);

        auto meanDeflAtJunction = 0.5 * (defl[secBeforeStrutIndex] + defl[secBeforeStrutIndex + 1]);
        auto deflStrut = strut.GetVerticalExtension(-strutLoad * n); // Tension reaction 
        auto diff = PercentDifference(abs(deflStrut), abs(meanDeflAtJunction));
        fconv << strutLoad.GetValue() * n
            << " " << meanDeflAtJunction.GetValue()
            << " " << deflStrut.GetValue()
            << " " << diff.GetValue() << std::endl;

        //if (diff < convergenceTol)
        if (abs(deflStrut - meanDeflAtJunction) < m_configState->strutConvTol)
        {
            m_outputSet->deflectionStrut = defl;
            converged = true;
            break;
        }
        else
        {
            if (deflStrut > meanDeflAtJunction)
                strutLoadMin = strutLoad;
            else
                strutLoadMax = strutLoad;
            strutLoad = 0.5 * (strutLoadMin + strutLoadMax);
        }
    }

    if (converged)
    {
        m_outputSet->converged = true;
        m_outputSet->convergedFlag->setVisible(true);
    }
    else
    {
        m_outputSet->convergedFlag->setVisible(false);
    }

    for (auto& series : m_outputSet->rfDirStress)
        series.series->clear();

    double minRF = std::numeric_limits<double>::max();
    double maxRF = std::numeric_limits<double>::lowest();
    const double ULT = 1.5;
    for (size_t i = 0; i < box.envelope.sections.size() - 1; ++i)
    {
        auto& sec = box.envelope.sections[i];

        sec.dirStressUprSkin = (ULT * box.BM[i] * sec.topSkinToNA) / sec.IzBar
            * (sec.effExUprSkin / sec.ExBar);

        sec.dirStressLwrSkin = (ULT * box.BM[i] * sec.botSkinToNA) / sec.IzBar
            * (sec.effExLwrSkin / sec.ExBar);

        sec.dirStressUprStringer = (ULT * box.BM[i] * sec.topStringerToNA) / sec.IzBar
            * (sec.effExUprStringer / sec.ExBar);

        sec.dirStressLwrStringer = (ULT * box.BM[i] * sec.botStringerToNA) / sec.IzBar
            * (sec.effExLwrStringer / sec.ExBar);

        sec.dirStressFrontSparUprFlange = (ULT * box.BM[i] * sec.frontSparUprFlangeToNA) / sec.IzBar
            * (sec.effExFrontSparUprFlange / sec.ExBar);

        sec.dirStressFrontSparLwrFlange = (ULT * box.BM[i] * sec.frontSparLwrFlangeToNA) / sec.IzBar
            * (sec.effExFrontSparLwrFlange / sec.ExBar);

        sec.dirStressRearSparUprFlange = (ULT * box.BM[i] * sec.rearSparUprFlangeToNA) / sec.IzBar
            * (sec.effExRearSparUprFlange / sec.ExBar);

        sec.dirStressRearSparLwrFlange = (ULT * box.BM[i] * sec.rearSparLwrFlangeToNA) / sec.IzBar
            * (sec.effExRearSparLwrFlange / sec.ExBar);

        sec.allowDirStressUprSkin = 0.004 * sec.effExUprSkin;
        sec.allowDirStressLwrSkin = 0.004 * sec.effExLwrSkin;
        sec.allowDirStressUprStringer = 0.004 * sec.effExUprStringer;
        sec.allowDirStressLwrStringer = 0.004 * sec.effExLwrStringer;
        sec.allowDirStressFrontSparUprFlange = 0.004 * sec.effExFrontSparUprFlange;
        sec.allowDirStressFrontSparLwrFlange = 0.004 * sec.effExFrontSparLwrFlange;
        sec.allowDirStressRearSparUprFlange = 0.004 * sec.effExRearSparUprFlange;
        sec.allowDirStressRearSparLwrFlange = 0.004 * sec.effExRearSparLwrFlange;

        sec.rfDirStressUprSkin = abs((sec.allowDirStressUprSkin / sec.dirStressUprSkin).GetValue());
        sec.rfDirStressLwrSkin = abs((sec.allowDirStressLwrSkin / sec.dirStressLwrSkin).GetValue());
        sec.rfDirStressUprStringer = abs((sec.allowDirStressUprStringer / sec.dirStressUprStringer).GetValue());
        sec.rfDirStressLwrStringer = abs((sec.allowDirStressLwrStringer / sec.dirStressLwrStringer).GetValue());
        sec.rfDirStressFrontSparUprFlange = abs((sec.allowDirStressFrontSparUprFlange / sec.dirStressFrontSparUprFlange).GetValue());
        sec.rfDirStressFrontSparLwrFlange = abs((sec.allowDirStressFrontSparLwrFlange / sec.dirStressFrontSparLwrFlange).GetValue());
        sec.rfDirStressRearSparUprFlange = abs((sec.allowDirStressRearSparUprFlange / sec.dirStressRearSparUprFlange).GetValue());
        sec.rfDirStressRearSparLwrFlange = abs((sec.allowDirStressRearSparLwrFlange / sec.dirStressRearSparLwrFlange).GetValue());

        const auto x = sec.GetSpanwisePos().GetValue();
        m_outputSet->rfDirStress[0].series->append(x, sec.rfDirStressUprSkin);
        m_outputSet->rfDirStress[1].series->append(x, sec.rfDirStressLwrSkin);
        m_outputSet->rfDirStress[2].series->append(x, sec.rfDirStressUprStringer);
        m_outputSet->rfDirStress[3].series->append(x, sec.rfDirStressLwrStringer);
        m_outputSet->rfDirStress[4].series->append(x, sec.rfDirStressFrontSparUprFlange);
        m_outputSet->rfDirStress[5].series->append(x, sec.rfDirStressFrontSparLwrFlange);
        m_outputSet->rfDirStress[6].series->append(x, sec.rfDirStressRearSparUprFlange);
        m_outputSet->rfDirStress[7].series->append(x, sec.rfDirStressRearSparLwrFlange);

        for (auto& rf : m_outputSet->rfDirStress)
        {
            auto pt = rf.series->points().at(rf.series->points().count() - 1).y();
            maxRF = std::max(pt, maxRF);
            minRF = std::min(pt, minRF);
        }
    }

    const auto alDensity = 2.78_gPerCm3;
    const Volume strutMaterialVolume = strut.GetCrossSection() * strut.length;
    const Mass strutMass = alDensity * strutMaterialVolume;

    m_outputSet->rfDirStress[0].series->chart()->axisX()->setRange(0, m_configState->wingspan.GetValue() / 2.0);
    m_outputSet->rfDirStress[0].series->chart()->axisY()->setRange(minRF, maxRF);

    m_outputSet->zeroRF->clear();
    m_outputSet->zeroRF->append(0.0, 1.0);
    m_outputSet->zeroRF->append(wing.GetSemispan().GetValue(), 1.0);

    m_outputSet->deflectionClean = Deflect(wing, box, n, nullptr);

    m_outputSet->seriesDeflectionStrut->clear();
    for (size_t i = 0; i < box.envelope.sections.size(); ++i)
    {
        m_outputSet->seriesDeflectionStrut->append(box.envelope.sections[i].GetSpanwisePos().GetValue(),
            m_outputSet->deflectionStrut[i].GetValue());
    }

    m_outputSet->seriesDeflectionClean->clear();
    for (size_t i = 0; i < box.envelope.sections.size(); ++i)
    {
        m_outputSet->seriesDeflectionClean->append(box.envelope.sections[i].GetSpanwisePos().GetValue(),
            m_outputSet->deflectionClean[i].GetValue());
    }

    double maxTipDefl = m_configState->allowableDeflection.GetValue() / 100.0
        * wing.GetSemispan().GetValue();
    auto ylim = std::max(maxTipDefl * 1.5, m_outputSet->deflectionClean.back().GetValue() * 1.2);

    m_outputSet->seriesDeflectionJunctionPos->clear();
    m_outputSet->seriesDeflectionJunctionPos->append(strut.distanceFromRoot.GetValue(), -1.);
    m_outputSet->seriesDeflectionJunctionPos->append(strut.distanceFromRoot.GetValue() + 0.000001, ylim);

    m_outputSet->seriesDeflectionLimit->clear();
    m_outputSet->seriesDeflectionLimit->append(0, maxTipDefl);
    m_outputSet->seriesDeflectionLimit->append(wing.GetSemispan().GetValue(), maxTipDefl);

    m_outputSet->seriesDeflectionZero->clear();
    m_outputSet->seriesDeflectionZero->append(0, 0);
    m_outputSet->seriesDeflectionZero->append(wing.GetSemispan().GetValue(), 0);

    m_outputSet->seriesDeflectionClean->chart()->axisX()->setRange(0., wing.GetSemispan().GetValue());
    m_outputSet->seriesDeflectionClean->chart()->axisY()->setRange(-1., ylim);

    // Output SF 
    m_outputSet->seriesSF->clear();
    double minSF = std::numeric_limits<double>::max();
    double maxSF = std::numeric_limits<double>::lowest();
    for (size_t i = 0; i < box.envelope.sections.size(); ++i)
    {
        m_outputSet->seriesSF->append(box.envelope.sections[i].GetSpanwisePos().GetValue(),
            box.SF[i].GetValue());
        minSF = std::min(box.SF[i].GetValue(), minSF);
        maxSF = std::max(box.SF[i].GetValue(), maxSF);
    }
    m_outputSet->seriesSF->chart()->axisX()->setRange(0., wing.GetSemispan().GetValue());
    m_outputSet->seriesSF->chart()->axisY()->setRange(minSF, maxSF);
}
