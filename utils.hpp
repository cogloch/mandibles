#pragma once
#include <Eigen/Dense>

bool EqualDouble(double a, double b);
bool LessOrEqualDouble(double a, double b);
bool HigherOrEqualDouble(double a, double b);
Eigen::Vector2d Lerp2(const Eigen::Vector2d& from, const Eigen::Vector2d& to, double scale);