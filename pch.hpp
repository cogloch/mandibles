#pragma once
#include <QtWidgets>

#include <array>
#include <limits>
#include <string>
#include <vector>
#include <memory>
#include <ostream>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "units.hpp"
