#include "pch.hpp"
#include "qi_laminate.hpp"


const Length QiLaminate::s_plyThickness = 0.125_mm;
const Length QiLaminate::s_maxThickness = 10_mm;
std::vector<PlyFractions> QiLaminate::s_configs;

const Eigen::Vector3d QiLaminate::s_exContrib = {
        1.0, 0.1, 0.1
};

const Eigen::Vector3d QiLaminate::s_eyContrib = {
        0.1, 0.1, 1.0
};

const Eigen::Vector3d QiLaminate::s_gxyContrib = {
        0.1, 0.55, 0.1
};

const Eigen::Vector3d QiLaminate::s_allTauContrib = {
        0.1, 0.55, 0.1
};

int PlyFractions::GetBasePlyCount() const
{
    return num0 + num45 + num90;
}

bool operator ==(const PlyFractions& lhs, const PlyFractions& rhs)
{
    return (EqualDouble(lhs.p0, rhs.p0) && EqualDouble(lhs.p45, rhs.p45) && EqualDouble(lhs.p90, rhs.p90));
}

std::vector<PlyFractions> QiLaminate::GetSortedShear()
{
    std::vector<PlyFractions> sorted = s_configs;
    std::sort(sorted.begin(), sorted.end(), [](const PlyFractions& a, const PlyFractions& b) {
        return a.p45 > b.p45;
        });
    return sorted;
}

std::vector<PlyFractions> QiLaminate::GetSortedTrans()
{
    std::vector<PlyFractions> sorted = s_configs;
    std::sort(sorted.begin(), sorted.end(), [](const PlyFractions& a, const PlyFractions& b) {
        return a.p90 > b.p90;
        });
    return sorted;
}

std::vector<PlyFractions> QiLaminate::GetSortedAlign()
{
    std::vector<PlyFractions> sorted = s_configs;
    std::sort(sorted.begin(), sorted.end(), [](const PlyFractions& a, const PlyFractions& b) {
        return a.p0 > b.p0;
        });
    return sorted;
}

void QiLaminate::GenerateLayupSpace()
{
    int maxPlies = (s_maxThickness / s_plyThickness).GetValue();
    int maxPliesHalf = maxPlies / 2;
    for (int numTotal = maxPliesHalf / 2; numTotal >= 4; --numTotal)
    {
        int min10 = std::ceil(double(numTotal) / 10.0);
        int num45 = numTotal - (min10 * 2);
        num45 = num45 - (num45 % 2); // Floor to even 
        for (; num45 >= min10; num45 -= 2)
        {
            int rest = numTotal - num45;
            int max10 = rest - min10;
            for (int num0 = min10; num0 <= max10; ++num0)
            {
                int num90 = rest - num0;
                double pc0 = double(num0) / double(numTotal) * 100.;
                double pc45 = double(num45) / double(numTotal) * 100.;
                double pc90 = double(num90) / double(numTotal) * 100.;

                PlyFractions newlay(pc0, pc45, pc90, num0, num45, num90);
                bool exists = false;
                for (const auto& lay : s_configs)
                    if (lay == newlay)
                    {
                        exists = true;
                        break;
                    }

                if (!exists)
                {
                    if (pc0 < 10. || pc45 < 10. || pc90 < 10.)
                        throw "10% Rule!";
                    s_configs.push_back(newlay);
                }
            }
        }
    }
}

int QiLaminate::GetMaxPlyMul(PlyFractions& frac)
{
    return std::floor(s_maxThickness.GetValue() / s_plyThickness.GetValue() / (frac.GetBasePlyCount() * 2));
}

Length QiLaminate::GetBaseLamThickness() const
{
    // x2 for opposite side around NA!
    return baseLam.GetBasePlyCount() * s_plyThickness * 2.;
}

Length QiLaminate::GetThickness() const
{
    return GetBaseLamThickness() * numBaseLams;
}