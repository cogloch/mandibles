#include "pch.hpp"
#include "structural_mesh.hpp"
#include "loading_mesh.hpp"


Length WingEnvelope::GetDistanceBetweenSections(size_t sec0Index, size_t sec1Index) const
{
    // Sections are streamwise (not chordwise), hence any point is valid on either section
    return Length(abs(sections[sec0Index].verts.front().x() - 
                      sections[sec1Index].verts.front().x()
    ));
}

Volume WingEnvelope::GetVolumeBetweenSections(size_t sec0Index, size_t sec1Index) const
{
    if (sec0Index == sec1Index)
        return 0_m3;

    if (sec0Index > sec1Index)
        std::swap(sec0Index, sec1Index);

    Volume result = 0_m3;
    for (size_t i = sec0Index + 1; i < sec1Index; ++i)
    {
        const auto dx = GetDistanceBetweenSections(i - 1, i);
        const auto prevArea = sections[i - 1].GetArea();
        const auto curArea = sections[i].GetArea();
        const auto meanArea = 0.5 * (prevArea + curArea);
        result += dx * meanArea;
    }
    return result;
}

size_t WingEnvelope::GetNumSections() const
{
    return sections.size();
}

Length SectionEnvelope::GetCurveLength(size_t vert0Index, size_t vert1Index) const
{
    // Order vert0Index < vert1Index
    if (vert0Index > vert1Index)
        std::swap(vert0Index, vert1Index);

    Length result(0_m);
    for (; vert0Index < vert1Index; ++vert0Index)
        result += (verts[vert0Index + 1] - verts[vert0Index]).norm();

    return result;
}

Area SectionEnvelope::GetArea() const
{
    // Gauss area
    Area area = 0_m2;

    for (size_t i = 0; i < verts.size() - 1; ++i)
    {
        const auto xcur = verts[i].z();
        const auto ynext = verts[i + 1].y();
        const auto darea = xcur * ynext;
        area += darea;
    }

    area += verts[verts.size() - 1].z() * verts[0].y();

    for (size_t i = 0; i < verts.size() - 1; ++i)
        area -= verts[i + 1].z() * verts[i].y();

    area -= verts[0].z() * verts[verts.size() - 1].y();

    return 0.5 * abs(area);
}

Length SectionEnvelope::GetUprSkinLength() const
{
    return GetCurveLength(0, verts.size() / 2);
}

Length SectionEnvelope::GetLwrSkinLength() const
{
    return GetCurveLength(verts.size() - 1, verts.size() / 2 + 1);
}

StructuralMesh::StructuralMesh(WingEnvelope boxEnvelope_)
    : taper(boxEnvelope_.sections.size())
    , boxEnvelope(boxEnvelope_)
{
}

std::vector<double> StructuralMesh::GetDirStressRF(std::vector<EquivSectionalLoad>& loading, Case loadCase)
{
    const double ULT = 1.5;
    std::vector<double> rf;
    for (size_t i = 0; i < sections.size(); ++i)
    {
        auto& sec = sections[i];
        auto dummy = sec.GetIzBar();
     
        auto bm = loading[i].spanMoment;
        auto NA = sec.GetNA();

        auto dirStressUprSkin = (ULT * bm * -(Length)NA.distance(sec.uprSkin.verts.front())) / sec.izbar
            * (sec.uprSkin.element.material.GetEffEx() / sec.exbar);
        sec.uprSkin.element.dirStress[loadCase] = dirStressUprSkin;
        //rf.push_back(abs((0.004 * sec.uprSkin.element.material.GetEffEx() / dirStressUprSkin)).GetValue());

        auto dirStressLwrSkin = (ULT * bm * (Length)NA.distance(sec.lwrSkin.verts.front())) / sec.izbar
            * (sec.lwrSkin.element.material.GetEffEx() / sec.exbar);
        sec.lwrSkin.element.dirStress[loadCase] = dirStressLwrSkin;
        //rf.push_back(abs((0.004 * sec.lwrSkin.element.material.GetEffEx() / dirStressLwrSkin)).GetValue());

        for (auto& stringer : sec.uprStringers)
        {
            auto str = (ULT * bm * -(Length)NA.distance(stringer.element.centroid)) / sec.izbar
                * (stringer.element.material.GetEffEx() / sec.exbar);
            stringer.element.dirStress[loadCase] = str;
            rf.push_back(abs((0.004 * stringer.element.material.GetEffEx() / str)).GetValue());
        }

        for (auto& stringer : sec.lwrStringers)
        {
            auto str = (ULT * bm * (Length)NA.distance(stringer.element.centroid)) / sec.izbar
                * (stringer.element.material.GetEffEx() / sec.exbar);
            stringer.element.dirStress[loadCase] = str;
            rf.push_back(abs((0.004 * stringer.element.material.GetEffEx() / str)).GetValue());
        }

        for (auto& spar : sec.spars)
        {
            auto str = (ULT * bm * -(Length)NA.distance(spar.uprFlange.element.centroid)) / sec.izbar
                * (spar.uprFlange.element.material.GetEffEx() / sec.exbar);
            rf.push_back(abs((0.004 * spar.uprFlange.element.material.GetEffEx() / str)).GetValue());
            spar.uprFlange.element.dirStress[loadCase] = str;

            str = (ULT * bm * (Length)NA.distance(spar.lwrFlange.element.centroid)) / sec.izbar
                * (spar.lwrFlange.element.material.GetEffEx() / sec.exbar);
            rf.push_back(abs((0.004 * spar.lwrFlange.element.material.GetEffEx() / str)).GetValue());
            spar.lwrFlange.element.dirStress[loadCase] = str;
        }
    }

    return rf;
}

std::vector<std::vector<double>> StructuralMesh::GetShearStresses(std::vector<EquivSectionalLoad>& loads, Case loadCase)
{
    std::vector<std::vector<double>> tau;

    for (size_t i = 0; i < sections.size();++i)
    {
        auto& sec = sections[i];
        auto T = loads[i].streamMoment;
        auto Sy = loads[i].shear;
        auto Ibar = sec.GetIzBar();
        auto Ebar = sec.GetExBar();
        auto A = sec.GetInteriorArea();
        auto ref = sec.GetCentroid().y();

        // Const due to torsion
        auto qt = (T / (2 * A)).GetValue();

        // Cut at skin midpoint (assume symmetry although it's not true)
        double qs = 0;
        auto ts = sec.uprSkin.element.material.GetThickness();
        for (int skinIdx = sec.uprSkin.verts.size() / 2; skinIdx > 1; --skinIdx)
        {
            auto& cur = sec.uprSkin.verts[skinIdx];
            auto& next = sec.uprSkin.verts[skinIdx - 1];
            auto ds = (next - cur).norm();
            auto mid = cur + (next - cur) * 0.5;
            auto y = mid.y() - ref;
            auto Exs = sec.uprSkin.element.material.GetEffEx();

            qs += ((Sy / Ibar) * (ts * y * ds) * (Exs/Ebar)).GetValue();
        }

        auto qskin = qs + qt;
        sec.uprSkin.element.maxShearFlow = qskin;
        sec.lwrSkin.element.maxShearFlow = qskin; // Assume symmetrical-ish

        auto hh = sec.spars[0].GetWebHeight() / 2;
        auto tw = sec.spars[0].web.element.material.GetThickness();
        auto Exw = sec.spars[0].web.element.material.GetEffEx();
        auto qsw = qs + ((Sy / Ibar) * (Exw / Ebar) * (tw * hh * hh - tw * hh * hh / 2)).GetValue();

        auto qw = qsw + qt;
        sec.spars[0].web.element.maxShearFlow = qw;
        sec.spars[1].web.element.maxShearFlow = qw; // Assume symmetrical-ish
        
        std::vector<double> stresses = { qskin / ts.GetValue(), qw / tw.GetValue() };
        tau.push_back(stresses);
    }

    return tau;
}

std::vector<Length> StructuralMesh::Deflect(std::vector<EquivSectionalLoad>& loading)
{
    const auto numSec = sections.size();
    std::vector<double> deflectionAngle(numSec);
    std::vector<Length> deflection(numSec);

    // BCs
    deflectionAngle[0] = 0.;
    deflection[0] = 0_m;

    for (size_t i = 1; i < sections.size(); ++i)
    {
        auto& cur = sections[i];
        auto& prev = sections[i - 1];
        Length dx = abs(cur.GetCentroid().x() - prev.GetCentroid().x());

        // Integrate M/EI(x)dx
        const auto prevFrac = loading[i - 1].spanMoment / prev.GetExIzBar();
        const auto curFrac = loading[i].spanMoment / cur.GetExIzBar();
        deflectionAngle[i] = deflectionAngle[i - 1] + (0.5 * (prevFrac + curFrac) * dx).GetValue();

        // Integrate deflAngle(x)dx
        deflection[i] = deflection[i - 1] + 0.5 * (deflectionAngle[i - 1] +  deflectionAngle[i]) * dx;
    }

    return deflection;
}

std::vector<double> StructuralMesh::Twist(std::vector<EquivSectionalLoad>& loading)
{
    const auto numSec = sections.size();
    std::vector<double> twistRate(numSec);
    std::vector<double> twist(numSec);

    // BCs
    twistRate[0] = 0.;
    twist[0] = 0;

    for (size_t i = 1; i < sections.size(); ++i)
    {
        auto& cur = sections[i];
        auto& prev = sections[i - 1];
        Length dx = abs(cur.GetCentroid().x() - prev.GetCentroid().x());

        // T/GJ(x)
        const auto prevFrac = loading[i - 1].streamMoment / prev.GetGJBar();
        const auto curFrac = loading[i].streamMoment / cur.GetGJBar();
        twistRate[i] = twistRate[i - 1] + (0.5 * (prevFrac + curFrac) * dx).GetValue();

        // Integrate rot(x)dx
        twist[i] = twist[i - 1] + 0.5 * (twistRate[i - 1] + twistRate[i]) * dx.GetValue();
    }

    return twist;
}
