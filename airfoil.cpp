#include "pch.hpp"
#include "airfoil.hpp"
#include "geometry.hpp"


Airfoil::Airfoil(const std::string& fromRaw)
{
    std::stringstream stream(fromRaw);
    std::string buffer(256, '\0');
    // First line is some description of the airfoil file 
    stream.getline(&buffer[0], buffer.size());

    double fltToken = 0.0;

    // Force Lednicer 
    stream >> fltToken;
    if (static_cast<int>(fltToken) < 2)
        throw "Lednicer format required";
    PiecewiseCurve2d topSurface(static_cast<size_t>(fltToken));

    stream >> fltToken;
    if (static_cast<int>(fltToken) < 2)
        throw "Lednicer format required";
    PiecewiseCurve2d botSurface(static_cast<size_t>(fltToken));

    for (auto& pt : topSurface)
        stream >> pt[0] >> pt[1];

    for (auto& pt : botSurface)
        stream >> pt[0] >> pt[1];

    // Lednicer should be sorted already 
    //topSurface.SortPoints();
    //botSurface.SortPoints();

    //topSurface.Refine();
    //botSurface.Refine();

    for (auto& topPoint : topSurface)
        botSurface.AddPointAtX(topPoint.x());

    for (auto& botPoint : botSurface)
        topSurface.AddPointAtX(botPoint.x());

    surfaces[TOP] = std::move(topSurface);
    surfaces[BOT] = std::move(botSurface);
}

Airfoil& AirfoilManager::GetAirfoil(size_t index)
{
    return m_airfoils[index];
}

// Return index of added airfoil
size_t AirfoilManager::LoadAirfoilByLocalPath(const std::string& path)
{
    QFile file(path.c_str());
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream stream(&file);
    std::string buffer = stream.readAll().toUtf8().constData();
    m_airfoils.emplace_back(buffer);
    return m_airfoils.size() - 1;
}