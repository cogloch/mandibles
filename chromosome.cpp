#include "pch.hpp"
#include "chromosome.hpp"
#include "stringer.hpp"
#include "structural_mesh.hpp"


const Length Chromosome::fuselageHeight = 3.8_m;//4_m;

std::vector<PlyFractions> lays;
std::vector<PlyFractions> sortedShearLays;
std::vector<PlyFractions> sortedAlignedLays;


const int Chromosome::minStringers = 10;
const int Chromosome::maxStringers = 25;
const int Chromosome::minStringerSizemm = 20;
const int Chromosome::maxStringerSizemm = 50;


Chromosome::Chromosome()
    : taper(g_numCuts)
{
    if (sortedAlignedLays.empty())
    {
        QiLaminate::GenerateLayupSpace();
        lays = QiLaminate::s_configs;
        sortedShearLays = QiLaminate::GetSortedShear();
        sortedAlignedLays = QiLaminate::GetSortedAlign();
    }

    //
    //  ROOT (to be replicated and tapered spanwise)
    //

    // HS-CF, EP matrix, 60% Vf        
    propPtr = std::make_shared<Ply>();
    *propPtr = Ply()
        .SetRho(1.6_gPerCm3)
        .SetE1(140_GPa)
        .SetE2(10_GPa)
        .SetG12(5_GPa)
        .SetNu(0.3)
        .SetS12(70_MPa)
        // For T-H/max stress failure (ignore)
        .SetS1t(1500_MPa)
        .SetS1c(-1200_MPa)
        .SetS2t(50_MPa)
        .SetS2c(-250_MPa);

    // Stringers 
    const size_t numRootUprStringers = 20;//5;
    StringerT rootUprStringer; // No streamwise taper
    rootUprStringer.width = 29_mm;
    rootUprStringer.height = 36_mm;
    rootUprStringer.element.material = QiLaminate(PlyFractions(60_percent, 30_percent, 10_percent), 4, propPtr); //1
    //QiLaminate(PlyFractions(60_percent, 30_percent, 10_percent), 1, propPtr);
    for (size_t stringerIdx = 0; stringerIdx < numRootUprStringers; ++stringerIdx)
        rootSection.uprStringers.push_back(rootUprStringer);

    const size_t numRootLwrStringers = 8;//14; //8;//4;
    StringerT rootLwrStringer; // No streamwise taper
    rootLwrStringer.width = 25_mm;
    rootLwrStringer.height = 31_mm;
    PlyFractions frac;
    frac.num0 = 4;
    frac.num45 = 2;
    frac.num90 = 1;
    rootLwrStringer.element.material = QiLaminate(frac, 3, propPtr); // 5
        //QiLaminate(PlyFractions(60_percent, 30_percent, 10_percent), 1, propPtr);
    for (size_t stringerIdx = 0; stringerIdx < numRootLwrStringers; ++stringerIdx)
        rootSection.lwrStringers.push_back(rootLwrStringer);

    // Skins
    frac.num0 = 10;//11;
    frac.num45 = 6;// 7;
    frac.num90 = 2;
    rootSection.uprSkinLam = QiLaminate(frac, 1, propPtr);    // 6->4
        //QiLaminate(PlyFractions(50_percent, 40_percent, 10_percent), 1, propPtr);
    frac.num0 = 10;// 11;
    frac.num45 = 6;// 7;
    frac.num90 = 2;
    rootSection.lwrSkinLam = QiLaminate(frac, 1, propPtr);    // 8->3
    //QiLaminate(PlyFractions(60_percent, 30_percent, 10_percent), 1, propPtr);

    // Spars

    frac.num0 = 1;
    frac.num45 = 8;
    frac.num90 = 1;
    rootSection.frontSparWebLam = QiLaminate(frac, 6, propPtr);
    rootSection.rearSparWebLam = QiLaminate(frac, 5, propPtr);    //6->5
    // 6, 5 -> 12, 10
    //QiLaminate(PlyFractions(20_percent, 60_percent, 20_percent), 1, propPtr);

    frac.num0 = 2;
    frac.num45 = 6;
    frac.num90 = 1;
    rootSection.frontSparUprLam = QiLaminate(frac, 4, propPtr);
    rootSection.frontSparLwrLam = QiLaminate(frac, 4, propPtr);
    rootSection.rearSparUprLam = QiLaminate(frac, 4, propPtr);
    rootSection.rearSparLwrLam = QiLaminate(frac, 4, propPtr);
    //QiLaminate(PlyFractions(20_percent, 60_percent, 20_percent), 1, propPtr);

    rootSection.frontSparUprDim = 70_mm;
    rootSection.frontSparLwrDim = 39_mm;
    rootSection.rearSparUprDim = 62_mm;
    rootSection.rearSparLwrDim = 51_mm;

    strutChord = 945_mm;// 400_mm;//945_mm;
    strutTC = 15_percent;//10_percent;//15_percent;
    strutWallThickness = 7_mm;// 5_mm; //10_mm
    // Junction
    distFromRoot = 14.85_m;//10_m;//14.85_m;//14.85_m;//47.3_m / 4.0;// 14.85_m;
}

void Chromosome::Generate()
{
    /*static std::vector<PlyFractions> lays;
    static std::vector<PlyFractions> sortedShearLays;
    static std::vector<PlyFractions> sortedAlignedLays;*/

    // TODO bias aligned/shear

    std::default_random_engine generator;
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> stringerDist(minStringers, maxStringers);
    std::uniform_int_distribution<int> stringerDimDistmm(minStringerSizemm, maxStringerSizemm);

    StringerT rootUprStringer; // No streamwise taper

    rootUprStringer.width = double(stringerDimDistmm(generator)) / 1000.0;
    rootUprStringer.height = double(stringerDimDistmm(generator)) / 1000.0;

    std::uniform_int_distribution<int> stringerLayDist(0, lays.size() - 1);
    PlyFractions stringerLay = lays[stringerLayDist(generator)];
    int maxLays = QiLaminate::GetMaxPlyMul(stringerLay);
    std::uniform_int_distribution<int> stringerLayMulDist(/*maxLays / 2*/1, maxLays);
    rootUprStringer.element.material = QiLaminate(stringerLay, stringerLayMulDist(generator), propPtr);

    rootSection.uprStringers.clear();

    const size_t numRootUprStringers = stringerDist(generator);
    for (size_t stringerIdx = 0; stringerIdx < numRootUprStringers; ++stringerIdx)
        rootSection.uprStringers.push_back(rootUprStringer);

    StringerT rootLwrStringer; // No streamwise taper
    rootLwrStringer.width = double(stringerDimDistmm(generator)) / 1000.0;
    rootLwrStringer.height = double(stringerDimDistmm(generator)) / 1000.0;

    stringerLay = lays[stringerLayDist(generator)];
    maxLays = QiLaminate::GetMaxPlyMul(stringerLay);
    stringerLayMulDist = std::uniform_int_distribution<int>(1, maxLays);
    rootLwrStringer.element.material = QiLaminate(stringerLay, stringerLayMulDist(generator), propPtr);

    rootSection.lwrStringers.clear();

    const size_t numRootLwrStringers = stringerDist(generator);
    for (size_t stringerIdx = 0; stringerIdx < numRootLwrStringers; ++stringerIdx)
        rootSection.lwrStringers.push_back(rootLwrStringer);

    // Skins

    std::uniform_int_distribution<int> layDist(0, lays.size() - 1);
    PlyFractions lay = lays[layDist(generator)];
    auto maxMul = QiLaminate::GetMaxPlyMul(lay);
    std::uniform_int_distribution<int> layMulDist(1, maxMul);

    rootSection.uprSkinLam = QiLaminate(lay, layMulDist(generator), propPtr);

    lay = lays[layDist(generator)];
    maxMul = QiLaminate::GetMaxPlyMul(lay);
    auto laymul = std::uniform_int_distribution<int>(1, maxMul)(generator);

    rootSection.lwrSkinLam = QiLaminate(lay, laymul, propPtr);

    // Spars

    rootSection.frontSparWebLam = UniformRandLam(lays);
    rootSection.rearSparWebLam = UniformRandLam(lays);

    rootSection.frontSparUprLam = UniformRandLam(lays);
    rootSection.frontSparLwrLam = UniformRandLam(lays);
    rootSection.rearSparUprLam = UniformRandLam(lays);
    rootSection.rearSparLwrLam = UniformRandLam(lays);

    // to mm
    rootSection.frontSparUprDim = stringerDimDistmm(generator) / 1000.0;
    rootSection.frontSparLwrDim = stringerDimDistmm(generator) / 1000.0;
    rootSection.rearSparUprDim = stringerDimDistmm(generator) / 1000.0;
    rootSection.rearSparLwrDim = stringerDimDistmm(generator) / 1000.0;

    std::uniform_int_distribution<int> stcDist(8, 15);
    strutTC = double(stcDist(generator)) /*/ 100.0*/;

    // Junction
    static const double halfspan = 47900. / 2.0;
    std::uniform_int_distribution<int> halfspFracDist(30, 70);
    distFromRoot = (double)halfspFracDist(generator) / 100.0 * halfspan / 1000.0;

    static double rc = 4000;
    static double lamb = 0.4;
    static double tc = rc * lamb;
    double cx = rc + (tc - rc) / halfspan * (distFromRoot.GetValue() * 1000.);
    cx *= 0.5; // Span wingbox (10-60% chord)
    if (cx > 1000) cx = 1000;

    std::uniform_int_distribution<int> scDist(400, cx);
    strutChord = double(scDist(generator)) / 1000.0;

    std::uniform_int_distribution<int> stDist(1, 20); // 5-40
    strutWallThickness = (double)stDist(generator) / 1000.0;

    // Taper spanwise 
    bool enableTaper = false;
    if (enableTaper)
    {
        RunningSection remaining = rootSection;
        int availUprStringers = remaining.uprStringers.size();// -1;
        int availLwrStringers = remaining.lwrStringers.size();// -1;
        for (size_t taperI = 1; taperI < taper.uprStringerDiff.size(); ++taperI)
        {
            // Upr stringers
            // remaining.uprStringers.size() - 1
            int numTaper = std::geometric_distribution<>(0.5)(generator);
            //availUprStringers = (int)remaining.uprStringers.size() - 2;
            if (numTaper < (availUprStringers - 1))
            {
                taper.uprStringerDiff[taperI] = -numTaper;
                availUprStringers -= numTaper;
            }
            //remaining.uprStringers.erase(remaining.uprStringers.end() - numTaper - 1);

            // Lwr stringers 
            numTaper = std::geometric_distribution<>(0.5)(generator);
            //availUprStringers = (int)remaining.uprStringers.size() - 2;
            if (numTaper < (availLwrStringers - 1))
            {
                taper.lwrStringerDiff[taperI] = -numTaper;
                availLwrStringers -= numTaper;
            }
        }
    }
}

QiLaminate Chromosome::UniformRandLam(std::vector<PlyFractions>& lays)
{
    static std::default_random_engine generator;
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    static std::uniform_int_distribution<int> layDist(0, lays.size() - 1);
    PlyFractions lay = lays[layDist(generator)];
    auto maxMul = QiLaminate::GetMaxPlyMul(lay);
    std::uniform_int_distribution<int> layMulDist(1, maxMul);
    return QiLaminate(lay, layMulDist(generator), propPtr);
}

void Chromosome::MutateFrom(const Chromosome& other, double shrinkScale)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    gen.seed(std::chrono::system_clock::now().time_since_epoch().count());
    
    // TODO: limits
    strutTC = std::normal_distribution<>(other.strutTC.GetValue(), sqrt(5))(gen);
    distFromRoot = std::normal_distribution<>(other.distFromRoot.GetValue(), sqrt(5))(gen);
    strutChord = std::normal_distribution<>(other.strutChord.GetValue(), sqrt(5))(gen);
    strutWallThickness = std::normal_distribution<>(other.strutWallThickness.GetValue(), sqrt(5))(gen);

    rootSection.uprSkinLam = other.rootSection.uprSkinLam;
    rootSection.lwrSkinLam = other.rootSection.lwrSkinLam;

    rootSection.frontSparWebLam = other.rootSection.lwrSkinLam;
    rootSection.rearSparWebLam = other.rootSection.lwrSkinLam;

    rootSection.frontSparUprLam = other.rootSection.frontSparUprLam;
    rootSection.frontSparLwrLam = other.rootSection.frontSparLwrLam;
    rootSection.rearSparUprLam = other.rootSection.rearSparUprLam;
    rootSection.rearSparLwrLam = other.rootSection.rearSparLwrLam;

    rootSection.frontSparUprDim = std::normal_distribution<>(other.rootSection.frontSparUprDim.GetValue(), sqrt(5))(gen);
    rootSection.frontSparLwrDim = std::normal_distribution<>(other.rootSection.frontSparLwrDim.GetValue(), sqrt(5))(gen);
    rootSection.rearSparUprDim = std::normal_distribution<>(other.rootSection.rearSparUprDim.GetValue(), sqrt(5))(gen);
    rootSection.rearSparLwrDim = std::normal_distribution<>(other.rootSection.rearSparLwrDim.GetValue(), sqrt(5))(gen);

    rootSection.uprStringers = other.rootSection.uprStringers;
    rootSection.lwrStringers = other.rootSection.lwrStringers;

    // TODO: stringer taper mutations
}

void Chromosome::Crossover(const Chromosome& dad, const Chromosome& mum)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    gen.seed(std::chrono::system_clock::now().time_since_epoch().count());
    std::bernoulli_distribution dist(0.5);  // 50/50 chance of either mum or dad feature

    const auto dadUprStringer = dad.rootSection.uprStringers.front();
    const auto mumUprStringer = dad.rootSection.uprStringers.front();

    StringerT rootUprStringer;
    rootUprStringer.width = dist(gen) ? dadUprStringer.width : mumUprStringer.width;
    rootUprStringer.height = dist(gen) ? dadUprStringer.height : mumUprStringer.height;
    rootUprStringer.element.material = dist(gen) ? dadUprStringer.element.material : mumUprStringer.element.material;
    rootUprStringer.element.material = dist(gen) ? dadUprStringer.element.material : mumUprStringer.element.material;
    auto numRootUprStringers = dist(gen) ? dad.rootSection.uprStringers.size() : mum.rootSection.uprStringers.size();
    for (size_t stringerIdx = 0; stringerIdx < numRootUprStringers; ++stringerIdx)
        rootSection.uprStringers.push_back(rootUprStringer);

    const auto dadLwrStringer = dad.rootSection.lwrStringers.front();
    const auto mumLwrStringer = dad.rootSection.lwrStringers.front();

    StringerT rootLwrStringer;
    rootLwrStringer.width = dist(gen) ? dadLwrStringer.width : mumLwrStringer.width;
    rootLwrStringer.height = dist(gen) ? dadLwrStringer.height : mumLwrStringer.height;
    rootLwrStringer.element.material = dist(gen) ? dadLwrStringer.element.material : mumLwrStringer.element.material;
    rootLwrStringer.element.material = dist(gen) ? dadLwrStringer.element.material : mumLwrStringer.element.material;
    auto numRootLwrStringers = dist(gen) ? dad.rootSection.lwrStringers.size() : mum.rootSection.lwrStringers.size();
    for (size_t stringerIdx = 0; stringerIdx < numRootLwrStringers; ++stringerIdx)
        rootSection.lwrStringers.push_back(rootLwrStringer);
    
    rootSection.uprSkinLam = dist(gen) ? dad.rootSection.uprSkinLam : mum.rootSection.uprSkinLam;
    rootSection.lwrSkinLam = dist(gen) ? dad.rootSection.lwrSkinLam : mum.rootSection.lwrSkinLam;

    rootSection.frontSparWebLam = dist(gen) ? dad.rootSection.frontSparWebLam : mum.rootSection.frontSparWebLam;
    rootSection.rearSparWebLam = dist(gen) ? dad.rootSection.rearSparWebLam : mum.rootSection.rearSparWebLam;

    rootSection.frontSparUprLam = dist(gen) ? dad.rootSection.frontSparUprLam : mum.rootSection.frontSparUprLam;
    rootSection.frontSparLwrLam = dist(gen) ? dad.rootSection.frontSparLwrLam : mum.rootSection.frontSparLwrLam;
    rootSection.rearSparUprLam = dist(gen) ? dad.rootSection.rearSparUprLam : mum.rootSection.rearSparUprLam;
    rootSection.rearSparLwrLam = dist(gen) ? dad.rootSection.rearSparLwrLam : mum.rootSection.rearSparLwrLam;

    rootSection.frontSparUprDim = dist(gen) ? dad.rootSection.frontSparUprDim : mum.rootSection.frontSparUprDim;
    rootSection.frontSparLwrDim = dist(gen) ? dad.rootSection.frontSparLwrDim : mum.rootSection.frontSparLwrDim;
    rootSection.rearSparUprDim = dist(gen) ? dad.rootSection.rearSparUprDim : mum.rootSection.rearSparUprDim;
    rootSection.rearSparLwrDim = dist(gen) ? dad.rootSection.rearSparLwrDim : mum.rootSection.rearSparLwrDim;

    strutTC = dist(gen) ? dad.strutTC : mum.strutTC;
    distFromRoot = dist(gen) ? dad.distFromRoot : mum.distFromRoot;
    strutChord = dist(gen) ? dad.strutChord : mum.strutChord;
    strutWallThickness = dist(gen) ? dad.strutWallThickness : mum.strutWallThickness;
}
