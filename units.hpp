#pragma once
#include "pch.hpp"
#include <Eigen/Dense>


// See https://www.codeproject.com/Articles/447922/Application-of-Cplusplus11-User-Defined-Literals-t
// "Scientific and Engineering C++" - Barton, Nackman

// Construct any units from powers of the base SI KMS units (given as the non-type parameters M, L, T)
// E.g. Area:   PhysicalQuantity<0, 2, 0>
//      Accel:  PhysicalQuantity<0, 1, -2>
//      Force:  PhysicalQuantity<1, 1, -2>
//      Stress: PhysicalQuantity<1, -1, -2>
template<int M, int L, int T>
class PhysicalQuantity
{
public:
    constexpr PhysicalQuantity(double value = 0.0) : m_value(value) {}
    constexpr PhysicalQuantity(const PhysicalQuantity& other) : m_value(other.m_value) {}

    constexpr PhysicalQuantity operator+=(const PhysicalQuantity& rhs)
    {
        m_value += rhs.m_value;
        return *this;
    }

    constexpr PhysicalQuantity& operator-=(const PhysicalQuantity& rhs)
    {
        m_value -= rhs.m_value;
        return *this;
    }

    constexpr double Convert(const PhysicalQuantity& rhs)
    {
        return m_value / rhs.m_value;
    }

    constexpr double GetValue() const
    {
        return m_value;
    }

    // Intellisense is being retarded:
    // https://developercommunity.visualstudio.com/content/problem/374666/c-intellisense-reports-incorrect-more-than-one-ope.html
    template<int M1, int L1, int T1>
    friend PhysicalQuantity operator-(const PhysicalQuantity<M1, L1, T1>& rhs) 
    {
        return -rhs.GetValue();
    }

private:
    double m_value;
};

template<int M, int L, int T>
constexpr PhysicalQuantity<M, L, T> abs(const PhysicalQuantity<M, L, T>& val)
{
    return PhysicalQuantity<M, L, T>(std::abs(val.GetValue()));
}

template<int M, int L, int T>
constexpr PhysicalQuantity<M, L, T> operator+(const PhysicalQuantity<M, L, T>& lhs,
                                              const PhysicalQuantity<M, L, T>& rhs)
{
    return PhysicalQuantity<M, L, T>(lhs) += rhs;
}

template<int M, int L, int T>
constexpr PhysicalQuantity<M, L, T> operator-(const PhysicalQuantity<M, L, T>& lhs,
                                              const PhysicalQuantity<M, L, T>& rhs)
{
    return PhysicalQuantity<M, L, T>(lhs) -= rhs;
}

template<int M1, int L1, int T1, int M2, int L2, int T2>
constexpr PhysicalQuantity<M1 + M2, L1 + L2, T1 + T2> operator*(const PhysicalQuantity<M1, L1, T1>& lhs,
                                                                const PhysicalQuantity<M2, L2, T2>& rhs)
{
    return PhysicalQuantity<M1 + M2, L1 + L2, T1 + T2>(lhs.GetValue() * rhs.GetValue());
}

template<int M, int L, int T>
constexpr PhysicalQuantity<M, L, T> operator*(const double& lhs,
                                              const PhysicalQuantity<M, L, T>& rhs)
{
    return PhysicalQuantity<M, L, T>(lhs * rhs.GetValue());
}

template<int M, int L, int T>
constexpr PhysicalQuantity<M, L, T> operator*(const PhysicalQuantity<M, L, T>& lhs,
                                              const double& rhs)
{
    return PhysicalQuantity<M, L, T>(lhs.GetValue() * rhs);
}

template<int M1, int L1, int T1, int M2, int L2, int T2>
constexpr PhysicalQuantity<M1 - M2, L1 - L2, T1 - T2> operator/(const PhysicalQuantity<M1, L1, T1>& lhs,
                                                                const PhysicalQuantity<M2, L2, T2>& rhs)
{
    return PhysicalQuantity<M1 - M2, L1 - L2, T1 - T2>(lhs.GetValue() / rhs.GetValue());
}

template<int M, int L, int T>
constexpr PhysicalQuantity<-M, -L, -T> operator/(double lhs,
                                                 const PhysicalQuantity<M, L, T>& rhs)
{
    return PhysicalQuantity<-M, -L, -T>(lhs / rhs.GetValue());
}

template<int M, int L, int T>
constexpr PhysicalQuantity<M, L, T> operator/(const PhysicalQuantity<M, L, T>& lhs, 
                                              double rhs)
{
    return PhysicalQuantity<M, L, T>(lhs.GetValue() / rhs);
}

template<int M, int L, int T>
constexpr bool operator==(const PhysicalQuantity<M, L, T>& lhs,
                          const PhysicalQuantity<M, L, T>& rhs)
{
    return (lhs.GetValue() == rhs.GetValue());
}

template<int M, int L, int T>
constexpr bool operator!=(const PhysicalQuantity<M, L, T>& lhs,
                          const PhysicalQuantity<M, L, T>& rhs)
{
    return (lhs.GetValue() != rhs.GetValue());
}

template<int M, int L, int T>
constexpr bool operator<=(const PhysicalQuantity<M, L, T>& lhs,
                          const PhysicalQuantity<M, L, T>& rhs)
{
    return lhs.GetValue() <= rhs.GetValue();
}

template<int M, int L, int T>
constexpr bool operator>=(const PhysicalQuantity<M, L, T>& lhs,
                          const PhysicalQuantity<M, L, T>& rhs)
{
    return lhs.GetValue() >= rhs.GetValue();
}

template<int M, int L, int T>
constexpr bool operator<(const PhysicalQuantity<M, L, T>& lhs,
                         const PhysicalQuantity<M, L, T>& rhs)
{
    return lhs.GetValue() < rhs.GetValue();
}

template<int M, int L, int T>
constexpr bool operator>(const PhysicalQuantity<M, L, T>& lhs,
                         const PhysicalQuantity<M, L, T>& rhs)
{
    return lhs.GetValue() > rhs.GetValue();
}

using Percentage = PhysicalQuantity<0, 0, 0>;

using Mass   = PhysicalQuantity<1, 0, 0>;   // K
using Length = PhysicalQuantity<0, 1, 0>;   // M
using Time   = PhysicalQuantity<0, 0, 1>;   // S

using Area    = PhysicalQuantity<0,  2, 0>;
using Volume  = PhysicalQuantity<0,  3, 0>;
using Density = PhysicalQuantity<1, -3, 0>;

using Speed        = PhysicalQuantity<0, 1, -1>;
using Acceleration = PhysicalQuantity<0, 1, -2>;
using Frequency    = PhysicalQuantity<0, 0, -1>;

using Force     = PhysicalQuantity<1,  1, -2>;
using Stress    = PhysicalQuantity<1, -1, -2>;
using Moment    = PhysicalQuantity<1,  2, -2>;

using ForcePerUnitLength = PhysicalQuantity<1, 0, -2>;

using AxialStiffness = PhysicalQuantity<1, 1, -2>;
using SecondAreaMoment = PhysicalQuantity<0, 4, 0>;
using BendingStiffness = PhysicalQuantity<1, 3, -2>;

// Common aliases
using Pressure  = Stress;
using Strength  = Stress;
using Stiffness = Stress;

using Weight = Force;
using ShearForce = Force;
using BendingMoment = Moment;
using Torque = Moment;
using TorsionMoment = Moment;

using Lift = Force;
using Drag = Force;

// Expand to ""_literalToken UDLs
#define DEFINE_LITERAL(unitsType, units, literalToken) \
    constexpr unitsType operator""_##literalToken(long double value) { return value * units; } \
    constexpr unitsType operator""_##literalToken(unsigned long long value) { return static_cast<double>(value) * units; }

constexpr Percentage percent(1.0);
DEFINE_LITERAL(Percentage, percent, percent)

constexpr Length metre(1.0);
constexpr Length millimetre = metre * 1.0E-3;
constexpr Length centimetre = metre * 1.0E-2;
constexpr Length decimetre  = metre * 1.0E-1;
constexpr Length kilometre  = metre * 1.0E+3;
constexpr Length inch = 2.54 * centimetre;
constexpr Length foot = 12 * inch;
constexpr Length yard = 3 * foot;
constexpr Length mile = 5280 * foot;

DEFINE_LITERAL(Length, metre, m)
DEFINE_LITERAL(Length, millimetre, mm)
DEFINE_LITERAL(Length, centimetre, cm)
DEFINE_LITERAL(Length, decimetre, dm)
DEFINE_LITERAL(Length, kilometre, km)
DEFINE_LITERAL(Length, inch, in)
DEFINE_LITERAL(Length, foot, ft)
DEFINE_LITERAL(Length, yard, yd)
DEFINE_LITERAL(Length, mile, mi)

constexpr Frequency hertz(1.0);

DEFINE_LITERAL(Frequency, hertz, Hz)

constexpr Area metre2      = metre * metre;
constexpr Area millimetre2 = millimetre * millimetre;
constexpr Area centimetre2 = centimetre * centimetre;
constexpr Area decimetre2  = decimetre * decimetre;
constexpr Area kilometre2  = kilometre * kilometre;

DEFINE_LITERAL(Area, metre2, m2)
DEFINE_LITERAL(Area, millimetre2, mm2)
DEFINE_LITERAL(Area, centimetre2, cm2)
DEFINE_LITERAL(Area, decimetre2, dm2)
DEFINE_LITERAL(Area, kilometre2, km2)

constexpr Area inch2 = inch * inch;
constexpr Area foot2 = foot * foot;
constexpr Area mile2 = mile * mile;

DEFINE_LITERAL(Area, inch2, in2)
DEFINE_LITERAL(Area, foot2, ft2)
DEFINE_LITERAL(Area, mile2, mi2)

constexpr Volume metre3      = metre2 * metre;
constexpr Volume millimetre3 = millimetre2 * millimetre;
constexpr Volume centimetre3 = centimetre2 * centimetre;
constexpr Volume decimetre3  = decimetre2 * decimetre;
constexpr Volume kilometre3  = kilometre2 * kilometre;

DEFINE_LITERAL(Volume, metre3, m3)
DEFINE_LITERAL(Volume, millimetre3, mm3)
DEFINE_LITERAL(Volume, centimetre3, cm3)
DEFINE_LITERAL(Volume, decimetre3, dm3)
DEFINE_LITERAL(Volume, kilometre3, km3)

constexpr Volume litre = decimetre3;
constexpr Volume inch3 = inch2 * inch;
constexpr Volume foot3 = foot2 * foot;
constexpr Volume mile3 = mile2 * mile;

DEFINE_LITERAL(Volume, litre, L)
DEFINE_LITERAL(Volume, inch3, in3)
DEFINE_LITERAL(Volume, foot3, ft3)
DEFINE_LITERAL(Volume, mile3, mi3)

constexpr Time second(1.0);
constexpr Time minute = 60 * second;
constexpr Time hour   = 60 * minute;
constexpr Time day    = 24 * hour;

DEFINE_LITERAL(Time, second, s)
DEFINE_LITERAL(Time, minute, min)
DEFINE_LITERAL(Time, hour, hr)
DEFINE_LITERAL(Time, day, day)

constexpr Speed metrePerSecond = metre / second;
constexpr Acceleration metrePerSecond2 = metrePerSecond / second;

DEFINE_LITERAL(Speed, metrePerSecond, mPerS)
DEFINE_LITERAL(Acceleration, metrePerSecond2, mPerS2)

constexpr Mass kg(1.0);
constexpr Mass gramme = kg * 1.0E-3;
constexpr Mass tonne  = kg * 1.0E+3;

DEFINE_LITERAL(Mass, kg, kg)
DEFINE_LITERAL(Mass, gramme, g)
DEFINE_LITERAL(Mass, tonne, t)

constexpr Mass ounce = 0.028349523125 * kg;
constexpr Mass pound = 16 * ounce;
constexpr Mass stone = 14 * pound;

DEFINE_LITERAL(Mass, ounce, oz)
DEFINE_LITERAL(Mass, pound, lb)
DEFINE_LITERAL(Mass, stone, st)

constexpr Density kgPerM3 = kg / metre3;
constexpr Density gPerCm3 = gramme / centimetre3;
constexpr Density kgPerL = kg / litre;
constexpr Density gPerL = gramme / litre;

DEFINE_LITERAL(Density, kgPerM3, kgPerM3)
DEFINE_LITERAL(Density, gPerCm3, gPerCm3)
DEFINE_LITERAL(Density, kgPerL, kgPerL)
DEFINE_LITERAL(Density, gPerL, gPerL)

constexpr Force newton = kg * metrePerSecond2;
constexpr Pressure pa  = newton / metre2;
constexpr Stress  mpa  = pa * 1.0E6;
constexpr Stress  gpa  = pa * 1.0E9;

DEFINE_LITERAL(Force, newton, N)
DEFINE_LITERAL(Pressure, pa, Pa)
DEFINE_LITERAL(Stress, mpa, MPa)
DEFINE_LITERAL(Stress, gpa, GPa)

constexpr Moment newtonM = newton * metre;

DEFINE_LITERAL(Moment, newtonM, Nm)

constexpr ForcePerUnitLength newtonPerM = newton / metre;

DEFINE_LITERAL(ForcePerUnitLength, newtonPerM, NPerM);

constexpr SecondAreaMoment metre4 = metre3 * metre;
constexpr SecondAreaMoment millimetre4 = millimetre3 * millimetre;

DEFINE_LITERAL(SecondAreaMoment, metre4, m4)
DEFINE_LITERAL(SecondAreaMoment, millimetre4, mm4)

constexpr BendingStiffness newtonM2 = newton * metre2;
constexpr BendingStiffness newtonMm2 = newton * millimetre2;

DEFINE_LITERAL(BendingStiffness, newtonM2, Nm2)
DEFINE_LITERAL(BendingStiffness, newtonMm2, Nmm2)


// Extend Eigen

// OK
using Vec3Lengths = Eigen::Matrix<Length, 3, 1>;

// Want to use: Vec3<Length>

template<typename Quantity>
class Vec3 : public Eigen::Matrix<Quantity, 3, 1> {
public:
    //Vec3() : Eigen::Matrix<Quantity, 3, 1>(0., 0., 0.) {}
    //Vec3(const Quantity& x, const Quantity& y, const Quantity& z) 
    //    : Eigen::Matrix<Quantity, 3, 1>(x, y, z) {}
    //Vec3(const Eigen::Vector3d& eigVec)
    //    : Eigen::Matrix<Quantity, 3, 1>(eigVec) {}
    //Vec3(Eigen::Vector3d&& eigVec)
    //    : Eigen::Matrix<Quantity, 3, 1>(eigVec) {}
};

template<template<int,int,int> typename Quantity, int A, int B, int C>
class Vec3<Quantity<A, B, C>> : public Eigen::Matrix<Quantity<A, B, C>, 3, 1> {
public:
    Vec3() : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(Length(0.), Length(0.), Length(0.)) {}

    /*Vec3(const Quantity<A,B,C>& x, const Quantity<A,B,C>& y, const Quantity<A,B,C>& z) 
        : Eigen::Matrix<Quantity<A,B,C>, 3, 1>(x, y, z) {}*/
    Vec3(const double& x, const double& y, const double& z)
        : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(
            static_cast<Quantity<A, B, C>>(x), static_cast<Quantity<A, B, C>>(y), static_cast<Quantity<A, B, C>>(z)) {}
    Vec3(const Eigen::Vector3d& eigVec)
        : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(eigVec.cast<Quantity<A,B,C>>()) {}
    Vec3(Eigen::Vector3d&& eigVec)
        : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(eigVec.cast<Quantity<A,B,C>>()) {}

    Vec3(const Quantity<A, B, C>& x, const Quantity<A, B, C>& y, const Quantity<A, B, C>& z)
        : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(x, y, z) {}

    template<typename PassInit>
    __forceinline explicit Vec3(const PassInit& val)
        : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(val) {}
    
    template<typename T0, typename T1>
    __forceinline Vec3(const T0& x, const T1& y) 
        : Eigen::Matrix<Quantity<A, B, C>, 3, 1>(x, y) {}

    constexpr Eigen::Vector3d GetValue() const
    {
        return Eigen::Vector3d((*this)[0].GetValue(), (*this)[1].GetValue(), (*this)[2].GetValue());
    }
};

template<template<int, int, int> typename Quantity, int A, int B, int C>
constexpr Vec3<Quantity<A, B, C>> operator*(const double& lhs, const Vec3<Quantity<A, B, C>>& rhs)
{
    // TODO: fix operators and Eigen's expressions (not ending up at the PhysicalQuantity op overloads)
    // TODO: this shitty hack for "/"
    auto val = rhs.GetValue();
    auto result = val * lhs;
    auto resultVec = (Eigen::Vector3d)result;
    auto conv = Vec3<Quantity<A, B, C>>(resultVec);
    return conv;
    //Eigen::Vector3d val = rhs.GetValue();
    //return Vec3<Quantity<A, B, C>>(lhs * val);
}

template<template<int, int, int> typename Quantity, int A, int B, int C>
constexpr Vec3<Quantity<A, B, C>> operator*(const Vec3<Quantity<A, B, C>>& lhs, const double& rhs)
{
    // TODO: fix operators and Eigen's expressions (not ending up at the PhysicalQuantity op overloads)
    // TODO: this shitty hack for "/"
    auto val = lhs.GetValue();
    auto result = val * rhs;
    auto resultVec = (Eigen::Vector3d)result;
    auto conv = Vec3<Quantity<A, B, C>>(resultVec);
    return conv;
    //Eigen::Vector3d val = rhs.GetValue();
    //return Vec3<Quantity<A, B, C>>(lhs * val);
}

//template<int M, int L, int T>
//constexpr PhysicalQuantity<M, L, T> operator*(const PhysicalQuantity<M, L, T>& lhs,
//    const double& rhs)
//{
//    return PhysicalQuantity<M, L, T>(lhs.GetValue() * rhs);
//}

//using Point3d = Vec3<Length>;
using Point3d = Eigen::Matrix<double, 3, 1>;

using Vec3d = Eigen::Vector3d;
using Line3d = Eigen::ParametrizedLine<double, 3>;
using Plane3d = Eigen::Hyperplane<double, 3>;

// "Length is not a class template"
//template<template<int,int,int> class Quantity, int A, int B, int C>
//class Vec3 : public Eigen::Matrix<Quantity<A, B, C>, 3, 1> {};

// "Invalid conversion from PhysicalQuantity<0, 1, 0> to Eigen::Matrix<Length, 3, 1, 0, 3, 1>"
//template<typename Quantity>
//class Vec3 : public Eigen::Matrix<Quantity, 3, 1> {};