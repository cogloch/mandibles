#pragma once
#include <memory>


class OutputSet;
class ConfigState;

class Solver
{
public:
    Solver(std::shared_ptr<ConfigState>, std::shared_ptr<OutputSet>);

    void Run();

private:
    std::shared_ptr<ConfigState> m_configState;
    std::shared_ptr<OutputSet> m_outputSet;
};