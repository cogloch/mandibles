#pragma once
#include "units.hpp"
#include <vector>
#include <QtCharts>
using namespace QtCharts;


struct OutputSet
{
    std::vector<Length> deflectionClean;
    std::vector<Length> deflectionStrut;

    // Temp
    QLineSeries* seriesDeflectionStrut = nullptr;
    QLineSeries* seriesDeflectionClean = nullptr;
    QLineSeries* seriesDeflectionJunctionPos = nullptr;
    QLineSeries* seriesDeflectionLimit = nullptr;
    QLineSeries* seriesDeflectionZero = nullptr;

    QLineSeries* seriesSF = nullptr;

    QChart* sfChart = nullptr;

    bool converged = false;
    QLabel* convergedFlag = nullptr;

    struct SeriesRF
    {
        QLineSeries* series;
        std::string label;
    };
    std::vector<SeriesRF> rfDirStress;

    QLineSeries* zeroRF;
};


