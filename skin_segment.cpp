#include "pch.hpp"
#include "skin_segment.hpp"


Length SkinSegment::GetLength() const
{
    Length result(0_m);
    for (size_t i = 1; i < verts.size(); ++i)
    {
        auto segStart = verts[i - 1];
        auto segEnd = verts[i];
        result += (segEnd - segStart).norm();
    }
    return result;
}

Area SkinSegment::GetEffArea() const
{
    auto t = element.material.GetThickness();
    // Lots of overlap!!!
    //auto l = GetLength();
    //return t * l;

    auto overlapArea = GetLength() * t;

    auto vertsBot = verts;
    for (auto& vert : vertsBot)
        vert.y() += t.GetValue();
    auto vertsThick = verts;
    for (auto it = vertsBot.rbegin(); it != vertsBot.rend(); ++it)
    {
        vertsThick.push_back(*it);
    }

    // Gauss area
    Area area = 0_m2;

    for (size_t i = 0; i < vertsThick.size() - 1; ++i)
    {
        const auto xcur = vertsThick[i].z();
        const auto ynext = vertsThick[i + 1].y();
        const auto darea = xcur * ynext;
        area += darea;
    }

    area += vertsThick[vertsThick.size() - 1].z() * vertsThick[0].y();

    for (size_t i = 0; i < vertsThick.size() - 1; ++i)
        area -= vertsThick[i + 1].z() * vertsThick[i].y();

    area -= vertsThick[0].z() * vertsThick[vertsThick.size() - 1].y();

    auto cleanArea = 0.5 * abs(area);

    // TODO: clean; just for debug 
    auto darea = overlapArea - cleanArea;

    return cleanArea;
}

Stiffness SkinSegment::GetEffEx()
{
    return element.material.GetEffEx();
}
