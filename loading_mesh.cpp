#include "pch.hpp"
#include "loading_mesh.hpp"
#include "structural_mesh.hpp"


std::vector<EquivSectionalLoad> LoadingMesh::SolveAt(const ReferenceAxis& axis, double loadFactor)
{
    std::vector<EquivSectionalLoad> loads(axis.pts.size());
    loads.back().refPoint = axis.pts.back();
    loads.back().shear = 0_N;
    loads.back().spanMoment = 0_Nm;
    loads.back().streamMoment = 0_Nm;

    // Integrate distrib loads
    for (int i = loads.size() - 2; i >= 0; --i)
    {
        loads[i].refPoint = axis.pts[i];
        const auto dx = abs(axis.pts[i + 1].x() - axis.pts[i].x());

        double meanShear = 0.;
        for (auto& distribLoad : distribLoads)
        {
            // Fuck it, assume same sections
            meanShear += 
                0.5 * (distribLoad.unitSamples[i].force + distribLoad.unitSamples[i + 1].force).GetValue();
        }

        loads[i].shear = loads[i + 1].shear + Force(meanShear) * dx;
    }

    // Add point loads
    for (auto& ptLoad : pointLoads)
    {
        for (auto& sec : loads)
        {
            if (sec.refPoint.x() > ptLoad.appPoint.x())
                break;
            sec.shear += ptLoad.force;
        }
    }

    for (auto& sec : loads)
        sec.shear = sec.shear * loadFactor;

    //
    // Take moments
    //

    // Spanwise (BM)
    // Integrate SF 
    for (int i = loads.size() - 2; i >= 0; --i)
    {
        const Length dx = abs(axis.pts[i + 1].x() - axis.pts[i].x());
        const Force meanShear = 0.5 * (loads[i].shear + loads[i + 1].shear);
        loads[i].spanMoment = loads[i + 1].spanMoment + meanShear * dx;
    }

    // Streamwise (T)
    for (int i = loads.size() - 2; i >= 0; --i)
    {
        loads[i].streamMoment = loads[i + 1].streamMoment;

        for (auto& distribLoad : distribLoads)
        {
            // Fuck it, assume same sections
            auto meanShear = 0.5 * (distribLoad.unitSamples[i].force + distribLoad.unitSamples[i + 1].force).GetValue();
            Vec3d meanApp = distribLoad.unitSamples[i].appPoint +
                (distribLoad.unitSamples[i + 1].appPoint - distribLoad.unitSamples[i].appPoint) * 0.5;
            //auto line = Line3d::Through(axis.pts[i], axis.pts[i + 1]);
            double meansc = 0.5 * (axis.pts[i].z() + axis.pts[i + 1].z());
            auto tarm = meanApp.z() - meansc; //line.distance(meanApp);
            
            loads[i].streamMoment += loadFactor * (Force)meanShear * (Length)tarm;
        }
    }

    for (auto& ptLoad : pointLoads)
    {
        for (size_t i = 0; i < loads.size() - 1; ++i)
        {
            auto& cur = loads[i];
            auto& next = loads[i + 1];

            if (cur.refPoint.x() > ptLoad.appPoint.x())
                break;

            auto meansc = 0.5 * (axis.pts[i].z() + axis.pts[i + 1].z());
            Length tarm = ptLoad.appPoint.z() - meansc; //line.distance(meanApp);

            cur.streamMoment += loadFactor * ptLoad.force * tarm;
        }
    }

    return loads;
}
