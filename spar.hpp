#pragma once
#include "qi_laminate.hpp"


struct SparFlange
{
    StructuralElement element;
    Length base = 40_mm, height = 40_mm;
    Area GetArea() const;
    SecondAreaMoment I() const;
};

struct SparWeb
{
    StructuralElement element;
};

struct Spar
{
    SparFlange uprFlange;
    SparFlange lwrFlange;
    SparWeb web;

    Length GetWebHeight() const;
    Area GetWebArea() const;
    Area GetEffArea() const;
};
