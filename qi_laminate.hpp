#pragma once
#include "utils.hpp"
#include "composites.hpp"


inline int Gcd(int a, int b)
{
    while (b)
    {
        int t = b;
        b = a % b;
        a = t;
    }

    return a;
}

struct PlyFractions
{
    double p0, p45, p90;
    // Must be coprimes: gcd(num0, num45, num90) = 1
    int num0, num45, num90;
    int GetBasePlyCount() const;

    PlyFractions() : num0(0), num45(0), num90(0) {}
    explicit PlyFractions(double p0_, double p45_, double p90_, int num0_, int num45_, int num90_)
        : p0(p0_), p45(p45_), p90(p90_), num0(num0_), num45(num45_), num90(num90_) {}
    explicit PlyFractions(Percentage perc0, Percentage perc45, Percentage perc90)
    {
        int i0 = perc0.GetValue(), i45 = perc45.GetValue(), i90 = perc90.GetValue();
        auto gcd = Gcd(i0, Gcd(i45, i90));
        num0 = i0 / gcd;
        num45 = i45 / gcd;
        num90 = i90 / gcd;
    }
};

bool operator ==(const PlyFractions& lhs, const PlyFractions& rhs);

struct Material 
{
    virtual ~Material() = default;

    virtual Density rho() = 0;
    virtual Stress tensUlt() = 0;
    virtual Stiffness modulus() = 0;

protected:
    Material() = default;
};

struct QiLaminate : Material
{
    Density rho() override
    {
        return mechProp->rho;
    }
    
    Stress tensUlt() override
    {
        return GetStrTens();
    }

    Stiffness modulus() override
    {
        return GetEffEx();
    }

    static const Length s_plyThickness;
    static const Length s_maxThickness;
    static std::vector<PlyFractions> s_configs;

    static std::vector<PlyFractions> GetSortedShear();
    static std::vector<PlyFractions> GetSortedTrans();
    static std::vector<PlyFractions> GetSortedAlign();
    static void GenerateLayupSpace();

////////////////////////////////////////////////////////////////
    QiLaminate() = default;
    QiLaminate(PlyFractions baseFractions, int numBase, std::shared_ptr<Ply> mechProp_)
        : baseLam(baseFractions)
        , numBaseLams(numBase)
        , mechProp(mechProp_)
    {
    }

    static int GetMaxPlyMul(PlyFractions& frac);

    PlyFractions baseLam;
    int numBaseLams;

    Length GetBaseLamThickness() const;
    Length GetThickness() const;

    Eigen::Matrix3d GetMatrixD()
    {
        if (!updateD)
            return D;

        enum Orientation { Zero, FortyfivePos, FortyfiveNeg, Ninety };
        // this is shit
        // all of it
        std::vector<Orientation> base;
        int baseSz = baseLam.GetBasePlyCount();
        int sz0 = baseLam.num0;
        int sz45 = baseLam.num45;
        int sz90 = baseLam.num90;
        // 0/pm45/90
        /*while (baseSz > 0)
        {
            if (sz0 > 0)
            {
                sz0--;
                baseSz--;
                base.push_back(Zero);
            }
            if (sz45 > 0)
            {
                sz45 -= 2;
                baseSz -= 2;
                base.push_back(Fortyfive);
                base.push_back(Fortyfive);
            }
            if (sz90 > 0)
            {
                sz90--;
                baseSz--;
                base.push_back(Ninety);
            }
        }
        
        std::vector<Orientation> half;
        for (int i = 0; i < numBaseLams; ++i)
        {
            for (auto& ply : base)
                half.push_back(ply);
        }
        */

        // 0, 90, +45, -45
        sz0 *= numBaseLams;
        sz45 *= numBaseLams;
        auto sz45Pos = sz45 / 2;
        auto sz45Neg = sz45 / 2;
        sz90 *= numBaseLams;
        std::vector<Orientation> half;
        if (sz0 > sz90)
        {
            while (sz90 > 0)
            {
                half.push_back(Zero);
                half.push_back(Ninety);
                sz0--;
                sz90--;
            }

            while (sz45Pos > 0 && sz0 > 0)
            {
                half.push_back(Zero);
                half.push_back(FortyfivePos);
                sz0--;
                sz45Pos--;
            }

            if (sz0 > 0)
            {
                // +45 gone
                while (sz45Neg > 0 && sz0 > 0)
                {
                    half.push_back(Zero);
                    half.push_back(FortyfiveNeg);
                    sz0--;
                    sz45Neg--;
                }

                while (sz0 > 0)
                {
                    half.push_back(Zero);
                    sz0--;
                }

                while (sz45Neg > 0)
                {
                    half.push_back(FortyfiveNeg);
                    sz45Neg--;
                }
            }
            else
            {
                // 0 gone
                while (sz45Neg > 0 && sz45Pos > 0)
                {
                    half.push_back(FortyfivePos);
                    half.push_back(FortyfiveNeg);
                    sz45Pos--;
                    sz45Neg--;
                }

                while (sz45Pos > 0)
                {
                    half.push_back(FortyfivePos);
                    sz45Pos--;
                }

                while (sz45Neg > 0)
                {
                    half.push_back(FortyfiveNeg);
                    sz45Neg--;
                }
            }
        }
        else
        {
            while (sz0 > 0)
            {
                half.push_back(Ninety);
                half.push_back(Zero);
                sz0--;
                sz90--;
            }

            while (sz45Pos > 0 && sz90 > 0)
            {
                half.push_back(Ninety);
                half.push_back(FortyfivePos);
                sz90--;
                sz45Pos--;
            }

            if (sz90 > 0)
            {
                // +45 gone
                while (sz45Neg > 0 && sz90 > 0)
                {
                    half.push_back(Ninety);
                    half.push_back(FortyfiveNeg);
                    sz90--;
                    sz45Neg--;
                }

                while (sz90 > 0)
                {
                    half.push_back(Ninety);
                    sz90--;
                }

                while (sz45Neg > 0)
                {
                    half.push_back(FortyfiveNeg);
                    sz45Neg--;
                }
            }
            else
            {
                // 90 gone
                while (sz45Neg > 0 && sz45Pos > 0)
                {
                    half.push_back(FortyfivePos);
                    half.push_back(FortyfiveNeg);
                    sz45Pos--;
                    sz45Neg--;
                }

                while (sz45Pos > 0)
                {
                    half.push_back(FortyfivePos);
                    sz45Pos--;
                }

                while (sz45Neg > 0)
                {
                    half.push_back(FortyfiveNeg);
                    sz45Neg--;
                }
            }
        }

        std::vector<Orientation> full = half;
        for (auto it = half.rbegin(); it != half.rend(); ++it)
        {
            // Keep symmetric 
            /*if (*it == FortyfivePos)
            {
                full.push_back(FortyfiveNeg);
                continue;
            }
            if (*it == FortyfiveNeg)
            {
                full.push_back(FortyfivePos);
                continue;
            }*/
            full.push_back(*it);
        }

        D = Eigen::Matrix3d();

        Eigen::Matrix3d q[] = {
            mechProp->GetQ0(),
            mechProp->GetQ45Pos(),
            mechProp->GetQ45Neg(),
            mechProp->GetQ90()
        };
        double t = (0.125_mm).GetValue();
        double bot = -(half.size() * t);
        for (int k = 0; k < full.size(); ++k)
        {
            auto h0 = bot + k * t;
            auto h1 = h0 + t;
            auto h = pow(h1, 3) - pow(h0, 3);
            D = D + (1. / 3.) * q[full[k]] * h;
        }

        updateD = false;
        return D;
    }

    Eigen::Matrix3d D;
    bool updateD = true;

    Stiffness GetEffGxy()
    {
        if (m_gxyFactor < 0.)
        {
            double plynum = baseLam.GetBasePlyCount();
            m_gxyFactor = s_gxyContrib.dot(Vec3d(double(baseLam.num0) / plynum,
                double(baseLam.num45) / plynum,
                double(baseLam.num90) / plynum));
        }

        Stiffness fibreDir = mechProp->E1 / (2. * (1. + 0.33));
        return m_gxyFactor * fibreDir;
    }

    Stress GetAllTau()
    {
        if (m_allTauFactor < 0.)
        {
            double plynum = baseLam.GetBasePlyCount();
            m_allTauFactor = s_allTauContrib.dot(Vec3d(double(baseLam.num0) / plynum,
                double(baseLam.num45) / plynum,
                double(baseLam.num90) / plynum));
        }

        return m_allTauFactor * (mechProp->S1t / 2.0);
    }

    Stiffness GetEffEx()
    {
        if (m_exFactor < 0.)
        {
            double plynum = baseLam.GetBasePlyCount();
            m_exFactor = s_exContrib.dot(Vec3d(double(baseLam.num0) / plynum, 
                                               double(baseLam.num45) / plynum,
                                               double(baseLam.num90) / plynum));
        }

        return m_exFactor * mechProp->E1;
    }

    Stiffness GetEffEy()
    {
        if (m_eyFactor < 0.)
        {
            double plynum = baseLam.GetBasePlyCount();
            m_eyFactor = s_eyContrib.dot(Vec3d(double(baseLam.num0) / plynum,
                double(baseLam.num45) / plynum,
                double(baseLam.num90) / plynum));
        }

        return m_eyFactor * mechProp->E1;
    }

    Stress GetStrTens()
    {
        if (m_strTensFactor < 0.)
        {
            double plynum = baseLam.GetBasePlyCount();
            m_strTensFactor = s_exContrib.dot(Vec3d(double(baseLam.num0) / plynum,
                double(baseLam.num45) / plynum,
                double(baseLam.num90) / plynum));
        }

        return m_strTensFactor * mechProp->S1t;
    }

    std::shared_ptr<Ply> mechProp;

private:
    double m_exFactor = -1.;
    double m_eyFactor = -1.;
    double m_gxyFactor = -1.;
    double m_allTauFactor = -1.;

    double m_strTensFactor = -1.;

    static const Eigen::Vector3d s_exContrib;
    static const Eigen::Vector3d s_eyContrib;
    static const Eigen::Vector3d s_gxyContrib;
    static const Eigen::Vector3d s_allTauContrib;
};

struct StructuralElement
{
    Vec3d centroid;
    //std::shared_ptr<QiLaminate> material;
    QiLaminate material;

    // All at ULT
    enum Case
    {
        Pos1, Neg1, Pos25
    };
    std::array<Stress, 3> dirStress;
    Stress shearStress;
    double maxShearFlow;
};

