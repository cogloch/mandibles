#include "pch.hpp"
#include "wing_geometry.hpp"

#include <qfileinfo.h>
#include "utils.hpp"
#include "units.hpp"
#include "airfoil.hpp"
#include "wingbox.hpp"
#include "geometry.hpp"

// II.Form wing
// Spanwise : x
// Thicknesswise : y
// Streamwise : z
//
// Simplify for now:
//      ONE sector:
//         root airfoil, tip airfoil 
//         root incidence, tip incidence (twist)               ==== TODO
//         root chord, tip chord                              
//         tip displacement back (from quarter-chord sweep)    ==== TODO 
//
// 1. Load params for ONE sector
// ~Airfoils now in (topY, botY) at each chordFrac in [0..1]
// 2. Match chordFracs of root and tip airfoils (add (topY,botY) pairs on either envelope)
// ~For root airfoil:
// 2. Scale airfoil in local space by chord (around its origin)
// 3. Rotate airfoil in local space by incidence with the TE as pivot (aero, NOT geom twist)  ==== TODO
//          (shift all points in z by -rootChord/2, rotate, shift back by +rootChord/z)       ==== TODO
// ~Do 2-3 for tip airfoil
// 4. Shift back in z tip airfoil by -tipDispBack 
//
// III. Fit wingbox
// ~ Fit wingbox envelope at N+2 sections
//          Two sections reserved for root and tip envelopes
//          The rest of N sections are between the root and tip, given 
//          as fractions of semispan spanFrac in (0,semispan)
// LERP airfoil envelopes between the root and tip at each spanFrac 
//          Find points between matching chordFracs
// For each airfoil envelope (including root and tip), cut front and rear spar VERTICAL planes
//          ~ Spars are VERTICAL (perp to xz plane)



inline Vec3d Lerp3(const Vec3d& from, const Vec3d& to, double scale)
{
    return from + (to - from) * scale;
}

//inline Point3d Lerp3(const Point3d& from, const Point3d& to, double scale)
//{
//    auto sub = (Point3d)(to - from);
//    auto sca = (Point3d)(scale * sub);
//    auto tot = (Point3d)(from + sca);
//    return tot;
//}

struct Wing::Impl
{
    Airfoil m_rootFoil, m_tipFoil;
    Percentage m_frontOffset = 0.0_percent, m_rearOffset = 0.0_percent;
    Length m_rootChord = 0.0_m, m_tipChord = 0.0_m;
    Length m_tipDispBack = 0.0_m; // Offset in Z between LE at root and LE at tip (+ve back/towards tail)
    Length m_semispan = 0.0_m;
    // TODO: dihedral

    bool m_needsUpdating = true;
    SectionalEnvelope m_boxEnvelope;
    SectionalEnvelope m_airfoilEnvelope;

    void CheckWingParams();

    struct SectionVerts
    {
        // Map airfoil to wing space 
        // x_world = (0 at root; semispan at tip)
        // y_world = y_foil
        // z_world = x_foil
        SectionVerts(PiecewiseCurve2d& top, PiecewiseCurve2d& bot);

        // Do the maths on basic types because of Eigen's super smart operation optimizations that fuck everything up 
        std::vector<Vec3d> verts;
    };

    Plane3d MakeSparPlane(const Vec3d& rootLE, const Vec3d& rootTE,
        const Vec3d& tipLE, const Vec3d& tipTE,
        Percentage offset);

    enum Surface
    {
        TOP = 1 << 0,
        BOT = 1 << 1,
        BOTH = TOP | BOT
    };
    std::pair<size_t, Line3d> GetIntersectingSegment(
        std::vector<Vec3d>& section,
        Vec3d chordPoint,
        Surface surface);

    void AddSurfacePointAtChordPoint(
        std::vector<Vec3d>& section,
        Surface surface,
        Plane3d& cuttingPlane,
        Vec3d& pointOnChord);

    void UpdateWingboxEnvelope(size_t numSections);

    void MatchAirfoilSurfaces(PiecewiseCurve2d& surfA, PiecewiseCurve2d& surfB);
};

Wing::Wing() : m_impl(new Impl)
{
}

Wing::Wing(const Wing& other) : m_impl(new Impl(*other.m_impl))
{
}

Wing::~Wing()
{
}

SectionalEnvelope Wing::GetWingboxEnvelope(size_t numSections)
{
    if (m_impl->m_needsUpdating || m_impl->m_boxEnvelope.sections.size() != numSections)
        m_impl->UpdateWingboxEnvelope(numSections);
    return m_impl->m_boxEnvelope;
}

SectionalEnvelope Wing::GetWingboxEnvelope(const std::vector<Length>& sectionSpacings)
{
    throw "TODO";
    return SectionalEnvelope();
}

const SectionalEnvelope& Wing::GetAirfoilEnvelope() const
{
    return m_impl->m_airfoilEnvelope;
}

Length Wing::GetSemispan() const
{
    return m_impl->m_semispan;
}

Length Wing::GetSpan() const
{
    return m_impl->m_semispan * 2.0;
}

void Wing::Impl::CheckWingParams()
{
    // Be very angry at bad params 
}

Wing::Impl::SectionVerts::SectionVerts(PiecewiseCurve2d& top, PiecewiseCurve2d& bot)
{
    verts.reserve(top.GetSize() * 2);
    for (auto& pt : top)
        verts.emplace_back(Vec3d(0., pt.y(), pt.x()));
    for (auto pt = bot.rbegin(); pt != bot.rend(); ++pt)
        verts.emplace_back(Vec3d(0., pt->y(), pt->x()));
}

Plane3d Wing::Impl::MakeSparPlane(const Vec3d& rootLE, const Vec3d& rootTE,
                                                 const Vec3d& tipLE, const Vec3d& tipTE,
                                                 Percentage offset)
{
    const auto tipXChord = Lerp3(tipLE, tipTE, offset.GetValue() / 100.0);
    const auto rootXChord = Lerp3(rootLE, rootTE, offset.GetValue() / 100.0);

    const auto tipPt = tipXChord;
    const auto rootPtTop = rootXChord + Vec3d(0., 1000., 0.);
    const auto rootPtBot = rootXChord - Vec3d(0., 1000., 0.);

    return Plane3d::Through(tipPt, rootPtTop, rootPtBot);
}

std::pair<size_t, Line3d> Wing::Impl::GetIntersectingSegment(
    std::vector<Vec3d>& section, Vec3d chordPoint, Surface surface)
{
    const auto target = chordPoint.z();

    // Assume NO TWIST
    switch (surface)
    {
    case TOP:
    {
        for (size_t i = 1; i < section.size() /*/ 2*/; ++i)
            if (section[i].z() > target)
                return { i, Line3d::Through(section[i - 1], section[i]) };
    }
    break;
    case BOT:
    {
        for (size_t i = section.size() - 2; i > 0 /*section.size() / 2*/; --i)
            if (section[i].z() > target)
                return { i + 1, Line3d::Through(section[i], section[i + 1]) };
    }
    break;
    }

    return {};
}

void Wing::Impl::AddSurfacePointAtChordPoint(
    std::vector<Vec3d>& section, Surface surface, Plane3d& cuttingPlane, Vec3d& pointOnChord)
{
    if (surface & TOP)
    {
        // TODO: search as done in <StructuralMesh> (point on chord DOES NOT CORRESPOND to real surface point)
        const auto surfSegment = GetIntersectingSegment(section, pointOnChord, TOP);
        const auto surfPoint = surfSegment.second.intersectionPoint(cuttingPlane);
        section.insert(section.begin() + surfSegment.first, surfPoint);
    }

    if (surface & BOT)
    {
        const auto surfSegment = GetIntersectingSegment(section, pointOnChord, BOT);
        const auto surfPoint = surfSegment.second.intersectionPoint(cuttingPlane);
        section.insert(section.begin() + surfSegment.first, surfPoint);
    }
}

Vec3d GetLe(const std::vector<Vec3d>& section)
{
    double minZ = std::numeric_limits<double>::max();
    Vec3d le;
    for (auto& pt : section)
    {
        if (pt.z() < minZ)
        {
            minZ = pt.z();
            le = pt;
        }
    }

    return le;
}

Vec3d GetTe(const std::vector<Vec3d>& section)
{
    double maxZ = std::numeric_limits<double>::lowest();
    Vec3d te;
    for (auto& pt : section)
    {
        if (pt.z() > maxZ)
        {
            maxZ = pt.z();
            te = pt;
        }
    }

    return te;
}

void Wing::Impl::UpdateWingboxEnvelope(size_t numSections)
{
    CheckWingParams();
    if (numSections < 2)
        throw "There must be at least two wingbox spanwise sections (root + chord + interior sections).";

    if (m_boxEnvelope.sections.size() != numSections)
    {
        // Reallocate
        m_boxEnvelope.sections.clear();
        m_boxEnvelope.sections.resize(numSections);

        m_airfoilEnvelope.sections.clear();
        m_airfoilEnvelope.sections.resize(numSections);
    }

    auto& rootTop = m_rootFoil.surfaces[Airfoil::TOP];
    auto& rootBot = m_rootFoil.surfaces[Airfoil::BOT];
    auto& tipTop = m_tipFoil.surfaces[Airfoil::TOP];
    auto& tipBot = m_tipFoil.surfaces[Airfoil::BOT];

    size_t chordwiseSectionsSz = rootTop.GetSize(); // == root BOT == tip TOP == tip BOT

    SectionVerts root(rootTop, rootBot);
    SectionVerts tip(tipTop, tipBot);

    // Scale to chord 
    // Transformations without units
    auto scaling = Eigen::UniformScaling<double>(m_rootChord.GetValue());
    auto translation = Eigen::Translation<double, 3>(0, 0, 0);
    auto xform = translation * scaling;
    for (auto& pt : root.verts)
        pt = xform * pt;

    // Scale to chord and place tip 
    // TODO: !URGENT! Fix scaling (scales in x??)
    scaling = Eigen::UniformScaling<double>(m_tipChord.GetValue());
    translation = Eigen::Translation<double, 3>(m_semispan.GetValue(), 0, m_tipDispBack.GetValue());
    xform = translation * scaling;
    for (auto& pt : tip.verts)
        pt = xform * pt;

    std::vector<std::vector<Eigen::Vector3d>> sectionVerts(numSections);
    sectionVerts.front() = std::move(root.verts);
    sectionVerts.back() = std::move(tip.verts);

    auto& rootSection = sectionVerts.front();
    auto& tipSection = sectionVerts.back();

    std::vector<double> sectionFractions(numSections);
    double fractionOffset = 1.0 / (numSections - 1);
    for (size_t i = 0; i < numSections; ++i)
        sectionFractions[i] = i * fractionOffset;

    size_t vertsPerSection = sectionVerts.front().size();

    // Lerp interior sections 
    for (size_t sectionIndex = 1; sectionIndex < numSections - 1; ++sectionIndex)
    {
        auto& section = sectionVerts[sectionIndex];
        for (size_t vertIndex = 0; vertIndex < vertsPerSection; ++vertIndex)
            section.push_back(Lerp3(rootSection[vertIndex], tipSection[vertIndex], sectionFractions[sectionIndex]));
    }

    // Find root LE and TE 
    //auto rootLe = sectionVerts.front()[0];
    //auto rootTe = sectionVerts.front()[sectionVerts.front().size() / 2 + 1];
    //auto rootTe = sectionVerts.front()[sectionVerts.front().size() / 2];
    auto rootLe = GetLe(sectionVerts.front());
    auto rootTe = GetTe(sectionVerts.front());
    //auto tipLe = sectionVerts.back()[0];
    //auto tipTe = sectionVerts.back()[sectionVerts.back().size() / 2 + 1];
    //auto tipTe = sectionVerts.back()[sectionVerts.back().size() / 2];
    auto tipLe = GetLe(sectionVerts.back());
    auto tipTe = GetTe(sectionVerts.back());

    // Assume NO TWIST
    auto frontSparPlane = MakeSparPlane(rootLe, rootTe, tipLe, tipTe, m_frontOffset);
    auto rearSparPlane = MakeSparPlane(rootLe, rootTe, tipLe, tipTe, m_rearOffset);

    auto sectionVertsFull = sectionVerts;

    // Cut each section at the front/rear spar offsets 
    for (auto& section : sectionVerts)
    {
        // Find LE and TE of the current section (defining the chord)
        //auto le = section[0];
        //auto te = section[section.size() / 2 + 1];
        //auto te = section[section.size() / 2];
        auto le = GetLe(section);
        auto te = GetTe(section);

        // Intersection of spar w/ chord 
        //auto frontSparXChord = Line3d::Through(le, te).pointAt(m_frontOffset.GetValue() / 100.0);
        //auto rearSparXChord = Line3d::Through(le, te).pointAt(m_rearOffset.GetValue() / 100.0);
        auto frontSparXChord = Lerp3(le, te, m_frontOffset.GetValue() / 100.0);
        auto rearSparXChord = Lerp3(le, te, m_rearOffset.GetValue() / 100.0);

        std::vector<Vec3d> good;
        bool foundFront = false, foundRear = false;
        for (size_t vertI = 1; vertI < section.size(); ++vertI)
        {
            auto& prev = section[vertI - 1];
            auto& cur = section[vertI];
            auto seg = Line3d::Through(prev, cur);
            if (!foundFront)
            {
                auto skinPt = seg.intersectionPoint(frontSparPlane);
                if (skinPt.z() < cur.z())
                {
                    foundFront = true;
                    good.push_back(skinPt);
                    good.push_back(cur);
                }
            }
            else
            {
                auto skinPt = seg.intersectionPoint(rearSparPlane);
                if (skinPt.z() < cur.z())
                {
                    foundRear = true;
                    good.push_back(skinPt);
                    //good.push_back(cur);
                    break;
                }
                good.push_back(cur);
            }
        }

        if (!foundFront || !foundRear)
            throw "AAAAAAAHHHH";

        size_t tei = 0; 
        for (; tei < section.size(); ++tei)
        {
            if (EqualDouble(section[tei].z(), te.z()))
            {
                break;
            }
        }

        foundFront = false;
        foundRear = false;
        for (size_t vertI = tei + 2; vertI < section.size(); ++vertI)
        {
            auto& prev = section[vertI - 1];
            auto& cur = section[vertI];
            auto seg = Line3d::Through(prev, cur);
            if (!foundRear)
            {
                auto skinPt = seg.intersectionPoint(rearSparPlane);
                if (skinPt.z() > cur.z())
                {
                    foundRear = true;
                    auto alignZ = good.back().z();
                    good.push_back(skinPt);
                    good.back().z() = alignZ;
                    good.push_back(cur);
                }
            }
            else
            {
                auto skinPt = seg.intersectionPoint(frontSparPlane);
                if (skinPt.z() > cur.z())
                {
                    foundFront = true;
                    auto alignZ = good.front().z();
                    good.push_back(skinPt);
                    good.back().z() = alignZ;
                    //good.push_back(cur);
                    break;
                }
                good.push_back(cur);
            }
        }

        if (!foundFront || !foundRear)
            throw "AAAAAAAHHHH";

        // Used for cutting airfoil points not corresponding to the wingbox (assuming no twist)
        Vec3d frontTop = GetIntersectingSegment(section, frontSparXChord, TOP).second.intersectionPoint(frontSparPlane);
        Vec3d rearTop = GetIntersectingSegment(section, rearSparXChord, TOP).second.intersectionPoint(rearSparPlane);

        //if (EqualDouble(rearTop.z(), 1.4399999999999999))
        //    __debugbreak();

        AddSurfacePointAtChordPoint(section, BOTH, frontSparPlane, frontSparXChord);
        AddSurfacePointAtChordPoint(section, BOTH, rearSparPlane, rearSparXChord);

        section.erase(std::remove_if(section.begin(), section.end(),
            [&](Eigen::Vector3d pt) { return pt.z() < frontTop.z(); }),
            section.end());
        section.erase(std::remove_if(section.begin(), section.end(),
            [&](Eigen::Vector3d pt) { return pt.z() > rearTop.z(); }),
            section.end());

        // TODO: delete all the obsolete crap 
        section = good;
    }

    // Full (airfoil) envelope
    for (size_t secIdx = 0; secIdx < m_airfoilEnvelope.sections.size(); ++secIdx)
    {
        auto& section = m_airfoilEnvelope.sections[secIdx];
        section.sectionIndex = secIdx;
        
        for (size_t ptIdx = 0; ptIdx < sectionVertsFull[secIdx].size(); ++ptIdx)
        {
            const auto vert = sectionVertsFull[secIdx][ptIdx];
            section.verts.push_back(vert);
        }
    }

    // Cut (wingbox) envelope
    for (size_t secIdx = 0, numSections = m_boxEnvelope.sections.size(); secIdx < numSections; ++secIdx)
    {
        auto& section = m_boxEnvelope.sections[secIdx];
        section.sectionIndex = secIdx;

        for (size_t ptIdx = 0, numVerts = sectionVerts[secIdx].size(); ptIdx < numVerts; ++ptIdx)
        {
            const auto vert = sectionVerts[secIdx][ptIdx];
            section.verts.push_back(vert);
        }
    }

    m_needsUpdating = false;
}

void Wing::Impl::MatchAirfoilSurfaces(PiecewiseCurve2d& surfA, PiecewiseCurve2d& surfB)
{
    for (auto& section : surfA)
        surfB.AddPointAtX(section.x());
}

Wing& Wing::SetFoils(Airfoil root, Airfoil tip)
{
    m_impl->m_rootFoil = std::move(root);
    m_impl->m_tipFoil = std::move(tip);

    // Refine either foil, such that there are corresponding top/bottom surface points at the same Z (streamwise) positions
    m_impl->MatchAirfoilSurfaces(m_impl->m_rootFoil.surfaces[Airfoil::TOP], m_impl->m_tipFoil.surfaces[Airfoil::TOP]);
    m_impl->MatchAirfoilSurfaces(m_impl->m_rootFoil.surfaces[Airfoil::BOT], m_impl->m_tipFoil.surfaces[Airfoil::BOT]);

    m_impl->m_needsUpdating = true;
    return *this;
}

Wing& Wing::SetSparOffsets(Percentage front, Percentage rear)
{
    m_impl->m_frontOffset = front;
    m_impl->m_rearOffset = rear;

    m_impl->m_needsUpdating = true;
    return *this;
}

Wing& Wing::SetTipDisplacement(Length dispBack)
{
    if (dispBack < 0_m)
        throw "No forward sweep allowed.";

    m_impl->m_tipDispBack = dispBack;

    m_impl->m_needsUpdating = true;
    return *this;
}

Wing& Wing::SetChords(Length rootChord, Length tipChord)
{
    if (LessOrEqualDouble(rootChord.GetValue(), 0.0))
        throw "Root chord must be finite, positive.";

    m_impl->m_rootChord = rootChord;
    m_impl->m_tipChord = tipChord;

    m_impl->m_needsUpdating = true;
    return *this;
}

Wing& Wing::SetTaperRatio(Percentage taper)
{
    if (LessOrEqualDouble(m_impl->m_rootChord.GetValue(), 0.0))
        throw "Root chord must be finite, positive.";
    if (LessOrEqualDouble(taper.GetValue(), 0.0))
        throw "Taper ratio must be finite, positive.";
    if (taper > 100_percent)
        throw "Tip chord must be less or equal the root chord.";

    m_impl->m_tipChord = m_impl->m_rootChord * (taper / 100.0);

    m_impl->m_needsUpdating = true;
    return *this;
}

Wing& Wing::SetSemispan(Length semiSpan)
{
    if (LessOrEqualDouble(semiSpan.GetValue(), 0.0))
        throw "Wingspan must be finite, positive.";

    m_impl->m_semispan = semiSpan;

    m_impl->m_needsUpdating = true;
    return *this;
}
