#pragma once
#include "qi_laminate.hpp"


struct SkinSegment
{
    // Defined between stringer/flange centroids -> NOT the stiffened panel segment 
    // The stiffened panel segments will be formed by half of left segment to half of right segment:
    // O == spar flange
    // * == stringer
    // - == skin segment as defined here 
    // | == stiffened panel extent 
    // 
    // O---|---*---|---*---|---O
    // 
    // Pitch = ------
    // 3 <SkinSegment>s
    // 2 effective panels corresponding to the 2 stringers 
    // Require an even no. of verts per segment (to split panels)
    std::vector<Vec3d> verts;

    StructuralElement element;

    Length GetLength() const;
    Area GetEffArea() const;
    Stiffness GetEffEx();
};