#pragma once
#include <QWidget>
#include "config_state.hpp"
#include "solver.hpp"
#include <memory>


class ConfigWidget : public QWidget
{
    Q_OBJECT

public:
    ConfigWidget(std::shared_ptr<Solver>, 
                 std::shared_ptr<ConfigState>, 
                 QWidget* parent = nullptr);

    void BrowseRootAirfoil();
    void BrowseTipAirfoil();
    void Compute();

private:
    std::shared_ptr<Solver> m_solver;
    std::shared_ptr<ConfigState> m_configState;

    QLineEdit* m_rootAirfoilEdit = nullptr;
    QLineEdit* m_tipAirfoilEdit = nullptr;
    QLineEdit* m_rootChordEdit = nullptr;
    QLineEdit* m_taperRatioEdit = nullptr;
    QLineEdit* m_wingspanEdit = nullptr;
    QLineEdit* m_frontSparOffsetEdit = nullptr;
    QLineEdit* m_rearSparOffsetEdit = nullptr;
    QLineEdit* m_quarterChordSweepEdit = nullptr;

    QLineEdit* junctionToCentrelineEdit = nullptr;
    QLineEdit* junctionAngleEdit;
    QLineEdit* strutChordEdit;
    QLineEdit* strutLengthEdit;
    QLineEdit* strutTCEdit;
    QLineEdit* strutWallThicknessEdit;

    QLineEdit* m_strutConvTolEdit = nullptr;
    QLineEdit* m_allowableDeflectionEdit = nullptr;

    QVBoxLayout* m_mainLayout = nullptr;

    void CreateRootChord();
    void CreateTaperRatio();
    void CreateWingspan();
    void CreateFrontSparOffset();
    void CreateRearSparOffset();
    void CreateQuarterChordSweep();

    template<typename T>
    void CreateStandard(const std::string& label, QLineEdit** editWidgetPtr,
        double min, double max, const std::string& units, T* stateField,
        double stateFieldVal)
    {
        auto labelControl = new QLabel(label.c_str());
        *editWidgetPtr = new QLineEdit(std::to_string(stateFieldVal).c_str());
        (*editWidgetPtr)->setValidator(new QDoubleValidator(min, max, 2, this));
        QObject::connect(*editWidgetPtr, &QLineEdit::editingFinished, [=]() {
            *stateField = (*editWidgetPtr)->text().toDouble();

            m_solver->Run();
        });
        auto unitsControl = new QLabel(units.c_str());

        auto layout = new QHBoxLayout();
        layout->addWidget(labelControl);
        layout->addWidget(*editWidgetPtr);
        layout->addWidget(unitsControl);
        m_mainLayout->addLayout(layout);
    }
};
