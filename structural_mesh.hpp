#pragma once
#include "stringer.hpp"
#include "qi_laminate.hpp"
#include "structural_section.hpp"


struct EquivSectionalLoad;

// Allowed extent per section
struct SectionEnvelope
{
    std::vector<Vec3d> verts;

    Length GetCurveLength(size_t vert0Index, size_t vert1Index) const;
    Area GetArea() const;

    Length GetUprSkinLength() const;
    Length GetLwrSkinLength() const;

    // Assume upr/lwr symmetry about the line running through the front/rear spar midpoints 
    // Equivalent to NA (...almost)

    double GetChordLength() const
    {
        return (GetFrontSparMidpoint() - GetRearSparMidpoint()).norm();
    }

    Line3d GetChordLine() const
    {
        return Line3d::Through(GetFrontSparMidpoint(), GetRearSparMidpoint());
    }

    Vec3d GetChordMidpoint() const
    {
        const auto le = GetFrontSparMidpoint();
        const auto te = GetFrontSparMidpoint();
        return le + (te - le) * 0.5;
    }

    Vec3d GetFrontSparMidpoint() const
    {
        return verts.back() + (verts.front() - verts.back()) * 0.5;
    }

    Vec3d GetRearSparMidpoint() const
    {
        return verts[verts.size() / 2] + (verts[verts.size() / 2 - 1] - verts[verts.size() / 2]) * 0.5;
    }
};

struct WingEnvelope
{
    // Ordered root[0]->tip[size()-1]
    std::vector<SectionEnvelope> sections;

    Length GetDistanceBetweenSections(size_t sec0Index, size_t sec1Index) const;
    Volume GetVolumeBetweenSections(size_t sec0Index, size_t sec1Index) const;
    size_t GetNumSections() const;
};
// TODO compose
// Section
//       -- Aero mesh 
//       -- Box envelope
//       -- Load mesh 
//       -- Struct mesh 


struct SpanTaper
{
    SpanTaper(size_t numSections)
    {
        std::vector<int>* diffVec[] = {
            &uprStringerDiff,
            &lwrStringerDiff,
            &uprSkinPliesDiff,
            &lwrSkinPliesDiff,
            &frontSparWebPliesDiff,
            &rearSparWebPliesDiff,
            
            &frontSparUprPliesDiff,
            &rearSparUprPliesDiff,
            &frontSparLwrPliesDiff,
            &rearSparLwrPliesDiff,

            &uprStringerPliesDiff,
            &lwrStringerPliesDiff
        };

        for (auto& vec : diffVec)
            vec->resize(numSections);
    }

    // Change in number of stringers at the <index> section
    std::vector<int> uprStringerDiff;
    std::vector<int> lwrStringerDiff;

    std::vector<int> uprSkinPliesDiff;
    std::vector<int> lwrSkinPliesDiff;
    
    std::vector<int> frontSparWebPliesDiff;
    std::vector<int> rearSparWebPliesDiff;

    std::vector<int> frontSparUprPliesDiff;
    std::vector<int> frontSparLwrPliesDiff;
    std::vector<int> rearSparUprPliesDiff;
    std::vector<int> rearSparLwrPliesDiff;

    // Spanwise only (no streamwise taper other than what is done at root, because that takes some more time)
    std::vector<int> uprStringerPliesDiff;
    std::vector<int> lwrStringerPliesDiff;
};

// TODO: swap back taper sign to - (neg)
struct RunningSection
{
    // Equal pitch 
    std::vector<StringerT> uprStringers;
    std::vector<StringerT> lwrStringers;

    QiLaminate uprSkinLam;
    QiLaminate lwrSkinLam;

    QiLaminate frontSparWebLam;
    QiLaminate rearSparWebLam;

    QiLaminate frontSparUprLam;
    QiLaminate frontSparLwrLam;
    QiLaminate rearSparUprLam;
    QiLaminate rearSparLwrLam;

    Length frontSparUprDim;
    Length frontSparLwrDim;
    Length rearSparUprDim;
    Length rearSparLwrDim;

    //QiLaminate uprStringerLam;
    //QiLaminate lwrStringerLam;
};

struct ReferenceAxis
{
    // sz == sections.size()
    std::vector<Vec3d> pts;
};

struct StructuralMesh
{
    StructuralMesh(WingEnvelope boxEnvelope_);
    WingEnvelope boxEnvelope;       // Wingbox extent
    //WingEnvelope airEnvelope;       // Whole streamlined wing extent 
    std::vector<StructuralSection> sections;

    // Initialize to root properties
    RunningSection runSec;
    SpanTaper taper;

    ReferenceAxis GetRefAxis() const
    {
        ReferenceAxis ret;
        ret.pts.reserve(sections.size());
        for (auto& sec : sections)
            ret.pts.push_back(sec.GetCentroid());
    }

    enum Case
    {
        Pos1, Neg1, Pos25
    };
    std::vector<double> GetDirStressRF(std::vector<EquivSectionalLoad>&, Case loadCase = Case::Pos25);
    std::vector<std::vector<double>> GetShearStresses(std::vector<EquivSectionalLoad>&, Case loadCase = Case::Pos25);

    bool BuildLattice()
    {
        sections.clear();
        sections.reserve(boxEnvelope.sections.size());

        auto& rootSection = boxEnvelope.sections.front();
        auto& tipSection = boxEnvelope.sections.back();
        const auto rootUprSkinLen = rootSection.GetUprSkinLength();
        const auto rootLwrSkinLen = rootSection.GetLwrSkinLength();

        const Length rootUprPitch = rootUprSkinLen / (runSec.uprStringers.size() + 1);
        const Length rootUprHalfPitch = rootUprPitch / 2.0;
        const Length rootLwrPitch = rootLwrSkinLen / (runSec.lwrStringers.size() + 1);
        const Length rootLwrHalfPitch = rootLwrPitch / 2.0;

        const Line3d rootChordline = rootSection.GetChordLine();
        const Line3d tipChordline = tipSection.GetChordLine();

        // For each skin: Count pitch on the skin -> Get projection on chordline 
        // -> Projection as fraction of chord at root -> Projection as fraction of chord at tip 
        // -> Stringer cutting plane formed by <point on skin, root projection, tip projection>

        // Form cutting planes for spars and stringers 
        // Ordered LE to TE
        std::vector<Plane3d> uprStringerPlanes;
        std::vector<Plane3d> lwrStringerPlanes;

        // Count segment by segment (to account for curvature); lerp to get actual vert 
        Length runningZ = 0_m;
        size_t stringerIdx = 0;
        // Upr (should stop at half verts in practice but w/e)
        for (size_t i = 1; i < rootSection.verts.size(); ++i)
        {
            auto& prev = rootSection.verts[i - 1];
            auto& cur = rootSection.verts[i];
            Length dz = (cur - prev).norm();
            
            //auto filled = stringerIdx * rootUprPitch;
            auto target = rootUprPitch * (stringerIdx + 1);

            // Stop at full pitch 
            if (runningZ + dz > target)
            {
                //auto totalZ = filled + runningZ;

                // rz + frac*dz = target
                // frac = (target - rz) / dz      (target to next -> FLIP TO -> prev to target)
                double lerpFac = 1.0 - ((target - runningZ) / dz).GetValue();
                //runningZ = rootUprPitch - runningZ; // Reset for next stringer
                auto segline = Line3d::Through(prev, cur);
                //Vec3d skinPt = segline.pointAt(lerpFac);
                Vec3d skinPt = prev + (cur - prev) * lerpFac;
                Vec3d rootProj = rootChordline.projection(skinPt);
                const auto rootFrontMidpoint = rootSection.GetFrontSparMidpoint();
                const auto rootRearMidpoint = rootSection.GetRearSparMidpoint();
                auto chordFrac = ((rootProj - rootFrontMidpoint).norm()) /
                    ((rootRearMidpoint - rootFrontMidpoint).norm());
                //Vec3d tipProj = Line3d::Through(tipSection.frontSparMidpoint, tipSection.rearSparMidpoint)
                //    .pointAt(chordFrac);
                const auto tipFrontMidpoint = tipSection.GetFrontSparMidpoint();
                const auto tipRearMidpoint = tipSection.GetRearSparMidpoint();
                Vec3d tipProj = tipFrontMidpoint + 
                    (tipRearMidpoint - tipRearMidpoint) * chordFrac;

                uprStringerPlanes.push_back(Plane3d::Through(skinPt, rootProj, tipProj));
                if (++stringerIdx == runSec.uprStringers.size())
                    break;
            }
            
            runningZ += dz;
        }

        runningZ = 0_m;
        stringerIdx = 0;
        // Upr (should stop at half verts in practice but w/e)
        for (size_t i = rootSection.verts.size() - 2; i > rootSection.verts.size() / 2; --i)
        {
            auto& prev = rootSection.verts[i + 1];
            auto& cur = rootSection.verts[i];
            Length dz = (cur - prev).norm();

            //auto filled = stringerIdx * rootUprPitch;
            auto target = rootLwrPitch * (stringerIdx + 1);

            // Stop at full pitch 
            if (runningZ + dz > target)
            {
                //auto totalZ = filled + runningZ;

                // rz + frac*dz = target
                // frac = (target - rz) / dz      (target to next -> FLIP TO -> prev to target)
                double lerpFac = 1.0 - ((target - runningZ) / dz).GetValue();
                //runningZ = rootUprPitch - runningZ; // Reset for next stringer
                auto segline = Line3d::Through(prev, cur);
                //Vec3d skinPt = segline.pointAt(lerpFac);
                Vec3d skinPt = prev + (cur - prev) * lerpFac;
                Vec3d rootProj = rootChordline.projection(skinPt);


                const auto rootFrontMidpoint = rootSection.GetFrontSparMidpoint();
                const auto rootRearMidpoint = rootSection.GetRearSparMidpoint();
                auto chordFrac = ((rootProj - rootFrontMidpoint).norm()) /
                    ((rootRearMidpoint - rootFrontMidpoint).norm());
                //Vec3d tipProj = Line3d::Through(tipSection.frontSparMidpoint, tipSection.rearSparMidpoint)
                //    .pointAt(chordFrac);
                const auto tipFrontMidpoint = tipSection.GetFrontSparMidpoint();
                const auto tipRearMidpoint = tipSection.GetRearSparMidpoint();
                Vec3d tipProj = tipFrontMidpoint +
                    (tipRearMidpoint - tipRearMidpoint) * chordFrac;

                lwrStringerPlanes.push_back(Plane3d::Through(skinPt, rootProj, tipProj));
                if (++stringerIdx == runSec.lwrStringers.size())
                    break;
            }

            runningZ += dz;
        }

        // Decrease spanwise
        int numUprStringers = runSec.uprStringers.size();
        int numLwrStringers = runSec.lwrStringers.size();

        for (size_t secIdx = 0; secIdx < boxEnvelope.sections.size(); ++ secIdx)
        {
            auto& geomSec = boxEnvelope.sections[secIdx];
            sections.push_back({});
            auto& structSec = sections.back();

            // Taper spars
            runSec.frontSparWebLam.numBaseLams += taper.frontSparWebPliesDiff[secIdx];
            runSec.rearSparWebLam.numBaseLams += taper.rearSparWebPliesDiff[secIdx];
            runSec.frontSparUprLam.numBaseLams += taper.frontSparUprPliesDiff[secIdx];
            runSec.frontSparLwrLam.numBaseLams += taper.frontSparLwrPliesDiff[secIdx];
            runSec.rearSparUprLam.numBaseLams += taper.rearSparUprPliesDiff[secIdx];
            runSec.rearSparLwrLam.numBaseLams += taper.rearSparLwrPliesDiff[secIdx];

            //
            // Place spars
            //

            // Front spar 
            auto& frontSpar = structSec.spars[0];
            frontSpar.uprFlange.element.centroid = geomSec.verts.front();
            frontSpar.lwrFlange.element.centroid = geomSec.verts.back();
            frontSpar.web.element.centroid = geomSec.GetFrontSparMidpoint();

            frontSpar.uprFlange.element.material = runSec.frontSparUprLam;
            frontSpar.lwrFlange.element.material = runSec.frontSparLwrLam;
            frontSpar.uprFlange.base = runSec.frontSparUprDim;
            frontSpar.lwrFlange.base = runSec.frontSparLwrDim;
            frontSpar.web.element.material = runSec.frontSparWebLam;

            // Rear spar 
            const auto numVerts = geomSec.verts.size();
            auto& rearSpar = structSec.spars[1];
            rearSpar.uprFlange.element.centroid = geomSec.verts[numVerts / 2 - 1];
            rearSpar.lwrFlange.element.centroid = geomSec.verts[numVerts / 2];
            rearSpar.web.element.centroid = geomSec.GetRearSparMidpoint();

            rearSpar.uprFlange.element.material = runSec.rearSparUprLam;
            rearSpar.lwrFlange.element.material = runSec.rearSparLwrLam;
            rearSpar.uprFlange.base = runSec.rearSparUprDim;
            rearSpar.lwrFlange.base = runSec.rearSparLwrDim;
            rearSpar.web.element.material = runSec.rearSparWebLam;

            // Taper stringers
            for (auto& str : runSec.uprStringers)
                str.element.material.numBaseLams -= taper.uprStringerPliesDiff[secIdx];

            for (auto& str : runSec.lwrStringers)
                str.element.material.numBaseLams -= taper.lwrStringerPliesDiff[secIdx];

            numUprStringers += taper.uprStringerDiff[secIdx];
            if (numUprStringers < 0)
                __debugbreak();
            numLwrStringers += taper.lwrStringerDiff[secIdx];

            // Taper skins
            runSec.uprSkinLam.numBaseLams += taper.uprSkinPliesDiff[secIdx];
            runSec.lwrSkinLam.numBaseLams += taper.lwrSkinPliesDiff[secIdx];

            //
            // Place stringers
            //
            size_t vertIdx = 1;
            structSec.uprSkin.verts.push_back(geomSec.verts[0]);
            for (size_t stringerIdx = 0; stringerIdx < numUprStringers; ++stringerIdx)
            {
                auto& plane = uprStringerPlanes[stringerIdx];
                //vertIdx = 1;
                for (; vertIdx < geomSec.verts.size(); ++vertIdx)
                {
                    auto& prev = geomSec.verts[vertIdx - 1];
                    auto& cur = geomSec.verts[vertIdx];
                    auto seg = Line3d::Through(prev, cur);
                    auto skinPt = seg.intersectionPoint(plane);
                    if (skinPt.z() < cur.z())
                    {
                        StringerT stringer = runSec.uprStringers[stringerIdx];
                        stringer.position = skinPt;
                        //stringer.element.material = runSec.uprStringerLam;
                        // TODO
                        //stringer.element.centroid = stringer.GetCentroidGlobal(/* surface normal here*/);
                        stringer.element.centroid = stringer.position;
                        structSec.uprStringers.push_back(stringer);
                        structSec.uprSkin.verts.push_back(skinPt);
                        structSec.uprSkin.verts.push_back(cur);
                        ++vertIdx;
                        break;
                    }
                    structSec.uprSkin.verts.push_back(cur);
                }
            }

            // Add rest of skin verts
            //vertIdx++;
            for (; vertIdx <= geomSec.verts.size() / 2 - 1; ++vertIdx)
                structSec.uprSkin.verts.push_back(geomSec.verts[vertIdx]);

            structSec.uprSkin.element.material = runSec.uprSkinLam;

            vertIdx = geomSec.verts.size() - 2;
            structSec.lwrSkin.verts.push_back(geomSec.verts[geomSec.verts.size() - 1]);
            for (size_t stringerIdx = 0; stringerIdx < numLwrStringers; ++stringerIdx)
            {
                if (stringerIdx >= lwrStringerPlanes.size()) // Cut planes extend outside or something
                    return false;
                auto& plane = lwrStringerPlanes[stringerIdx];
                
                for (; vertIdx > geomSec.verts.size() / 2; --vertIdx)
                {
                    auto& prev = geomSec.verts[vertIdx + 1];
                    auto& cur = geomSec.verts[vertIdx];
                    auto seg = Line3d::Through(prev, cur);
                    auto skinPt = seg.intersectionPoint(plane);
                    if (skinPt.z() < cur.z())
                    {
                        StringerT stringer = runSec.lwrStringers[stringerIdx];
                        stringer.position = skinPt;
                        //stringer.element.material = runSec.lwrStringerLam;
                        // TODO
                        //stringer.element.centroid = stringer.GetCentroidGlobal(/* surface normal here*/);
                        stringer.element.centroid = stringer.position;
                        structSec.lwrStringers.push_back(stringer);
                        structSec.lwrSkin.verts.push_back(skinPt);
                        structSec.lwrSkin.verts.push_back(cur);
                        --vertIdx;
                        break;
                    }
                    structSec.lwrSkin.verts.push_back(cur);
                }
            }

            // Add rest of skin verts
            //vertIdx--;
            for (; vertIdx >= geomSec.verts.size() / 2; --vertIdx)
                structSec.lwrSkin.verts.push_back(geomSec.verts[vertIdx]);

            structSec.lwrSkin.element.material = runSec.lwrSkinLam;
        }

        return true;
    }

    std::vector<Length> Deflect(std::vector<EquivSectionalLoad>& loading);
    std::vector<double> Twist(std::vector<EquivSectionalLoad>& loading);
};




