#pragma once
#include "wingbox.hpp"
#include "structural_mesh.hpp"


struct ReferenceAxis;

// Do NOT add up 
struct EquivSectionalLoad
{
    Force         shear;        // Along y 
    BendingMoment spanMoment;   // About z [Mz/BM]
    Torque        streamMoment; // About x [Mx/T]

    Vec3d refPoint;
};

struct PointLoad
{
    PointLoad() = default;
    PointLoad(Force force_, Vec3d appPoint_) : force(force_), appPoint(appPoint_) {}
    PointLoad(ForcePerUnitLength unitForce_, Vec3d appPoint_) : force(unitForce_.GetValue()), appPoint(appPoint_) {}
    Force force;
    Vec3d appPoint;
};

struct DistributedLoad
{
    // To integrate between samples
    std::vector<PointLoad> unitSamples;
};

inline const WingboxSection* GetReferenceSection(const SectionalEnvelope& airMesh, double target)
{
    double minDist = std::numeric_limits<double>::max();
    const WingboxSection* closestSec = nullptr;
    for (auto& sec : airMesh.sections)
    {
        if (EqualDouble(sec.verts.front().x(), target))
            return &sec;

        auto dist = abs(sec.verts.front().x() - target);
        if (dist < minDist)
        {
            dist = minDist;
            closestSec = &sec;
        }
    }
    return closestSec;
}

inline const SectionEnvelope* GetReferenceSection(const WingEnvelope& structMesh, double target)
{
    double minDist = std::numeric_limits<double>::max();
    const SectionEnvelope* closestSec = nullptr;
    for (auto& sec : structMesh.sections)
    {
        auto dist = abs(sec.verts.front().x() - target);
        if (dist < minDist)
        {
            dist = minDist;
            closestSec = &sec;
        }
    }
    return closestSec;
}

// TODO: from AVL
inline DistributedLoad GenerateLiftDistribution(
    const std::vector<double>& spanwiseSamplePts,
    Length wingspan,
    Mass mtom,
    const SectionalEnvelope& airMesh)
{
    static const Acceleration g = 9.80665_mPerS2;
    static const double acFraction = 0.25;

    Weight mtow = mtom * g;
    ForcePerUnitLength L0 = 4.0 * mtow / (M_PI * wingspan);

    DistributedLoad distrib;
    for (const auto& samplePt : spanwiseSamplePts)
    {
        const auto frac = 2 * samplePt / wingspan.GetValue();
        const auto fracSq = frac * frac;
        const ForcePerUnitLength secLift = L0 * sqrt(1 - fracSq);

        auto closestSec = GetReferenceSection(airMesh, samplePt);
        const auto le = closestSec->verts.front();
        const auto te = closestSec->verts[closestSec->verts.size() / 2];
        Vec3d ac = le + (te - le) * acFraction;

        distrib.unitSamples.push_back(PointLoad(secLift, ac));
    }
    return distrib;
}

inline DistributedLoad GenerateWingMassDistrib(
    const std::vector<double>& spanwiseSamplePts,
    Length wingspan,
    Mass dryMass, // One wing
    const SectionalEnvelope& airMesh)
{
    static const Acceleration g = 9.80665_mPerS2;
    static const double cgFraction = 0.43;

    Weight wTotal = dryMass * g;
    // semispan * W0 / 2 = W
    // W0 = 2W / semispan;
    // Wx/W0 = (semispan-x)/semispan
    // Wx = W0*(semispan-x)/semispan
    const Length semispan = wingspan / 2.0;
    const ForcePerUnitLength w0 = 2.0 * wTotal / semispan;

    DistributedLoad distrib;
    for (const auto& samplePt : spanwiseSamplePts)
    {
        const auto frac = (semispan.GetValue() - samplePt) / semispan.GetValue();
        const ForcePerUnitLength wx = w0 * frac;

        auto closestSec = GetReferenceSection(airMesh, samplePt);
        const auto le = closestSec->verts.front();
        const auto te = closestSec->verts[closestSec->verts.size() / 2];
        Vec3d cg = le + (te - le) * cgFraction;

        distrib.unitSamples.push_back(PointLoad(-wx, cg));
    }
    return distrib;
}

inline DistributedLoad GenerateFuelLoadDistrib(
    const std::vector<double>& spanwiseSamplePts,
    Length wingspan,
    Mass fuelMass, // One wing
    const std::vector<StructuralSection>& structMesh)
{
    static const Acceleration g = 9.80665_mPerS2;
    static const double cgFraction = 0.5;

    Weight wTotal = fuelMass * g;
    // semispan * W0 / 2 = W
    // W0 = 2W / semispan;
    // Wx/W0 = (semispan-x)/semispan
    // Wx = W0*(semispan-x)/semispan
    const Length semispan = wingspan / 2.0;
    const ForcePerUnitLength w0 = 2.0 * wTotal / semispan;

    DistributedLoad distrib;
    for (size_t i = 0; i < spanwiseSamplePts.size(); ++i)
    {
        const auto& samplePt = spanwiseSamplePts[i];

        const auto frac = (semispan.GetValue() - samplePt) / semispan.GetValue();
        const ForcePerUnitLength wx = w0 * frac;

        //auto closestSec = GetReferenceSection(structMesh, samplePt);
        auto closestSec = structMesh[i];
        //const auto le = closestSec->verts.front();
        //const auto te = closestSec->verts[closestSec->verts.size() / 2];
        //Vec3d cg = le + (te - le) * cgFraction;
        Vec3d cg = closestSec.GetCentroid();

        distrib.unitSamples.push_back(PointLoad(-wx, cg));
    }
    return distrib;
}

struct LoadingMesh
{
    std::vector<EquivSectionalLoad> SolveAt(const ReferenceAxis& axis, double loadFactor = 1.0);

    std::vector<PointLoad> pointLoads;
    std::vector<DistributedLoad> distribLoads;
};

