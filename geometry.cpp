#include "pch.hpp"
#include "geometry.hpp"
#include "utils.hpp"


using Line2d  = Eigen::Hyperplane<double, 2>;
using Point2d = Eigen::Vector2d;

//using Line3d = Eigen::Hyperplane<double, 3>;
using Point3d = Eigen::Vector3d;

PiecewiseCurve2d::PiecewiseCurve2d(size_t numPoints)
{
    points.resize(numPoints);
}

// TODO: insertion interface
// should become unnecessary
void PiecewiseCurve2d::ClearDuplicates()
{
    auto it = points.begin() + 1;
    while (it != points.end())
        if (EqualDouble(it->x(), (it - 1)->x()))
            it = points.erase(it);
        else
            ++it;
}

// TODO: insertion interface
// should become unnecessary
void PiecewiseCurve2d::SortPoints()
{
    std::sort(points.begin(), points.end(),
        [](const Point2d& a, const Point2d& b) {
            return a.x() < b.x();
        });
}

void PiecewiseCurve2d::Refine()
{
    for (auto it = points.begin() + 1; it < points.end(); ++it)
        it = points.insert(it, Lerp2(*(it - 1), *it, 0.5));
}

void PiecewiseCurve2d::AddPointAtX(double x)
{
    if (x < 0.0 || x > 1.0)
        throw "Invalid airfoil X coord";

    if (points.size() < 2)
        throw "A fit";

    if (HasPointAtX(x))
        return;

    const auto vertLine = Line2d::Through({ x, 0.0 }, { x, 1000.0 });

    // Assume points on surface are ordered
    for (auto it = points.begin() + 1; it != points.end(); ++it)
    {
        auto prev = it - 1;
        auto cur = it;

        if (cur->x() > x)
        {
            const auto surfLine = Line2d::Through(*prev, *cur);
            points.insert(cur, surfLine.intersection(vertLine));
            return;
        }
    }
}

bool PiecewiseCurve2d::HasPointAtX(double x)
{
    for(const auto& pt : points)
        if (EqualDouble(x, pt.x()))
            return true;
    return false;
}

PiecewiseCurve3d::PiecewiseCurve3d(size_t numPoints)
{
    points.resize(numPoints);
}

// TODO: insertion interface
// should become unnecessary
void PiecewiseCurve3d::ClearDuplicates()
{
    auto it = points.begin() + 1;
    while (it != points.end())
        if (EqualDouble(it->x(), (it - 1)->x()))
            it = points.erase(it);
        else
            ++it;
}

// TODO: insertion interface
// should become unnecessary
void PiecewiseCurve3d::SortPoints()
{
    std::sort(points.begin(), points.end(),
        [](const Point3d& a, const Point3d& b) {
            return a.x() < b.x();
        });
}

void PiecewiseCurve3d::Refine()
{
    //for (auto it = points.begin() + 1; it < points.end(); ++it)
    //    it = points.insert(it, Lerp2(*(it - 1), *it, 0.5));
}

void PiecewiseCurve3d::AddPointAtX(double x)
{
    if (x < 0.0 || x > 1.0)
        throw "Invalid airfoil X coord";

    if (points.size() < 2)
        throw "A fit";

    if (HasPointAtX(x))
        return;

    //const auto vertLine = Line3d::Through({ x, 0.0 }, { x, 1000.0 });

    //// Assume points on surface are ordered
    //for (auto it = points.begin() + 1; it != points.end(); ++it)
    //{
    //    auto prev = it - 1;
    //    auto cur = it;

    //    if (cur->x() > x)
    //    {
    //        const auto surfLine = Line3d::Through(*prev, *cur);
    //        points.insert(cur, surfLine.intersection(vertLine));
    //        return;
    //    }
    //}
}

bool PiecewiseCurve3d::HasPointAtX(double x)
{
    for (const auto& pt : points)
        if (EqualDouble(x, pt.x()))
            return true;
    return false;
}