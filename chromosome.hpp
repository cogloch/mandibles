#pragma once
#include <random>
#include <chrono>
#include "qi_laminate.hpp"
#include "structural_mesh.hpp"

const size_t g_numCuts = 50;


struct Chromosome
{
    std::shared_ptr<Ply> propPtr;

    Chromosome(const Chromosome& other) = default;
    Chromosome& operator=(const Chromosome& other) = default;

    Chromosome();
    void Generate();
    void MutateFrom(const Chromosome& other, double shrinkScale);
    void Crossover(const Chromosome& dad, const Chromosome& mum);
    QiLaminate UniformRandLam(std::vector<PlyFractions>& lays);


    RunningSection rootSection;
    SpanTaper taper;

    // Box dimm
    Length strutChord = 400_mm;
    Percentage strutTC = 15_percent;
    Length strutWallThickness = 1_mm;
    // Junction
    Length distFromRoot = 11464_mm;
    static const Length fuselageHeight;

    static const int minStringers;
    static const int maxStringers;
    static const int minStringerSizemm;
    static const int maxStringerSizemm;
};



