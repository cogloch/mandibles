#include "pch.hpp"
#include "config_widget.hpp"


void AddHLine(QVBoxLayout* layout)
{
    auto line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    layout->addWidget(line);
}

ConfigWidget::ConfigWidget(std::shared_ptr<Solver> solver,
                           std::shared_ptr<ConfigState> configState,
                           QWidget* parent)
    : QWidget(parent)
    , m_configState(configState)
    , m_solver(solver)
{
    m_mainLayout = new QVBoxLayout();
    m_mainLayout->setAlignment(Qt::AlignTop);
    setLayout(m_mainLayout);

    auto refreshBtn = new QPushButton("Compute");
    m_mainLayout->addWidget(refreshBtn);
    QObject::connect(refreshBtn, &QPushButton::clicked, this, &ConfigWidget::Compute);

    // Controls in config box
    auto rootAirfoilLayout = new QGridLayout();
    m_mainLayout->addLayout(rootAirfoilLayout);

    auto rootAirfoilLabel = new QLabel("Root airfoil");
    rootAirfoilLayout->addWidget(rootAirfoilLabel, 0, 0);
    m_rootAirfoilEdit = new QLineEdit(m_configState->rootAirfoilPath.c_str());
    m_rootAirfoilEdit->setText(m_configState->rootAirfoilPath.c_str());
    m_rootAirfoilEdit->setDisabled(true);
    rootAirfoilLayout->addWidget(m_rootAirfoilEdit, 0, 1);
    auto rootAirfoilBrowseBtn = new QPushButton("Browse...");
    rootAirfoilLayout->addWidget(rootAirfoilBrowseBtn, 0, 2);
    QObject::connect(rootAirfoilBrowseBtn, &QPushButton::clicked, this, &ConfigWidget::BrowseRootAirfoil);

    auto tipAirfoilLayout = new QGridLayout();
    m_mainLayout->addLayout(tipAirfoilLayout);

    auto tipAirfoilLabel = new QLabel("Tip airfoil");
    tipAirfoilLayout->addWidget(tipAirfoilLabel, 0, 0);
    m_tipAirfoilEdit = new QLineEdit(m_configState->tipAirfoilPath.c_str());
    m_tipAirfoilEdit->setText(m_configState->tipAirfoilPath.c_str());
    m_tipAirfoilEdit->setDisabled(true);
    tipAirfoilLayout->addWidget(m_tipAirfoilEdit, 0, 1);
    auto tipAirfoilBrowseBtn = new QPushButton("Browse...");
    tipAirfoilLayout->addWidget(tipAirfoilBrowseBtn, 0, 2);
    QObject::connect(tipAirfoilBrowseBtn, &QPushButton::clicked, this, &ConfigWidget::BrowseTipAirfoil);

    //CreateRootChord();
    //CreateTaperRatio();
    //CreateWingspan();
    //CreateFrontSparOffset();
    //CreateRearSparOffset();
    //CreateQuarterChordSweep();
    CreateStandard("Root chord", &m_rootChordEdit,
        1.0, 10.0, "[m]", &m_configState->rootChord, m_configState->rootChord.GetValue());

    CreateStandard("Taper ratio", &m_taperRatioEdit,
        10.0, 100.0, "[% root chord]", &m_configState->taperRatio, m_configState->taperRatio.GetValue());

    CreateStandard("Wingspan", &m_wingspanEdit,
        1.0, 100.0, "[m]", &m_configState->wingspan, m_configState->wingspan.GetValue());

    CreateStandard("Front spar offset", &m_frontSparOffsetEdit,
        5.0, 60.0, "[% chord]", &m_configState->frontSparOffset, m_configState->frontSparOffset.GetValue());

    CreateStandard("Rear spar offset", &m_rearSparOffsetEdit,
        10.0, 85.0, "[% chord]", &m_configState->rearSparOffset, m_configState->rearSparOffset.GetValue());

    CreateStandard("1/4 Chord sweep", &m_quarterChordSweepEdit,
        0.0, 0.1, "[deg]", &m_configState->quarterChordSweep, m_configState->quarterChordSweep);

    AddHLine(m_mainLayout);

    CreateStandard("Allowable tip deflection", &m_allowableDeflectionEdit,
        1.0, 50.0, "[% semispan]", &m_configState->allowableDeflection, m_configState->allowableDeflection.GetValue());

    CreateStandard("Strut convergence tolerance", &m_strutConvTolEdit,
        0.00000001, 0.1, "[m]", &m_configState->strutConvTol, m_configState->strutConvTol.GetValue());

    AddHLine(m_mainLayout);

    CreateStandard("Junction to centreline", &junctionToCentrelineEdit,
        10.0, 90.0, "[% semispan]", &m_configState->junctionToCentreline, m_configState->junctionToCentreline.GetValue());
    CreateStandard("Junction angle", &junctionAngleEdit,
        1., 90.0, "[deg]", &m_configState->junctionAngle, m_configState->junctionAngle);
    CreateStandard("Strut chord", &strutChordEdit,
        0.01, 1.0, "[m]", &m_configState->strutChord, m_configState->strutChord.GetValue());
    CreateStandard("Strut length", &strutLengthEdit,
        1.0, 90.0, "[m]", &m_configState->strutLength, m_configState->strutLength.GetValue());
    CreateStandard("Strut t/c", &strutTCEdit,
        1.0, 100.0, "[% strut chord]", &m_configState->strutTC, m_configState->strutTC.GetValue());
    CreateStandard("Strut wall thickness", &strutWallThicknessEdit,
        0.0001, 0.1, "[m]", &m_configState->strutWallThickness, m_configState->strutWallThickness.GetValue());
   
    // Overwrite standard handlers
    //junctionToCentrelineEdit->disconnect();

    QObject::connect(junctionToCentrelineEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->junctionToCentreline = junctionToCentrelineEdit->text().toDouble();
        const auto dist = m_configState->junctionToCentreline / 100.0 * (m_configState->wingspan / 2.0);
        const Length fuselageHeight = 4_m;
        m_configState->junctionAngle = (atan((fuselageHeight / dist).GetValue())) * (180.0 / M_PI);
        junctionAngleEdit->setText(std::to_string(m_configState->junctionAngle).c_str());
        m_configState->strutLength = sqrt((fuselageHeight * fuselageHeight + dist * dist).GetValue());
        strutLengthEdit->setText(std::to_string(m_configState->strutLength.GetValue()).c_str());

        m_solver->Run();
    });

    QObject::connect(junctionAngleEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->junctionAngle = junctionAngleEdit->text().toDouble();
        const Length fuselageHeight = 4_m;
        const auto dist = fuselageHeight / tan(m_configState->junctionAngle * M_PI / 180.0);
        const auto distPerc = dist / (m_configState->wingspan / 2.0) * 100_percent;
        m_configState->junctionToCentreline = distPerc;
        junctionToCentrelineEdit->setText(std::to_string(m_configState->junctionToCentreline.GetValue()).c_str());
        m_configState->strutLength = sqrt((fuselageHeight * fuselageHeight + dist * dist).GetValue());
        strutLengthEdit->setText(std::to_string(m_configState->strutLength.GetValue()).c_str());

        m_solver->Run();
    });

    AddHLine(m_mainLayout);
}

void ConfigWidget::BrowseRootAirfoil()
{
    m_configState->rootAirfoilPath = QFileDialog::getOpenFileName(this, "Open").toUtf8();
    m_rootAirfoilEdit->setText(m_configState->rootAirfoilPath.c_str());
}

void ConfigWidget::BrowseTipAirfoil()
{
    m_configState->tipAirfoilPath = QFileDialog::getOpenFileName(this, "Open").toUtf8();
    m_tipAirfoilEdit->setText(m_configState->tipAirfoilPath.c_str());
}

void ConfigWidget::Compute()
{
    m_solver->Run();
}

void ConfigWidget::CreateRootChord()
{
    auto label = new QLabel("Root chord");
    m_rootChordEdit = new QLineEdit(std::to_string(m_configState->rootChord.GetValue()).c_str());
    m_rootChordEdit->setValidator(new QDoubleValidator(1.0, 10., 2, this));
    QObject::connect(m_rootChordEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->rootChord = m_rootChordEdit->text().toDouble();
    });
    auto units = new QLabel("[m]");
    
    auto layout = new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(m_rootChordEdit);
    layout->addWidget(units);
    m_mainLayout->addLayout(layout);
}

void ConfigWidget::CreateTaperRatio()
{
    auto label = new QLabel("Taper Ratio");
    m_taperRatioEdit = new QLineEdit(std::to_string(m_configState->taperRatio.GetValue()).c_str());
    m_taperRatioEdit->setValidator(new QDoubleValidator(0.0, 100., 2, this));
    QObject::connect(m_taperRatioEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->taperRatio = m_taperRatioEdit->text().toDouble();
        });
    auto units = new QLabel("[% root chord]");

    auto layout = new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(m_taperRatioEdit);
    layout->addWidget(units);
    m_mainLayout->addLayout(layout);
}

void ConfigWidget::CreateWingspan()
{
    auto label = new QLabel("Wingspan");
    m_wingspanEdit = new QLineEdit(std::to_string(m_configState->wingspan.GetValue()).c_str());
    m_wingspanEdit->setValidator(new QDoubleValidator(1.0, 100., 2, this));
    QObject::connect(m_wingspanEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->wingspan = m_wingspanEdit->text().toDouble();
        });
    auto units = new QLabel("[m]");

    auto layout = new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(m_wingspanEdit);
    layout->addWidget(units);
    m_mainLayout->addLayout(layout);
}

void ConfigWidget::CreateFrontSparOffset()
{
    auto label = new QLabel("Front Spar Offset");
    m_frontSparOffsetEdit = new QLineEdit(std::to_string(m_configState->frontSparOffset.GetValue()).c_str());
    m_frontSparOffsetEdit->setValidator(new QDoubleValidator(5.0, 90., 2, this));
    QObject::connect(m_frontSparOffsetEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->frontSparOffset = m_frontSparOffsetEdit->text().toDouble();
        });
    auto units = new QLabel("[% chord]");

    auto layout = new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(m_frontSparOffsetEdit);
    layout->addWidget(units);
    m_mainLayout->addLayout(layout);
}

void ConfigWidget::CreateRearSparOffset()
{
    auto label = new QLabel("Rear Spar Offset");
    m_rearSparOffsetEdit = new QLineEdit(std::to_string(m_configState->rearSparOffset.GetValue()).c_str());
    m_rearSparOffsetEdit->setValidator(new QDoubleValidator(5.0, 90., 2, this));
    QObject::connect(m_rearSparOffsetEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->rearSparOffset = m_rearSparOffsetEdit->text().toDouble();
        });
    auto units = new QLabel("[% chord]");

    auto layout = new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(m_rearSparOffsetEdit);
    layout->addWidget(units);
    m_mainLayout->addLayout(layout);
}

void ConfigWidget::CreateQuarterChordSweep()
{
    auto label = new QLabel("1/4 Chord Sweep");
    m_quarterChordSweepEdit = new QLineEdit(std::to_string(m_configState->quarterChordSweep).c_str());
    m_quarterChordSweepEdit->setValidator(new QDoubleValidator(0.0, 1.0, 2, this));
    QObject::connect(m_quarterChordSweepEdit, &QLineEdit::editingFinished, [&]() {
        m_configState->quarterChordSweep = m_quarterChordSweepEdit->text().toDouble();
        });
    auto units = new QLabel("[deg]");

    auto layout = new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(m_quarterChordSweepEdit);
    layout->addWidget(units);
    m_mainLayout->addLayout(layout);
}



