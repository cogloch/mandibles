#include "pch.hpp"
#include "autovdasi.h"
#include <QtWidgets/QApplication>
#include "composites.hpp"
#include "wing_geometry.hpp"
#include "units.hpp"
#include "wingbox.hpp"
#include "loading_state.hpp"
#include "config_widget.hpp"
#include "config_state.hpp"
#include "output_set.hpp"

#include <QtCharts>
using namespace QtCharts;


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    autovdasi w;

    auto outputSet = std::make_shared<OutputSet>();
    auto configState = std::make_shared<ConfigState>();
    auto solver = std::make_shared<Solver>(configState, outputSet);
    auto configWidget = new ConfigWidget(solver, configState);

    outputSet->seriesDeflectionStrut = new QLineSeries();
    outputSet->seriesDeflectionStrut->setName("Wing + Strut");
    
    outputSet->seriesDeflectionClean = new QLineSeries();
    outputSet->seriesDeflectionClean->setName("Clean wing");
    
    outputSet->seriesDeflectionJunctionPos = new QLineSeries();
    QPen pen(Qt::green, 1, Qt::PenStyle::DashLine);
    outputSet->seriesDeflectionJunctionPos->setPen(pen);
    outputSet->seriesDeflectionJunctionPos->setName("Junction position");

    outputSet->seriesDeflectionLimit = new QLineSeries();
    QPen deflLimitPen = pen;
    deflLimitPen.setColor(Qt::red);
    outputSet->seriesDeflectionLimit->setPen(deflLimitPen);
    outputSet->seriesDeflectionLimit->setName("Deflection limit");

    outputSet->seriesDeflectionZero = new QLineSeries();
    QPen zeroDeflPen = pen;
    zeroDeflPen.setColor(Qt::black);
    outputSet->seriesDeflectionZero->setPen(zeroDeflPen);
    outputSet->seriesDeflectionZero->setName("Zero deflection");

    auto chart = new QChart();
    chart->legend()->show();
    chart->addSeries(outputSet->seriesDeflectionLimit);
    chart->addSeries(outputSet->seriesDeflectionStrut);
    chart->addSeries(outputSet->seriesDeflectionClean);
    chart->addSeries(outputSet->seriesDeflectionJunctionPos);
    chart->addSeries(outputSet->seriesDeflectionZero);
    chart->createDefaultAxes();
    chart->setTitle("Deflection");

    auto xAxis = new QValueAxis();
    xAxis->setTickCount(10);

    auto chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    
    auto tabWidget = new QTabWidget();

    auto checksTab = new QWidget();
    tabWidget->addTab(checksTab, "Checks");
    auto checksLayout = new QVBoxLayout();
    checksLayout->addWidget(chartView);

    // Strength checks
    outputSet->rfDirStress = {
        {nullptr, "Upr skin"},
        {nullptr, "Lwr skin"},
        {nullptr, "Upr stringers"},
        {nullptr, "Lwr stringers"},
        {nullptr, "Front spar upr flange"},
        {nullptr, "Front spar lwr flange"},
        {nullptr, "Rear spar upr flange"},
        {nullptr, "Rear spar lwr flange"}
    };

    auto dirStressRFChart = new QChart();
    dirStressRFChart->legend()->show();

    for (auto& rfSeries : outputSet->rfDirStress)
    {
        rfSeries.series = new QLineSeries();
        rfSeries.series->setName(rfSeries.label.c_str());
        dirStressRFChart->addSeries(rfSeries.series);
    }

    outputSet->zeroRF = new QLineSeries();
    outputSet->zeroRF->setPen(zeroDeflPen);
    dirStressRFChart->addSeries(outputSet->zeroRF);

    dirStressRFChart->createDefaultAxes();
    dirStressRFChart->setTitle("Direct stress RFs");

    auto dirStressRFView = new QChartView(dirStressRFChart);
    dirStressRFView->setRenderHint(QPainter::Antialiasing);
    checksLayout->addWidget(dirStressRFView);

    checksTab->setLayout(checksLayout);

    auto loadsTab = new QWidget();
    tabWidget->addTab(loadsTab, "Loads");
    auto loadsLayout = new QVBoxLayout();
    loadsTab->setLayout(loadsLayout);

    // Loads charts
    outputSet->sfChart = new QChart();
    outputSet->sfChart->legend()->show();

    outputSet->seriesSF = new QLineSeries();
    outputSet->seriesSF->setName("2.5g");

    outputSet->sfChart->addSeries(outputSet->seriesSF);

    outputSet->sfChart->createDefaultAxes();
    outputSet->sfChart->setTitle("Shear Force");
    auto sfChartView = new QChartView(outputSet->sfChart);
    sfChartView->setRenderHint(QPainter::Antialiasing);
    loadsLayout->addWidget(sfChartView);

    auto mainLayout = new QGridLayout();
    mainLayout->setSizeConstraint(QLayout::SetNoConstraint);
    mainLayout->addWidget(configWidget, 0, 0);
    mainLayout->addWidget(tabWidget, 0, 1);

    outputSet->convergedFlag = new QLabel("Converged!");
    outputSet->convergedFlag->setStyleSheet("QLabel { color : green; }");
    outputSet->convergedFlag->setVisible(false);
    mainLayout->addWidget(outputSet->convergedFlag, 1, 0);

    auto mainWidget = new QWidget();
    mainWidget->setLayout(mainLayout);
    
    w.setCentralWidget(mainWidget);
    //w.setCentralWidget(chartView);

    w.resize(800, 600);
    w.show();
    
    return a.exec();
}
